// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageDto _$ImageDtoFromJson(Map<String, dynamic> json) {
  return ImageDto(
    id: json['id'] as String,
    blurHash: json['blur_hash'] as String,
    altDescription: json['description'] as String,
    url: json['urls']['regular'] as String,
    likes: json['likes'] as int,
  );
}

Map<String, dynamic> _$ImageDtoToJson(ImageDto instance) => <String, dynamic>{
      'id': instance.id,
      'blurHash': instance.blurHash,
      'altDescription': instance.altDescription,
      'url': instance.url,
      'likes': instance.likes,
    };
