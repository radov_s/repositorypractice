import 'package:flutter/material.dart';
import 'package:layout_building_package_1/app_carousel_slider.dart';
import './custom_appbar.dart';

class MainLayout extends StatelessWidget {
  Widget _buildCircle(double sWidth, double sHeight, double scaling,
      Color color, Color borderColor) {
    return Container(
      width: sWidth * scaling,
      height: sHeight * scaling,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
        border: Border.all(
          color: borderColor,
          width: 2,
        ),
      ),
    );
  }

  final softWhite = Color(0xFFF4EADD);
  final softRed = Color(0xFFE95D55);

  @override
  Widget build(BuildContext context) {
    final _screenWidth = MediaQuery.of(context).size.width;
    final _screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: softWhite,
      body: Stack(
        children: [
          Positioned(
            right: _screenWidth * 0.08,
            top: -_screenHeight * 0.05,
            child: _buildCircle(
                _screenWidth, _screenHeight, 0.60, softWhite, softRed),
          ), //white on the middle top
          Positioned(
            right: -_screenWidth * 0.4,
            top: -_screenHeight * 0.38,
            child:
                _buildCircle(_screenWidth, _screenHeight, 1, softRed, softRed),
          ), //red top right
          Positioned(
            right: -_screenWidth * 0.17,
            top: _screenHeight * 0.05,
            child: _buildCircle(
                _screenWidth, _screenHeight, 0.35, softRed, softWhite),
          ), //red top right inside
          Positioned(
            right: _screenWidth * 0.7,
            top: _screenHeight * 0.75,
            child: _buildCircle(
                _screenWidth, _screenHeight, 0.44, softRed, softRed),
          ), //red bottom left
          Positioned(
            bottom: _screenWidth * 0.1,
            left: -_screenHeight * 0.027,
            child: _buildCircle(_screenWidth, _screenHeight, 0.16,
                Colors.transparent, Colors.white),
          ), //white border bottom left
          Positioned(
            bottom: _screenHeight * 0.30,
            left: _screenWidth * 0.05,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: _screenWidth * 0.90,
                  child: Divider(
                    thickness: 3,
                    color: Colors.black,
                    height: 50,
                  ),
                ),
                Text('Sample subtitle', style: TextStyle(fontSize: 18),),
                Text('Sample long title',style: TextStyle(fontSize: 25),),
                Divider(),
                Text('Lorem ipsum dolor sit amet'),
                Text('consectetur adipiscing elit.'),
              ],
            ),
          ),
          CustomAppBar(),
          Positioned(
            bottom: 50,
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: AppCarouselSlider(),
          ),
        ],
      ),
    );
  }
}
