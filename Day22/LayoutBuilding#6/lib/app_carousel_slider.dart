import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class AppCarouselSlider extends StatefulWidget {
  const AppCarouselSlider({Key key}) : super(key: key);

  @override
  _AppCarouselSliderState createState() => _AppCarouselSliderState();
}

class _AppCarouselSliderState extends State<AppCarouselSlider> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: CarouselSlider.builder(
        itemCount: 10,
        height: 100.0,
        viewportFraction: 0.2,
        onPageChanged: (index) {
          setState(() {
            _current = index;
          });
        },
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.black26,
            child: Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: Icon(Icons.android_outlined,
                  size: 42.0,
                  color: _current == index ? Colors.yellow : Colors.white),
            ),
          );
        },
      ),
    );
  }
}
