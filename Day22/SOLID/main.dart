class Circle {
  final double radius;

  Circle(this.radius);

  double area() {
    return 3.1415 * radius * radius;
  }

  String toString() {
    return radius.toString();
  }
}

class Square {
  final double length;

  Square(this.length);

  double area() {
    return length * length;
  }

  String toString() {
    return length.toString();
  }
}

class AreaCalculator {
  List<dynamic> shapes = [];

  AreaCalculator(shapes);

  void output() {
    shapes.forEach((element) => print(element));
  }
}

class ExtendedAreaCalculator extends AreaCalculator {
  ExtendedAreaCalculator(shapes) : super(shapes);
  void sum() {
    shapes.forEach((element) => print(element.area()));
  }
}

abstract class MyAbstractClass implements ExtendedAreaCalculator {
  @override
  void sum() {
    print('hello');
  }
}

abstract class Animal {
  int _quantityLegs;
  void voice(){}
}

class Human implements Animal {


  @override
  void voice() {
    print('Human speech');
  }

  @override
  int _quantityLegs = 2;
}

class Dog implements Animal {


  @override
  void voice() {
    print('Woof');
  }

  @override
  int _quantityLegs = 4;
}

void main() {
  var list =
  ExtendedAreaCalculator([Circle(3), Circle(4), Circle(5), Square(6)]);
  list.output();
  list.sum();


  Dog dog = Dog();
  print(dog._quantityLegs);
  dog.voice();
}
