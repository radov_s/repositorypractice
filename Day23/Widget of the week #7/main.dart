import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'PROFILE'),
    Tab(text: 'MAIN'),
    Tab(text: 'INTEREST'),
    Tab(text: 'RIGHT'),
  ];

  double _value = 0;
  double _valueCupertino = 0;

  var _selectedRange = RangeValues(0, 10);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.5 -
                AppBar().preferredSize.height,
            child: DefaultTabController(
              length: myTabs.length,
              child: Scaffold(
                appBar: AppBar(
                  bottom: TabBar(
                    tabs: myTabs,
                  ),
                ),
                body: TabBarView(
                  children: myTabs.map((Tab tab) {
                    final String label = tab.text.toLowerCase();
                    return Center(
                      child: RichText(
                        text: TextSpan(
                          text: 'Hello ',
                          style:
                              TextStyle(fontSize: 35, color: Colors.blueAccent),
                          children: const <TextSpan>[
                            TextSpan(
                                text: 'whole',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black38)),
                            TextSpan(text: ' world!'),
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          Slider(
            value: _value,
            divisions: 3,
            label: _value.toStringAsFixed(2),
            onChanged: (newValue) {
              _value = newValue;
              setState(() {});
            },
          ),
          RangeSlider(
            min: 0,
            max: 10.0,
            values: _selectedRange,
            onChanged: (RangeValues newValue) {
              _selectedRange = newValue;
              setState(() {});
            },
          ),
          CupertinoSlider(
            value: _valueCupertino,
            max: 10,
            onChanged: (newValue) {
              _valueCupertino = newValue;
              setState(() {});
            },
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              alignment: Alignment.bottomRight,
              child: Builder(builder: (BuildContext context) {
                return FloatingActionButton(
                  onPressed: () {
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text('Ooo'), duration: Duration(seconds: 1),));
                  },
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
