
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            NumberManagerWidget(child: Text(''),),
            FractionallySizedBox(
              // heightFactor: 0.005,
              widthFactor: 0.66,
              child: Container(
                height: 50.0,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15.0),
                  gradient: LinearGradient(
                    colors: [Colors.black54, Colors.black],
                  ),
                ),
                child: InkWell(
                  onTap: () => print('tap'),
                ),
              ),
            ),
            MyStatefulWidget(
              child: MyContainer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CounterLabel(),
                    CounterValue(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  final Widget child;

  const MyStatefulWidget({Key key, @required this.child}) : super(key: key);

  static MyStatefulWidgetState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MyInheritedWidget>().data;
  }

  @override
  State<StatefulWidget> createState() {
    return MyStatefulWidgetState();
  }
}

class MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _counterValue = 0;

  int get counterValue => _counterValue;

  void addCounterBy1() {
    setState(() {
      _counterValue += 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyInheritedWidget(
      child: widget.child,
      data: this,
    );
  }
}

class MyInheritedWidget extends InheritedWidget{
  final MyStatefulWidgetState data;

  MyInheritedWidget({
    Key key,
    @required Widget child,
    @required this.data,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

class MyContainer extends StatelessWidget {
  final Widget child;

  MyContainer({
    Key key,
    @required this.child,
  })  : super(key: key);

  void onPressed(BuildContext context) {
    MyStatefulWidget.of(context).addCounterBy1();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 200,
        height: 200,
        child: RaisedButton(
          color: Colors.red,
          onPressed: (){
            onPressed(context);
          },
          child: child,
        ),
      ),
    );
  }
}

class CounterLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      "Counter",
      style: TextStyle(
        color: Colors.white,
        fontSize: 20,
      ),
    );
  }
}

class CounterValue extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CounterValueState();
  }
}

class CounterValueState extends State<CounterValue> {
  int counterValue;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    MyStatefulWidgetState data = MyStatefulWidget.of(context);
    counterValue = data.counterValue;
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      "$counterValue",
      style: TextStyle(
        color: Colors.white,
        fontSize: 25.0
      ),
    );
  }
}

class MyModel extends InheritedModel<double> {

  final double a;

  MyModel({this.a, Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(covariant MyModel oldWidget) {
    return a != oldWidget.a;
  }

  @override
  bool updateShouldNotifyDependent(MyModel oldWidget, Set<double> dependencies) {
    return (a != oldWidget.a && dependencies.contains('a'));
  }

  static MyModel of(BuildContext context, {double aspect}) {
    return InheritedModel.inheritFrom<MyModel>(context, aspect: aspect);
  }

}

class NumberManagerWidget extends StatefulWidget {
  final Widget child;

  NumberManagerWidget({Key key, @required this.child})
      : assert(child != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() => NumberManagerWidgetState();
}

class NumberManagerWidgetState extends State<NumberManagerWidget> {
  double first;

  @override
  void initState() {
    super.initState();
    first = 0;
  }

  // ... snip code for handling timers, etc, to update the tick values ...

  @override
  Widget build(BuildContext context) {
    return MyModel(
      a: first,
      child: widget.child,
    );
  }
}

class AsInheritedModel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final MyModel model = MyModel.of(context, aspect: 0);
    return Text("Values: ${model.a}");
  }
}

class AsInheritedWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final MyModel model = MyModel.of(context);
    return Text("Values: ${model.a}");
  }
}