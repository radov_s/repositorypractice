
import 'dart:async';

import 'package:flutter/material.dart';


class ColorsStream {

  int _redValue = 0;
  int _greenValue = 0;
  int _blueValue = 0;

  ColorsStream() {
    _addColorToStream();
  }

  StreamController _colorStreamController = StreamController<Color>();

  Stream<Color> get pageColor => _colorStreamController.stream;

  void changeRedColor(int newRedValue){
    _redValue = newRedValue;
    _addColorToStream();
  }

  void changeGreenColor(int newGreenValue){
    _greenValue = newGreenValue;
    _addColorToStream();
  }

  void changeBlueColor(int newBlueValue){
    _blueValue = newBlueValue;
    _addColorToStream();
  }

  void _addColorToStream() => _colorStreamController.add(Color.fromRGBO(_redValue, _greenValue, _blueValue, 1));
}