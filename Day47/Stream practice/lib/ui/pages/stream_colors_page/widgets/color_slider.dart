import 'package:flutter/material.dart';

class ColorSlider extends StatelessWidget {
  final Color sliderColor;
  final double value;
  final void Function(int) onChanged;

  ColorSlider({
    @required this.sliderColor,
    @required this.value,
    @required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Slider(
      min: 0,
      max: 255,
      activeColor: sliderColor,
      value: value,
      onChanged: (double value) {
        onChanged(value.round());
      },
    );
  }
}
