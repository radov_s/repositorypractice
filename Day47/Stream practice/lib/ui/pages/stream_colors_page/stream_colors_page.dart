import 'package:flutter/material.dart';
import 'package:streams_practice/streams/colors_stream.dart';
import 'package:streams_practice/ui/pages/stream_colors_page/widgets/color_slider.dart';

class StreamColorsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ColorsStream _colorsStream = ColorsStream();
    return Scaffold(
      appBar: AppBar(
        title: Text('Streams background'),
      ),
      body: StreamBuilder<Color>(
        initialData: Colors.black,

        stream: _colorsStream.pageColor,
        builder: (context, snapshot) {
          return Container(
            color: snapshot.data,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ColorSlider(
                  sliderColor: Colors.red,
                  value: snapshot.data.red.toDouble(),
                  onChanged: _colorsStream.changeRedColor,
                ),
                ColorSlider(
                  sliderColor: Colors.green,
                  value: snapshot.data.green.toDouble(),
                  onChanged: _colorsStream.changeGreenColor,
                ),
                ColorSlider(
                  sliderColor: Colors.blue,
                  value: snapshot.data.blue.toDouble(),
                  onChanged: _colorsStream.changeBlueColor,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
