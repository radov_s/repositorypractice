import 'package:flutter/material.dart';
import 'package:streams_practice/ui/pages/stream_colors_page/stream_colors_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: StreamColorsPage(),
    );
  }
}
