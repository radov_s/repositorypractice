import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/text_provider.dart';

class FirstPage extends StatelessWidget {
  static const String routeName = '/first-page';

  @override
  Widget build(BuildContext context) {
    final TextProvider textProvider = Provider.of<TextProvider>(context);

    Widget _buildButton(IconData icon, Function function) {
      return InkWell(
        borderRadius: BorderRadius.circular(50.0),
        onTap: function,
        child: Container(
          width: 50.0,
          height: 50.0,
          //margin: const EdgeInsets.all(20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0),
            color: Color.fromRGBO(3, 57, 108, 0.6),
          ),
          child: Icon(icon),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          child: Icon(Icons.next_plan),
          onTap: (() => Navigator.pushReplacementNamed(context, '/')),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(25),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildButton(Icons.remove, textProvider.decrementCounter),
            _buildButton(Icons.add, textProvider.incrementCounter),
          ],
        ),
      ),
    );
  }
}
