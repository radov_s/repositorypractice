import 'package:flutter/material.dart';

class TextProvider with ChangeNotifier {
  int _sum = 0;

  int get Sum {
    return _sum;
  }

  void incrementCounter() {
    _sum++;
    notifyListeners();
  }

  void decrementCounter() {
    _sum--;
    notifyListeners();
  }
}
