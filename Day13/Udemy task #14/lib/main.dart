import 'package:flutter/material.dart';
import 'package:lecture_practice_5/widget/snackbar_test.dart';
import 'package:provider/provider.dart';
import './widget/first_page.dart';
import './provider/text_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextProvider(),
      child: MaterialApp(
        home: MyHomePage(),
        initialRoute: '/',
        routes: {
          FirstPage.routeName: (context) => FirstPage(),
        },
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final TextProvider textProvider =
        Provider.of<TextProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          child: Icon(Icons.next_plan),
          onTap: (() =>
              Navigator.pushReplacementNamed(context, FirstPage.routeName)),
        ),
      ),
      body: Center(
        child: Text(
          '${textProvider.Sum}',
          style: TextStyle(fontSize: 40),
        ),
      ),
    );
  }
}
