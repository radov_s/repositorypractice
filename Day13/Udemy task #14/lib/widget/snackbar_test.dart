import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/text_provider.dart';

class SnackBarTest extends StatelessWidget {
  final IconData icon;
  final String text;

  SnackBarTest(this.icon, this.text);

  @override
  Widget build(BuildContext context) {
    final TextProvider textProvider = Provider.of<TextProvider>(context);
    return Builder(
      builder: (ctx) => InkWell(
        onTap: () {
          Scaffold.of(ctx).hideCurrentSnackBar();
          Scaffold.of(ctx).showSnackBar(
            SnackBar(
              duration: Duration(seconds: 1),
              content: Text('$text${textProvider.Sum}'),
            ),
          );
        },
        child: Icon(icon, size: 50.0,),
      ),
    );
  }
}
