import 'package:flutter/material.dart';
import 'dart:math' as math;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
      ),
      home: MyHomePage(title: 'SLIDER'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _length = 100;

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[400],
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onHorizontalDragUpdate: (DragUpdateDetails details) {
                if(_length >= _width * 0.125 &&  _length <= _width * 0.875) {
                  setState(() {
                    _length += details.delta.dx;
                  });
                }
                if (_length < _width * 0.125) {
                  setState(() {
                    _length = _width * 0.125;
                  });
                }
                if (_length > _width *0.875) {
                  setState(() {
                    _length = _width * 0.875;
                  });
                }
              },
              child: Container(
                width: double.infinity,
                height: 100,
                color: Colors.grey[400],
                child: CustomPaint(
                  painter: SliderPainter(_length),
                ),
              ),
            ),
            Center(
              child: Text(
                _length.round().toString(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SliderPainter extends CustomPainter {
  double length;

  SliderPainter(this.length);


  @override
  void paint(Canvas canvas, Size size) {
    var pointPaint = Paint()
      ..color = Colors.red
      ..strokeWidth = 3
      ..strokeCap = StrokeCap.round;
    var paint = Paint()
      ..color = Colors.red
      ..strokeWidth = 7
      ..strokeCap = StrokeCap.round;

    var path = Path();

    Offset startingPoint = Offset(0.125 * size.width, size.height / 4);
    Offset endingPoint = Offset(size.width * 0.875, size.height / 4);

    Offset pointOnCircle = Offset(
      length,
      size.height / 4,
    );

    canvas.drawLine(startingPoint, endingPoint, paint);
    canvas.drawCircle(pointOnCircle, 10, pointPaint);

    path.close();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }

}
