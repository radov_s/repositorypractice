import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _value = 0;
  bool _isTurn = false;


  DateTime _chosenDateTime;

  // Show the modal that contains the CupertinoDatePicker
  void _showDatePicker(ctx) {
    // showCupertinoModalPopup is a built-in function of the cupertino library
    showCupertinoModalPopup(
      context: ctx,
      builder: (_) => Container(
        height: 500,
        color: Colors.blueAccent.withOpacity(_value),
        child: Column(
          children: [
            Container(
              height: 400,
              child: CupertinoDatePicker(
                  initialDateTime: DateTime.now(),
                  onDateTimeChanged: (val) {
                    setState(() {
                      _chosenDateTime = val;
                    });
                  }),
            ),
            CupertinoButton(
              child: Text('Apply', style: TextStyle(color: Colors.black, fontSize: 23),),
              onPressed: () => Navigator.of(ctx).pop(),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _isTurn? Colors.indigo : Colors.white,
      appBar: CupertinoNavigationBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CupertinoSlider(
                    value: _value,
                    min: 0,
                    max: 1,
                    onChanged: (newValue) {
                      setState(() {
                        _value = newValue;
                      });
                    },
                  ),
                  CupertinoSwitch(
                    value: _isTurn,
                    onChanged: (_) {
                      setState(() {
                        _isTurn = !_isTurn;
                      });
                    },
                  )
                ],
              ),
            ),
            Center(
              child: Text(_chosenDateTime != null
                  ? _chosenDateTime.toString()
                  : 'No date time picked!'),
            ),
            Expanded(
              child: InkWell(
                onTap: () {
                  _showDatePicker(context);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
