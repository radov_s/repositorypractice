import 'package:flutter/material.dart';
import './main.dart';

import 'drawer.dart';
import 'first_page.dart';

class SecondPage extends StatelessWidget {
  static const String routeName = 'second-page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
     drawer: MainDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(MyHomePage.routeName),
              child: Text('To Home'),
            ),
            RaisedButton(
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(FirstPage.routeName),
              child: Text('To First'),
            ),
          ],
        ),
      ),
    );
  }
}
