import 'package:flutter/material.dart';
import './drawer.dart';
import './first_page.dart';
import './second_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      home: MyHomePage(),
      onGenerateRoute: (setting) {
        if(setting.name == FirstPage.routeName) {
          return MaterialPageRoute(builder: (ctx) => FirstPage());
        }
        if(setting.name == SecondPage.routeName) {
          return MaterialPageRoute(builder: (ctx) => SecondPage());
        }
        return MaterialPageRoute(builder: (ctx) => MyHomePage());
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  static const routeName = './';
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(FirstPage.routeName),
              child: Text('To First'),
            ),
            RaisedButton(
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(SecondPage.routeName),
              child: Text('To Second'),
            ),
          ],
        ),
      ),
    );
  }
}
