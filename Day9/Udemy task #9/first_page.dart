import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './main.dart';
import './second_page.dart';
import 'drawer.dart';

class FirstPage extends StatelessWidget {
  static const String routeName = 'first-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Page'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(MyHomePage.routeName, arguments: 'First page'),
              child: Text('To Home'),
            ),
            RaisedButton(
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(SecondPage.routeName),
              child: Text('To Second'),
            ),
          ],
        ),
      ),
    );
  }
}
