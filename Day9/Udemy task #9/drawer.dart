import 'package:flutter/material.dart';
import './first_page.dart';
import './main.dart';
import './second_page.dart';

class MainDrawer extends StatelessWidget {
  Widget buildShadowBox({Widget child}) {
    return Container(
      margin: EdgeInsets.only(left: 5, top: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: child,
    );
  }

  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Text(
            'MENU',
            style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 30,
                color: Theme.of(context).primaryColor),
          ),
          SizedBox(
            height: 20,
          ),
          buildShadowBox(
              child: buildListTile('HOME', Icons.home, () {
            Navigator.of(context).pushReplacementNamed(MyHomePage.routeName);
          })),
          buildShadowBox(
              child: buildListTile('FIRST', Icons.first_page, () {
            Navigator.of(context).pushReplacementNamed(FirstPage.routeName);
          })),
          buildShadowBox(
              child: buildListTile(
                  'SECOND', Icons.format_list_numbered_outlined, () {
            Navigator.of(context).pushReplacementNamed(SecondPage.routeName);
          })),
        ],
      ),
    );
  }
}
