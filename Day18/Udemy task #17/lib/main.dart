import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/list_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ListProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    ListProvider providerModel =
        Provider.of<ListProvider>(context, listen: false);

    Widget _buildButton(Color color, Function function, String text) {
      return Expanded(
        child: Container(
          height: 80.0,
          color: color,
          child: InkWell(
            onTap: () {
              function();
              setState(() {});
            },
            child: Center(child: Text(text)),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: providerModel.list.length,
                itemBuilder: (BuildContext context, int index) {
                  return Center(child: Text(providerModel.list[index]));
                },
              ),
            ),
            Row(
              children: [
                _buildButton(
                    Colors.blue, providerModel.randomizeSize, 'generate'),
                _buildButton(Colors.pinkAccent, providerModel.sortList, 'sort'),
                _buildButton(Colors.deepPurpleAccent,
                    providerModel.deleteRandomItem, 'delete random item'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
