import 'dart:math';

import 'package:flutter/material.dart';

class ListProvider with ChangeNotifier {
  static int _size = 0;
  bool ASC = true;
  List<String> list = List.generate(_size, (index) => 'Item $index.toString()');

  int get size{
    return _size;
  }

  List<String> get LIST {
    return list;
  }

  void sortList() {
    if (!ASC) {
      list.sort((a,b) => a.compareTo(b));
      ASC = true;
    } else {
      list.sort((b,a) => a.compareTo(b));
      ASC = false;
    }
    notifyListeners();
  }

  void randomizeSize() {
    _size = Random().nextInt(100);
    list = List.generate(_size, (index) => 'Item $index');
    notifyListeners();
  }
  void deleteRandomItem() {
    if (list.isNotEmpty) {
      print(list.removeAt(Random().nextInt(list.length)));
    }
    notifyListeners();
  }

}