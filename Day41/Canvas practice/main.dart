import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(title: 'SLIDER'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[400],
        title: Text(widget.title),
      ),
      body: Center(
        child: Center(
          child: CustomSlider(
            init: 7,
            min: 5,
            max: 12,
            width: _width,
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class CustomSlider extends StatefulWidget {
  CustomSlider({Key key, this.min, this.max,this.init, this.width}) : super(key: key);

  double init = 0;
  double min = 0;
  double max = 0;
  double width;

  @override
  _CustomSliderState createState() => _CustomSliderState();
}

class _CustomSliderState extends State<CustomSlider> {
 double value = 0;

  @override
  void initState() {
    setState(() {
      value = widget.width/(widget.max - widget.min) * (widget.init - widget.min);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double percentageSlider = (widget.max - widget.min) / (widget.width * 0.875 - widget.width * 0.125) * value + widget.min;
    return GestureDetector(
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        if(value >= widget.width * 0.125 &&  value <= widget.width * 0.875) {
          setState(() {
            value += details.delta.dx;
          });
        }
        if (value < widget.width * 0.125) {
          setState(() {
            value = widget.width * 0.125;
          });
        }
        if (value > widget.width *0.875) {
          setState(() {
            value = widget.width * 0.875;
          });
        }
      },
      onTapDown: (TapDownDetails details) {
        value = details.localPosition.dx;
        if (value < widget.width * 0.125) {
          setState(() {
            value = widget.width * 0.125;
          });
        }
        if (value > widget.width * 0.875) {
          setState(() {
            value = widget.width * 0.875;
          });
        } else {
          setState(() {});
        }
      },
      child: Container(
        width: double.infinity,
        height: 75,
        child: CustomPaint(
          painter: SliderPainter(value),
          child: Column(
            children: [
              Text(
                percentageSlider.toString(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SliderPainter extends CustomPainter {
  double length;

  SliderPainter(this.length);

  @override
  void paint(Canvas canvas, Size size) {
    var pointPaint = Paint()
      ..color = Colors.red
      ..strokeWidth = 3
      ..strokeCap = StrokeCap.round;
    var paint = Paint()
      ..color = Colors.red
      ..strokeWidth = 7
      ..strokeCap = StrokeCap.round;

    var paintOpacity = Paint()
      ..color = Colors.red.withOpacity(0.5)
      ..strokeWidth = 7
      ..strokeCap = StrokeCap.round;

    Offset pointOnCircle = Offset(
      length,
      size.height / 4,
    );

    canvas.drawLine(Offset(0.125 * size.width, size.height / 4), Offset(length, size.height / 4), paint);
    canvas.drawLine(Offset(length, size.height / 4), Offset(size.width * 0.875, size.height / 4), paintOpacity);
    canvas.drawCircle(pointOnCircle, 10, pointPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
