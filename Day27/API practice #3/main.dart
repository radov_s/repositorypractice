import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<List<Brand>> fetchData(http.Client client, String brandName) async {
  final response = await client.get(Uri.parse(
      'http://makeup-api.herokuapp.com/api/v1/products.json?brand=$brandName'));
  return compute(parseData, response.body);
}

List<Brand> parseData(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Brand>((json) => Brand.fromJson(json)).toList();
}

class Brand {
  final String name;
  final double rating;
  final String productType;
  final String urlImage;
  final String price;

  Brand({this.name, this.rating, this.productType, this.urlImage, this.price});

  factory Brand.fromJson(Map<String, dynamic> json) {
    return Brand(
        name: json['name'],
        rating: json['rating'],
        price: json['price'],
        productType: json['product_type'],
        urlImage: json['image_link'] == null
            ? 'https://i.stack.imgur.com/6M513.png'
            : json['image_link']);
  }
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _brand = 'maybelline'; //maybelline
  final fieldText = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 44.0,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0),
            child: TextField(
              controller: fieldText,
              onSubmitted: (value) {
                setState(() {
                  _brand = value;
                });
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                prefixIcon: Icon(Icons.search),
                contentPadding:
                    const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                hintText: 'Brand',
                suffix: InkWell(
                  onTap: () => clearInput(),
                  child: Text(
                    'Clear',
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<List<Brand>>(
              future: fetchData(http.Client(), _brand),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                return snapshot.hasData
                    ? UniversityList(
                        brandItems: snapshot.data,
                        nameBrand: _brand,
                      )
                    : Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }

  void clearInput() {
    fieldText.clear();
  }
}

class UniversityList extends StatelessWidget {
  final List<Brand> brandItems;
  final String nameBrand;

  UniversityList({this.brandItems, this.nameBrand});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: brandItems.length,
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 250,
          childAspectRatio: 4 / 4,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5),
      itemBuilder: (context, index) {
        return Hero(
          tag: index,
          child: Material(
            child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (BuildContext context) {
                    return PageInfo(
                      index: index,
                      name: brandItems[index].name,
                      urlImage: brandItems[index].urlImage,
                      productType: brandItems[index].productType,
                      rating: brandItems[index].rating,
                      price: brandItems[index].price,
                    );
                  },
                ),
              ),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(
                        vertical: 5.0, horizontal: 5.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border.all(color: Colors.black),
                      image: DecorationImage(
                          image: NetworkImage(brandItems[index].urlImage),
                          fit: BoxFit.fill),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(
                        vertical: 5.0, horizontal: 5.0),
                    width: double.infinity,
                    height: 80,
                    color: Colors.black.withOpacity(0.4),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        brandItems[index].name.replaceAll(nameBrand, '1'),
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class PageInfo extends StatelessWidget {
  int index;
  final String name;
  final double rating;
  final String productType;
  final String urlImage;
  final String price;

  PageInfo(
      {this.index,
      this.name,
      this.rating,
      this.productType,
      this.urlImage,
      this.price});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Hero(
        tag: index,
        child: Scaffold(
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 40.0,
                  ),
                  Image.network(urlImage),
                  Text(name),
                  Text(productType),
                  Text(price),
                  Text(rating.toString()),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
