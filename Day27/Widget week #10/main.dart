import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool _pinned = false;
  bool _snap = false;
  bool _floating = false;
  double targetValue = 24.0;

  List<int> items = List<int>.generate(15, (int index) => index);

  @override
  Widget build(BuildContext context) {
    return PageView(
      children: [
        Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                pinned: _pinned,
                snap: _snap,
                floating: _floating,
                actions: [
                  IconButton(
                      icon: Icon(Icons.construction_sharp),
                      onPressed: () => print('u tap me')),
                ],
                backgroundColor: Color(0xff2962ff),
                expandedHeight: 160.0,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image.network(
                    'https://blogs.esa.int/space19plus/files/2019/03/nebula.jpg',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SliverGrid(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200.0,
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  childAspectRatio: 2.0,
                ),
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return Container(
                      alignment: Alignment.center,
                      color: Colors.blue[100 * (index % 9)],
                    );
                  },
                  childCount: 20,
                ),
              ),
            ],
          ),
        ),
        Scaffold(
          appBar: AppBar(),
          body: ListView.builder(
            itemCount: items.length,
            padding: const EdgeInsets.symmetric(vertical: 16),
            itemBuilder: (BuildContext context, int index) {
              return Dismissible(
                child: ListTile(
                  title: Text(
                    'Item ${items[index]}',
                  ),
                ),
                background: Container(
                  color: Colors.blueAccent,
                  child: Icon(Icons.add),
                ),
                secondaryBackground: Container(
                  color: Colors.red,
                  child: Icon(Icons.delete),
                ),
                // direction: DismissDirection.endToStart,
                key: UniqueKey(),
                onDismissed: (DismissDirection direction) {
                  setState(() {
                    if (direction == DismissDirection.endToStart) {
                      items.removeAt(index);
                    } else if (direction == DismissDirection.startToEnd) {
                      items.insert(index, items[index]);
                    }
                  });
                },
              );
            },
          ),
        ),
        Scaffold(
          body: Center(
            child: TweenAnimationBuilder(
              tween: Tween<double>(begin: 0, end: targetValue),
              duration: const Duration(seconds: 3),
              builder: (BuildContext context, double size, Widget child) {
                return IconButton(
                  iconSize: size,
                  color: Colors.blue,
                  icon: child,
                  onPressed: () {
                    setState(() {
                      targetValue = targetValue == 24.0 ? 400 : 24.0;
                    });
                  },
                );
              },
              child: Image.network('https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg')
            ),
          ),
        ),
      ],
    );
  }
}
