import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('test'),
        ),
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(100),
              child: Image.network(
                'https://picsum.photos/250?image=9',
                width: 100,
                height: 100,
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Text('some text'),
            ),
            Container(
              padding: EdgeInsets.only(top: 50),
              child: Text('sample text'),
            ),
          ],
        )
      ),
    );
  }
}