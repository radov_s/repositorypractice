import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double opacityLevel = 1.0;
  bool selected = false;

  void _changeOpacity() {
    setState(() => opacityLevel = opacityLevel == 0 ? 1.0 : 0.0);
  }

  double padValue = 0.0;
  void _updatePadding(double value) {
    setState(() {
      padValue = value;
    });
  }

  int acceptedData = 0;

  @override
  Widget build(BuildContext context) {
    return PageView(
      children: [
        Scaffold(
          appBar: AppBar(),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: double.infinity,
                color: Colors.black,
                height: 50.0,
                child: InkWell(
                    onTap: () {
                      _updatePadding(padValue == 0.0 ? 100.0 : 0.0);
                    }
                ),
              ),
              AnimatedPadding(
                padding: EdgeInsets.all(padValue),
                duration: const Duration(seconds: 2),
                curve: Curves.easeInOut,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 5,
                  color: Colors.blue,
                ),
              ),
              Expanded(
                child: AnimatedOpacity(
                    opacity: opacityLevel,
                    duration: const Duration(seconds: 2),
                    child: Image.network(
                        'https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg')),
              ),
              SizedBox(
                width: 200,
                height: 200,
                child: Stack(
                  children: <Widget>[
                    AnimatedPositioned(
                      width: selected ? 150.0 : 50.0,
                      height: selected ? 150.0 : 50.0,
                      top: selected ? 20.0 : 350.0,
                      duration: const Duration(seconds: 2),
                      curve: Curves.fastOutSlowIn,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            selected = !selected;
                          });
                        },
                        child: ColoredBox(
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              AnimatedContainer(
                width: selected ? 400 : 50.0,
                height: selected ? 50.0 : 50.0,
                color: selected ? Colors.red : Colors.blue,
                alignment:
                selected ? Alignment.center : AlignmentDirectional.topCenter,
                duration: const Duration(seconds: 3),
                curve: Curves.fastOutSlowIn,
                child: InkWell(
                    child: Center(child: const Text('TAP')),
                    onTap: () {
                      _changeOpacity();
                      setState(() {
                        selected = !selected;
                      });
                    }),
              ),
            ],
          ),
        ),
        Scaffold(
            body: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Draggable<int>(
                    data: 10,
                    child: Container(
                      height: 100.0,
                      width: 100.0,
                      child: const Center(
                        child: Icon(Icons.airplanemode_active),
                      ),
                    ),
                    feedback: Container(
                      color: Colors.transparent,
                      height: 100,
                      width: 100,
                      child: const Icon(Icons.airplanemode_inactive),
                    ),
                    childWhenDragging: Container(
                      height: 100.0,
                      width: 100.0,
                      child: const Center(
                        child: Icon(Icons.warning),
                      ),
                    ),
                  ),
                  DragTarget<int>(
                    builder: (
                        BuildContext context,
                        List<dynamic> accepted,
                        List<dynamic> rejected,
                        ) {
                      return Container(
                        height: 100.0,
                        width: 100.0,
                        color: Colors.cyan,
                        child: Center(
                          child: Text('Airplane crash to: $acceptedData times'),
                        ),
                      );
                    },
                    onAccept: (int data) {
                      setState(() {
                        acceptedData += data;
                      });
                    },
                  ),
                ],
              ),
            ),
        ),
      ]
    );
  }
}
