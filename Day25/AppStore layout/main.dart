import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.black,
        accentColor: Colors.black
      ),
      home: Scaffold(
        body: HeroAnimation(),
      ),
    );
  }
}

class HeroAnimation extends StatefulWidget {
  @override
  _HeroAnimationState createState() => _HeroAnimationState();
}

class _HeroAnimationState extends State<HeroAnimation> {
  final date = DateTime.now();

  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView.builder(
          itemCount: 20,
          itemBuilder: (BuildContext context, int index) {
            return Hero(
              tag: index,
              transitionOnUserGestures: true,
              flightShuttleBuilder:
                  (flightContext, animation, direction, fromcontext, toContext) {
                final Hero toHero = toContext.widget;
                return direction == HeroFlightDirection.push
                    ? ScaleTransition(
                        scale: animation.drive(
                          Tween<double>(
                            begin: 0.75,
                            end: 1.02,
                          ).chain(
                            CurveTween(
                                curve:
                                    Interval(0.1, 1.0, curve: Curves.easeInOut)),
                          ),
                        ),
                        child: toHero.child,
                      )
                    : SizeTransition(
                        sizeFactor: animation,
                        child: toHero.child,
                      );
              },
              child: GestureDetector(
                onTap: () {
                  print(date);
                  Navigator.of(context).push(
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) {
                        return PageInfo(index);
                      },
                    ),
                  );
                },
                child: Material(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      width: double.infinity,
                      height: 400.0,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        shape: BoxShape.rectangle,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.circular(15.0),
                        image: DecorationImage(
                          image: NetworkImage(
                              'http://groovymovieguy.com/wp-content/uploads/2020/10/the-light-house-poster-2-776x1024.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: CupertinoTabBar(
        onTap: (newValue) {
          setState(() {
            index = newValue;
          });
        },
        iconSize: 30.0,
        currentIndex: index,
        activeColor: Colors.blue,
        inactiveColor: Colors.grey,
        backgroundColor: Colors.white.withOpacity(0.3),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.gamepad),
            label: 'Arcade',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.apps),
            label: 'Apps',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.airplanemode_on),
            label: 'Games',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tab),
            label: 'Today',
          ),
        ],
      ),
    );
  }
}

class PageInfo extends StatelessWidget {
  int _index = 0;

  PageInfo(this._index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Hero(
            transitionOnUserGestures: true,
            tag: _index,
            child: Image.network(
                'http://groovymovieguy.com/wp-content/uploads/2020/10/the-light-house-poster-2-776x1024.jpg'),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          )
        ],
      ),
    );
  }
}
