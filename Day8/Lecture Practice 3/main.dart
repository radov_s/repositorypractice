import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import './page_example.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(1080, 2000),
      builder: () => MaterialApp(
        theme: ThemeData(
          textTheme: TextTheme(
            button: TextStyle(
                fontSize: 50.sp
            ),
          ),
          primaryColor: Colors.lightBlue,
        ),
        home: PageExample(),
      ),
    );
  }
}
