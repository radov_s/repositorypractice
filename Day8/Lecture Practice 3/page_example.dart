import 'package:flutter/material.dart';
import './main_layout.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PageExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('Example adaptive'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'text change adaptive'.toUpperCase(),
              style: TextStyle(fontSize: 50.h),
            ),
            Container(
              width: 150.w,
              height: 150.h,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 1)
              ),
              child: InkWell(
                onTap: () => print('tap'),
                child: Text(
                  'Tap me',
                  style: Theme.of(context).textTheme.button,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
