import 'package:flutter/material.dart';
import 'package:layout_building_app_store_1/widgets/products_grid.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: ProductsGrid(),
    );
  }
}