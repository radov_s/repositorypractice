import 'package:flutter/material.dart';
import 'package:layout_building_app_store_1/model/item.dart';


class Items with ChangeNotifier {
  List<Item> _items = [
    Item(
        id: 'item1',
        urlImage:
        'http://groovymovieguy.com/wp-content/uploads/2020/10/the-light-house-poster-2-776x1024.jpg',
        description: ' ',
        addInformation: ' '),
    Item(
        id: 'item2',
        urlImage:
        'https://i.pinimg.com/originals/96/a0/0d/96a00d42b0ff8f80b7cdf2926a211e47.jpg',
        description: ' ',
        addInformation: ' '),
    Item(
        id: 'item3',
        urlImage:
        'https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/169241483/original/af413b406d3cb2eedc149baf2a8b87156d86d56f/make-your-movie-or-short-film-poster.jpg',
        description: ' ',
        addInformation: ' '),
  ];
  List<Item> get items {
    return [..._items];
  }

  Item findById(String id) {
    return _items.firstWhere((product) => product.id == id);
  }
}