import 'dart:math';
import 'package:flutter/material.dart';

class MyModel with ChangeNotifier {
  int _firstCounter = 0;
  int _secondCounter = 0;

  int get counterFirst {
    return _firstCounter;
  }

  int get secondCounter {
    return _secondCounter;
  }

  void incrementCounter() {
    if (Random().nextBool()) {
      ++_firstCounter;
    } else {
      ++_secondCounter;
    }
    notifyListeners();
  }

  void decrementCounter() {
    if (Random().nextBool()) {
      --_firstCounter;
    } else {
      --_secondCounter;
    }
    notifyListeners();
  }
}
