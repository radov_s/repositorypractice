import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/my_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => MyModel(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MyModel providerModel = Provider.of<MyModel>(context, listen: false);
    return Scaffold(
      body: Center(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Selector<MyModel, int>(
                    selector: (_, providerModel) => providerModel.counterFirst,
                    builder: (context, counterFirst, child) {
                      return Text('$counterFirst');
                    }),
                RaisedButton(
                  onPressed: providerModel.decrementCounter,
                  child: Text('decrement'),
                ),
              ],
            ),
            Column(
              children: [
                Selector<MyModel, int>(
                  selector: (_, providerModel) => providerModel.secondCounter,
                  builder: (context, secondCounter, child) {
                    return Text('$secondCounter');
                  },
                ),
                RaisedButton(
                  onPressed: providerModel.incrementCounter,
                  child: Text('increment'),
                ),
              ],
            ),
          ],
        ),
      )),
    );
  }
}
