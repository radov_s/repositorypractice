import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        accentColor: Color(0xFF2AB7CA),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var list = List<int>.generate(101, (int index) => index);

  void _sortList() {
    setState(() {
      list = list.reversed.toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.5;
    double height = MediaQuery.of(context).size.height * 0.33;
    return Scaffold(
      backgroundColor: Color(0xFF2AB7CA),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.15,
            color: Color(0xfffe4a49),
          ),
          Divider(
            thickness: 3,
            color: Colors.black,
          ),
          Stack(
            children: [
              Center(
                child: Container(
                  width: width,
                  height: width,
                  decoration: BoxDecoration(
                    color: Colors.yellow,
                    shape: BoxShape.circle,
                  ),
                  child: CustomPaint(
                    painter: MyPainter(),
                    child: Container(),
                  ),
                ),
              ),
              SizedBox(
                height: width,
                child: ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Center(child: Text(list[index].toString()));
                  },
                ),
              ),
            ],
          ),
          Divider(
            thickness: 3,
            color: Colors.black,
          ),
          Container(
            width: double.infinity,
            height: height,
            child: InkWell(
              onTap: () {
                _sortList();
              },
              child: Center(
                child: Text(
                  'SORT LIST',
                  style: TextStyle(fontSize: 35.0),
                ),
              ),
            ),
          ),
          Divider(
            thickness: 3,
            color: Colors.black,
          ),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.21,
            color: Color(0xfff4f4f8),
          )
        ],
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.black87
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;

    final offsetCenter = Offset(
      size.width / 2,
      size.height / 2,
    );

    canvas.drawCircle(offsetCenter, 100.0 - 2.0, paint);

    paint
      ..style = PaintingStyle.fill
      ..strokeJoin = StrokeJoin.round;

    final arrowPath = Path()
      ..moveTo(offsetCenter.dx - 2.5, offsetCenter.dy)
      ..lineTo(offsetCenter.dx, 10)
      ..lineTo(offsetCenter.dx + 2.5, offsetCenter.dy)
      ..close();

    canvas.drawPath(arrowPath, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return this != oldDelegate;
  }
}
