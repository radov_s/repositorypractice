import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/navigation_service/route_helper.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/ui/image_page/image_page.dart';
import 'package:redux_conceptions/ui/user_page/user_page.dart';
import 'package:redux_epics/redux_epics.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      EpicMiddleware(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        onGenerateRoute: (RouteSettings settings) =>
            RouteHelper.instance.onGenerateRoute(settings),
        navigatorKey: NavigatorHolder.navigatorKey,
        home: Material(
          color: Colors.white,
          child: Scaffold(body: ImagePage()),
        ),
      ),
    );
  }
}
