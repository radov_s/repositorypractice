import 'dart:collection';

import 'package:redux_conceptions/dto/user_dto.dart';
import 'package:redux_conceptions/store/app/reducer.dart';
import 'package:redux_conceptions/store/users_state/users_action.dart';

class UsersState {
  final List<UserDtO> users;
  final UserDtO selectedUser;

  UsersState(
    this.users,
    this.selectedUser,
  );

  factory UsersState.initial() => UsersState([], UserDtO());

  UsersState copyWith({
    List<UserDtO> users,
    String id,
    UserDtO image,
  }) {
    return UsersState(
      users ?? this.users,
      image ?? this.selectedUser,
    );
  }

  UsersState reducer(dynamic action) {
    return Reducer<UsersState>(
      actions: HashMap.from({
        SaveUsersAction: (dynamic action) => saveUsers((action as SaveUsersAction).users),
      }),
    ).updateState(action, this);
  }

  UsersState saveUsers(List<UserDtO> users) {
    return copyWith(users: users);
  }
}
