import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/services/unspash_users_service.dart';

class UnsplashImageRepository {
  UnsplashImageRepository._privateConstructor();

  static final UnsplashImageRepository instance =
      UnsplashImageRepository._privateConstructor();

  Future<List<Image>> getUsers() async {
    var responseData = await UnsplashImageService.instance.getUsers();
    List<Image> users = [];
    for (var item in responseData.response) {
      users.add(Image.fromJson(item));
    }
    return users;
  }
}
