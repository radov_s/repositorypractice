import 'package:redux_conceptions/dto/user_dto.dart';
import 'package:redux_conceptions/services/unspash_users_service.dart';

class UnsplashUserRepository {
  UnsplashUserRepository._privateConstructor();

  static final UnsplashUserRepository instance =
      UnsplashUserRepository._privateConstructor();

  Future<List<UserDtO>> getUsers() async {
    var responseData = await UnsplashUserService.instance.getUsers();
    List<UserDtO> users = [];
    for (var item in responseData.response) {
      users.add(UserDtO.fromJson(item));
    }
    return users;
  }
}
