import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/images_state/images_action.dart';
import 'package:redux_conceptions/store/repositories/unsplash_image_repository.dart';
import 'package:redux_conceptions/store/users_state/users_action.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class ImagesEpics {
  static final indexEpic = combineEpics<AppState>([
    getImagesEpic,
  ]);

  static Stream<dynamic> getImagesEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetImagesAction>().switchMap(
      (action) {
        return Stream.fromFuture(
          UnsplashImageRepository.instance.getUsers().then(
            (List<Image> images) {
              if (images == null) {
                return EmptyAction();
              }
              return SaveImagesAction(
                images: images,
              );
            },
          ),
        );
      },
    );
  }
}
