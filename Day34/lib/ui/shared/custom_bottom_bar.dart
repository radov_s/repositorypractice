import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/navigation_service/navigation_viewmodel.dart';
import 'package:redux_conceptions/navigation_service/routes.dart';
import 'package:redux_conceptions/store/app/app_state.dart';

class CustomBottomBar extends StatelessWidget {
  const CustomBottomBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: (Store<dynamic> store) => NavigationViewModel.fromStore(store),
      builder: (BuildContext context, NavigationViewModel vm) {
        Widget _buildButton(
            Function function, IconData icon, String label, String nameRoute,
            {double size = 27}) {
          return InkWell(
            onTap: () {
              function();
            },
            child: Column(
              children: [
                Icon(
                  icon,
                  size: size,
                  color: vm.getCurrentPage == nameRoute
                      ? Colors.black
                      : Colors.grey,
                ),
                Text(
                  label,
                ),
              ],
            ),
          );
        }

        return SizedBox(
          height: 60.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton(
                vm.goToUserPage,
                Icons.read_more,
                'User',
                Routes.usersPage,
              ),
              _buildButton(
                vm.goToImagePage,
                Icons.add,
                'Image',
                Routes.imagesPage,
              ),
            ],
          ),
        );
      },
    );
  }
}
