import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/dto/user_dto.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/users_state/users_selector.dart';

class UsersPageViewModel {
  final List<UserDtO> users;
  final UserDtO user;
  final void Function() getUsers;

  UsersPageViewModel({
    @required this.users,
    @required this.getUsers,
    @required this.user,
  });

  static UsersPageViewModel fromStore(Store<AppState> store) {
    return UsersPageViewModel(
      users: UsersSelector.getCurrentUsers(store),
      user: UsersSelector.getSelectedUser(store),
      getUsers: UsersSelector.getUsersFunction(store),
    );
  }
}
