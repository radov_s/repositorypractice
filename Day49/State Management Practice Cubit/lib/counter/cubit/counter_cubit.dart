import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class CounterCubit extends Cubit<Color> {
  /// {@macro counter_cubit}
  CounterCubit() : super(Colors.white);

  /// Add 1 to the current state.
  void redEvent() => emit(Colors.red);
  void greenEvent() => emit(Colors.green);
  void blueEvent() => emit(Colors.blueAccent);

}