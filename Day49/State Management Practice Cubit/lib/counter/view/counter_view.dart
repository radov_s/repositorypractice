import 'package:bloc_practice_1/counter/cubit/counter_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounterView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CounterCubit, Color>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: state,
          appBar: AppBar(title: const Text('Color Cubit')),
          drawer: Drawer(
            child: SafeArea(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FloatingActionButton(
                    backgroundColor: Colors.red,
                    onPressed: () {
                      context.read<CounterCubit>().redEvent();
                    },
                  ),
                  FloatingActionButton(
                    backgroundColor: Colors.green,
                    onPressed: () {
                      context.read<CounterCubit>().greenEvent();
                    },
                  ),
                  FloatingActionButton(
                    backgroundColor: Colors.blueAccent,
                    onPressed: () {
                      context.read<CounterCubit>().blueEvent();
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
