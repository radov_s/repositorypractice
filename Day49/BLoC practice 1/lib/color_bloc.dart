import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum CounterEvent{
  incrementEvent,
  decrementEvent,
  resetEvent,
}

class CounterBloc extends Bloc<CounterEvent, double> {
  CounterBloc(double initialState) : super(initialState);

  double counter = 0;

  @override
  Stream<double> mapEventToState(event) async* {
    if (event == CounterEvent.incrementEvent) {
      counter++;
    }
    if (event == CounterEvent.decrementEvent) {
      counter--;
    }
    if (event == CounterEvent.resetEvent) {
      counter = 0;
    }
    yield counter;
  }
}

enum ColorEvent {
  redColor,
  greenColor,
  blueColor,
}

class ColorBloc extends Bloc<ColorEvent, Color> {
  ColorBloc(Color initialState) : super(initialState);

  Color color = Colors.black;

  @override
  Stream<Color> mapEventToState(ColorEvent event) async* {
    if (event == ColorEvent.redColor) {
      color = Colors.red;
    }
    else if (event == ColorEvent.greenColor) {
      color = Colors.green;
    }
    else if (event == ColorEvent.blueColor){
      color = Colors.blueAccent;
    }
    yield color;
  }
}