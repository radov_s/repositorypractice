import 'package:bloc_example/color_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context) => CounterBloc(0),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    CounterBloc _bloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: BlocBuilder<CounterBloc, double>(
          builder: (context, _counter) => Text('$_counter'),
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              _bloc.add(CounterEvent.incrementEvent);
            },
            child: Icon(Icons.add),
          ),
          const SizedBox(width: 20.0),
          FloatingActionButton(
            onPressed: () {
              _bloc.add(CounterEvent.decrementEvent);
            },
            child: Icon(Icons.remove),
          ),
          const SizedBox(width: 20.0),
          FloatingActionButton(
            onPressed: () {
              _bloc.add(CounterEvent.resetEvent);
            },
            child: Icon(Icons.exposure_zero),
          ),
        ],
      ),
    );
  }
}
