import 'package:flutter_bloc/flutter_bloc.dart';

enum CounterEvent{
  incrementEvent,
  decrementEvent,
  resetEvent,
}

class CounterBloc extends Bloc<CounterEvent, double> {
  CounterBloc(double initialState) : super(initialState);

  double counter = 0;

  @override
  Stream<double> mapEventToState(event) async* {
    if (event == CounterEvent.incrementEvent) {
      counter++;
    }
    if (event == CounterEvent.decrementEvent) {
      counter--;
    }
    if (event == CounterEvent.resetEvent) {
      counter = 0;
    }
    yield counter;
  }
}
