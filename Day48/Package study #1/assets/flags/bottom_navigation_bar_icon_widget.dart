import 'package:flutter/material.dart';

class BottomNavigationBarIcon extends StatelessWidget {
  final IconData icon;
  final Color color;
  final double size;

  BottomNavigationBarIcon({
    @required this.icon,
    @required this.color,
    @required this.size,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 30.0,
      height: 30.0,
      child: Center(
        child: Icon(
          icon,
          size: size,
          color: color,
        ),
      ),
    );
  }
}
