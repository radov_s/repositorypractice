import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/about_us/about_us_page.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/create_record_page.dart';
import 'package:Provider_template_v1/ui/pages/history_page/history_page.dart';
import 'package:Provider_template_v1/ui/pages/language_page/language_page.dart';
import 'package:Provider_template_v1/ui/pages/main_page/main_page.dart';
import 'package:Provider_template_v1/ui/pages/settings_page/settings_page.dart';
import 'package:Provider_template_v1/ui/pages/support/unknown_page.dart';
import 'package:flutter/material.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // endregion

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case (Routes.mainPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => MainPage(), settings: RouteSettings(name: Routes.mainPage,));
      case (Routes.settingsPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => SettingsPage(),settings: RouteSettings(name: Routes.settingsPage,));
      case (Routes.languagePage) : return PageRouteBuilder(pageBuilder: (context, _, __) => LanguagePage(),settings: RouteSettings(name: Routes.languagePage,));
      case (Routes.historyPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => HistoryPage(),settings: RouteSettings(name: Routes.historyPage,));
      case (Routes.createRecordPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => CreateRecordPage(),settings: RouteSettings(name: Routes.createRecordPage,));
      case (Routes.aboutUsPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => AboutUsPage(), settings: RouteSettings(name: Routes.aboutUsPage));
      default:
        return _defaultRoute(
          settings: settings,
          page: UnknownPage(),
        );
    }
  }

  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
