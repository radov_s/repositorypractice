import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/language_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:flutter/cupertino.dart';

class Language {
  final MainPageLanguage mainPageLanguage;
  final SettingsPageLanguage settingsPageLanguage;
  final LanguagePageLanguage languagePageLanguage;
  final HistoryPageLanguage historyPageLanguage;
  final CreateRecordPageLanguage createRecordPageLanguage;
  final AboutUsPageLanguage aboutUsPageLanguage;

  const Language({
    @required this.mainPageLanguage,
    @required this.settingsPageLanguage,
    @required this.languagePageLanguage,
    @required this.historyPageLanguage,
    @required this.createRecordPageLanguage,
    @required this.aboutUsPageLanguage,
  });
}
