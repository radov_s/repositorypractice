import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/language_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/models/language.dart';

const Language it = Language(
  mainPageLanguage: MainPageLanguage(
    createYourFirstNote: 'Crea la tua prima nota!',
  ),
  settingsPageLanguage: SettingsPageLanguage(
    title: 'Impostazioni',
    language: 'Linguaggio',
    aboutUs: 'Chi siamo',
  ),
  languagePageLanguage: LanguagePageLanguage(
    title: 'Linguaggio',
  ),
  historyPageLanguage: HistoryPageLanguage(
    searchFieldHintText: 'Cerca cliente o servizio',
    title: 'Storia',
  ),
  createRecordPageLanguage: CreateRecordPageLanguage(
      save: 'Salva',
      cancel: 'Annulla',
      create: 'Creare',
      color: 'Colore',
      nameInputLabel: 'Nome',
      dateLabel: 'Data di arrivo',
      priceLabel: 'Prezzo',
      timeBeginLabel: 'Il tempo inizia',
      timeEndLabel: 'Fine del tempo',
      descriptionLabel: 'Descrizione',
      serviceLabel: 'Servizio',
      add: 'Inserisci',
      addInputLabel: 'Aggiungi nuovo servizio',
      title: 'Crea record'),
);
