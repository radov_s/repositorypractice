import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/language_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/models/language.dart';

const Language fr = Language(
  mainPageLanguage: MainPageLanguage(
    createYourFirstNote: 'Créez votre première note!',
  ),
  settingsPageLanguage: SettingsPageLanguage(
    title: 'Paramètres',
    language: 'Langue',
    aboutUs: 'Aà propos de nous',
  ),
  languagePageLanguage: LanguagePageLanguage(
    title: 'Langue',
  ),
  historyPageLanguage: HistoryPageLanguage(
    searchFieldHintText: 'Rechercher un client ou un service',
    title: 'Histoire',
  ),
  createRecordPageLanguage: CreateRecordPageLanguage(
      save: 'Sauvegarder',
      cancel: 'Annuler',
      create: 'Créer',
      color: 'Couleur',
      nameInputLabel: 'Nom',
      dateLabel: 'Date venir',
      priceLabel: 'Prix',
      timeBeginLabel: 'Heure de début',
      timeEndLabel: 'Fin de l\'heure',
      descriptionLabel: 'La description',
      serviceLabel: 'Un service',
      add: 'Ajouter',
      addInputLabel: 'Ajouter un nouveau service',
      title: 'Créer un enregistrement'),
  aboutUsPageLanguage: AboutUsPageLanguage(
      backArrowSettings: 'null',
      titleAboutUs: 'null',
      textDescription: 'null',
      writeUs: 'Écrivez-nous',
      name: 'null',
      email: 'null',
      message: 'null',
      sendButton: 'null'),
);
