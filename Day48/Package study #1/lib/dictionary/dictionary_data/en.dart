import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/language_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/models/language.dart';
import 'package:Provider_template_v1/ui/pages/language_page/language_page.dart';

const Language en = Language(
  mainPageLanguage: MainPageLanguage(
    createYourFirstNote: 'Create your first note!',
  ),
  settingsPageLanguage: SettingsPageLanguage(
    title: 'Settings',
    language: 'Language',
    aboutUs: 'About us',
  ),
  languagePageLanguage: LanguagePageLanguage(
    title: 'Language',
  ),
  historyPageLanguage: HistoryPageLanguage(
    searchFieldHintText: 'Search client or service',
    title: 'History',
  ),
  createRecordPageLanguage: CreateRecordPageLanguage(
      save: 'Save',
      cancel: 'Cancel',
      create: 'Create',
      color: 'Color',
      nameInputLabel: 'Name',
      dateLabel: 'Date come',
      priceLabel: 'Price',
      timeBeginLabel: 'Time begin',
      timeEndLabel: 'Time end',
      descriptionLabel: 'Description',
      serviceLabel: 'Service',
      add: 'Add',
      addInputLabel: 'Add new service',
      title: 'Create Record'),
  aboutUsPageLanguage: AboutUsPageLanguage(
    writeUs: 'Write us'

  )

);
