import 'package:flutter/material.dart';

class SettingsPageLanguage {
  final String title;
  final String language;
  final String aboutUs;

  const SettingsPageLanguage({
    @required this.title,
    @required this.language,
    @required this.aboutUs,
  });
}
