import 'package:flutter/cupertino.dart';

class MainOnLanguage {
  final String titleTop1;
  final String titleTop2;
  final String titleTop3;
  final String buttonNameMiss;

  const MainOnLanguage({
    @required this.titleTop1,
    @required this.titleTop2,
    @required this.titleTop3,
    @required this.buttonNameMiss,
  });

}
