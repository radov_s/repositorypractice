import 'package:flutter/cupertino.dart';

class AboutUsPageLanguage {
  final String backArrowSettings;
  final String titleAboutUs;
  final String textDescription;
  final String writeUs;
  final String name;
  final String email;
  final String message;
  final String sendButton;

  const AboutUsPageLanguage({
    @required this.backArrowSettings,
    @required this.titleAboutUs,
    @required this.textDescription,
    @required this.writeUs,
    @required this.name,
    @required this.email,
    @required this.message,
    @required this.sendButton,
  });
}
