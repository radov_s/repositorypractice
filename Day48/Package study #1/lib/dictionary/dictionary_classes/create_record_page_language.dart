import 'package:flutter/cupertino.dart';

class CreateRecordPageLanguage {
  final String title;
  final String nameInputLabel;
  final String dateLabel;
  final String priceLabel;
  final String timeBeginLabel;
  final String timeEndLabel;
  final String descriptionLabel;
  final String serviceLabel;
  final String add;
  final String addInputLabel;
  final String color;
  final String create;
  final String cancel;
  final String save;

  const CreateRecordPageLanguage({
    @required this.nameInputLabel,
    @required this.dateLabel,
    @required this.priceLabel,
    @required this.timeBeginLabel,
    @required this.timeEndLabel,
    @required this.descriptionLabel,
    @required this.serviceLabel,
    @required this.add,
    @required this.addInputLabel,
    @required this.title,
    @required this.color,
    @required this.create,
    @required this.cancel,
    @required this.save,
  });
}
