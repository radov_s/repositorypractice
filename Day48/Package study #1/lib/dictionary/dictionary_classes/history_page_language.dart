import 'package:flutter/cupertino.dart';

class HistoryPageLanguage {
  final String title;
  final String searchFieldHintText;

 const HistoryPageLanguage({
    @required this.title,
    @required this.searchFieldHintText,
  });
}
