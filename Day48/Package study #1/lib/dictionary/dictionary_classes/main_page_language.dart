import 'package:flutter/cupertino.dart';

class MainPageLanguage {
  final String createYourFirstNote;

  const MainPageLanguage({
    @required this.createYourFirstNote,
  });
}
