import 'package:flutter/cupertino.dart';

class MainEmptyLanguage {
  final String firstNoteTextDescription;
  final String loadingTextInSplashScreen;

  const MainEmptyLanguage({
    @required this.firstNoteTextDescription,
    @required this.loadingTextInSplashScreen,
  });

}
