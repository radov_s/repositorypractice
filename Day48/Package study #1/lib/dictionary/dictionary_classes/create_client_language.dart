import 'package:flutter/cupertino.dart';

class CrateClientLanguage {
  final String titleCrateClient;
  final String name;
  final String dateCome;
  final String price;
  final String timeBegin;
  final String timeEnd;
  final String services;
  final String deleteServicesButtonText;
  final String addServicesButtonText;
  final String addNewServicesText;
  final String description;
  final String colorText;
  final String createButtonText;


  const CrateClientLanguage({
    @required this.titleCrateClient,
    @required this.name,
    @required this.dateCome,
    @required this.price,
    @required this.timeBegin,
    @required this.timeEnd,
    @required this.services,
    @required this.deleteServicesButtonText,
    @required this.addServicesButtonText,
    @required this.addNewServicesText,
    @required this.description,
    @required this.colorText,
    @required this.createButtonText,
  });
}
