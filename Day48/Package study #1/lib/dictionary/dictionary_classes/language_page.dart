import 'package:flutter/cupertino.dart';

class LanguagePage {
  final String backArrowSettings;
  final String titleLanguage;


  const LanguagePage({
    @required this.backArrowSettings,
    @required this.titleLanguage,
  });

}
