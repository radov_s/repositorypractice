import 'package:flutter/cupertino.dart';

class SettingsLanguage {
  final String backArrowText;
  final String titleSettings;
  final String buttonTextLanguage;
  final String buttonAboutUs;

  const SettingsLanguage({
    @required this.backArrowText,
    @required this.titleSettings,
    @required this.buttonTextLanguage,
    @required this.buttonAboutUs,
  });

}
