import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/images.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class CarouselHelper extends StatefulWidget {
  @override
  _CarouselHelperState createState() => _CarouselHelperState();
}

class _CarouselHelperState extends State<CarouselHelper> {
  int _current = 0;

  var list = [
    AssetImages.helpScreenFirst,
    AssetImages.helpScreenSecond,
    AssetImages.helpScreenThird,
  ];
  var listText = [
    'Here you can see your clients, delete, edit and view all information when clicking on a client ',
    'Tap for button to create a record',
    'Fill in your customer data, choose the date, price, service'
  ];

  @override
  Widget build(BuildContext context) {
    Widget _buildCircle(int index) {
      return Container(
        width: 8.0,
        height: 8.0,
        margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 8.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _current == index
                ? AppColors.kWhite
                : AppColors.kWhite.withOpacity(0.5)),
      );
    }

    return Material(
      color: AppColors.kBlack.withOpacity(0.9),
      child: InkWell(
        onTap: () => Navigator.of(context).pop(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CarouselSlider.builder(
              itemCount: 3,
              height: 450.0,
              viewportFraction: 1.0,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                });
              },
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 22.0, vertical: 16.0),
                      child: Center(
                        child: Text(
                          listText[index],
                          style: FontStyles.onBoardingText,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 31.0),
                      child: Image.asset(
                        list[index],
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                );
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildCircle(0),
                _buildCircle(1),
                _buildCircle(2),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
