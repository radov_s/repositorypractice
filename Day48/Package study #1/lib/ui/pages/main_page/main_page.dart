import 'package:Provider_template_v1/data/models/message.dart';
import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/shared/buttons/plus_button.dart';
import 'package:Provider_template_v1/ui/pages/shared/custom_app_bar.dart';
import 'package:Provider_template_v1/ui/pages/shared/message_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  final MainPageLanguage language = FlutterDictionary.instance.language?.mainPageLanguage ?? en.mainPageLanguage;

  @override
  Widget build(BuildContext context) {
    CreateRecordProvider createRecordProvider = Provider.of<CreateRecordProvider>(context);
    double screenWidth = MediaQuery.of(context).size.width;


    List<Message> currentList = [];

    for (Message message in createRecordProvider.messageList) {
      if (DateFormat.yMd().format(message.dateCome) == DateFormat.yMd().format(createRecordProvider.mainPageChosenDay)) {
        currentList.add(message);
      }
    }
    return MainLayout(
      appBar: CustomAppbar(),
      bottomNavBar: true,
      body: Stack(
        fit: StackFit.expand,
        children: [
          currentList.length <= 0
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Text(
                            language.createYourFirstNote,
                            style: FontStyles.mainPageEmptyText,
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 13.0,
                    ),
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.only(right: 60.0, left: 30.0, bottom: 79.0),
                      child: Image.asset(
                        'assets/illustration.png',
                        fit: BoxFit.contain,
                      ),
                    )),
                  ],
                )
              : SingleChildScrollView(
                  child: Column(
                    children: [...currentList.map((message) => MessageWidget(message: message))],
                  ),
                ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: PlusButton(),
            ),
          ),
        ],
      ),
    );
  }
}
