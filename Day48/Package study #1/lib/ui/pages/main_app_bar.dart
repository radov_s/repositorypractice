import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/icons/app_bar_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'main_page/carousel_helper.dart';

class MainAppBar extends PreferredSize {
  final double paddingTop;

  MainAppBar(this.paddingTop);

  Size get preferredSize => Size.fromHeight(100 - paddingTop);

  static Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('Popup example'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Hello"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarColor: Colors.transparent),
    );
    return Container(
      padding: EdgeInsets.only(top: paddingTop),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            flex: 1,
            child: SizedBox(
              width: 24,
            ),
          ),
          Expanded(
            flex: 1,
            child: InkWell(
              child: Icon(Icons.keyboard_arrow_left),
              onTap: () {},
            ),
          ),
          Stack(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Text('Friday', style: FontStyles.appBarDayText),
              ),
              Positioned(
                right: 1.0,
                top: -5,
                child: Text(
                  '31',
                  style: FontStyles.appBarDayText,
                ),
              ),
            ],
          ),
          Expanded(
            flex: 1,
            child: InkWell(
              child: Icon(Icons.keyboard_arrow_right),
              onTap: () {},
            ),
          ),
          Expanded(
            flex: 1,
            child: InkWell(
              child: Icon(
                AppBarInfo.info,
                size: 24,
              ),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => CarouselHelper(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
