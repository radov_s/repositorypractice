import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';

class PickerButton extends StatelessWidget {
  final Function pickerFunction;
  final String pickerLabel;
  final String value;
  final Widget child;

  const PickerButton({
    this.pickerFunction,
    @required this.pickerLabel,
    this.value,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 33.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            pickerLabel,
            style: FontStyles.createRecordPagePickerLabelText,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4.0, right: 32.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: SizedBox(
                width: 108.0,
                height: 36.0,
                child: Material(
                  color: AppColors.dodgerBlueColor,
                  child: InkWell(
                    onTap: pickerFunction,
                    child: Center(
                      child: value != null ? Text(
                        value,
                        style: FontStyles.createRecordPagePickerText,
                      ) : child,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
