import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/data/providers/date_picker_provider.dart';
import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/layouts/focus_layout.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/custom_picker/endtime_picker_child.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/custom_picker/price_popup_child.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/picker_button.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/custom_picker/time_picker_child.dart';
import 'package:Provider_template_v1/ui/pages/shared/buttons/create_button.dart';
import 'package:Provider_template_v1/ui/pages/shared/standart_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'color_picker/color_picker.dart';
import 'custom_picker/custom_picker.dart';
import 'custom_picker/date_picker_child.dart';

class CreateRecordPage extends StatefulWidget {
  @override
  _CreateRecordPageState createState() => _CreateRecordPageState();
}

class _CreateRecordPageState extends State<CreateRecordPage> {
  bool serviceIsEmpty = false;
  TextEditingController serviceController = TextEditingController();
  bool nameIsEmpty = false;
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  CreateRecordPageLanguage language = FlutterDictionary.instance.language?.createRecordPageLanguage ?? en.createRecordPageLanguage;

  @override
  Widget build(BuildContext context) {
    NavigationProvider navigationProvider = Provider.of<NavigationProvider>(context, listen: false);
    DatePickerProvider datePickerProvider = Provider.of<DatePickerProvider>(context);
    CreateRecordProvider createRecordProvider = Provider.of<CreateRecordProvider>(context);
    return FocusLayout(
      child: MainLayout(
        appBar: SharedAppBar(
          appBarTitle: language.title,
        ),
        bottomNavBar: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextField(
                controller: nameController,
                decoration: InputDecoration(
                  errorText: nameIsEmpty ? "This field can't be empty" : null,
                  helperStyle: FontStyles.createRecordPagePickerLabelText,
                  labelText: language.nameInputLabel,
                ),
              ),
              Row(
                children: [
                  PickerButton(
                    value: DateFormat('dd.MM').format(datePickerProvider.pickedDate),
                    pickerFunction: () {
                      showDialog(
                        context: context,
                        builder: (context) => CustomPicker(
                          pickerTitle: language.dateLabel,
                          child: DatePickerChild(),
                          cancelButtonFunction: () => datePickerProvider.setPickedDate(),
                        ),
                      );
                    },
                    pickerLabel: language.dateLabel,
                  ),
                 PickerButton(
                    pickerFunction: () {
                      showDialog(
                        context: context,
                        builder: (context) => CustomPicker(
                          pickerTitle: language.priceLabel,
                          child: PricePopupChild(),
                          cancelButtonFunction: () => createRecordProvider.changePrice(''),
                        ),
                      );
                    },
                    pickerLabel: language.priceLabel,
                    value: createRecordProvider.price.toString(),
                  ),
                ],
              ),
              Row(
                children: [
                  PickerButton(
                    value: DateFormat('H.mm').format(datePickerProvider.pickedStartTime),
                    pickerFunction: () {
                      showDialog(
                        context: context,
                        builder: (context) => CustomPicker(
                          pickerTitle: language.timeBeginLabel,
                          child: TimePickerChild(),
                          cancelButtonFunction: () => datePickerProvider.setPickedStartTime(),
                        ),
                      );
                    },
                    pickerLabel: language.timeBeginLabel,
                  ),
                  PickerButton(
                    value: DateFormat('H.mm').format(datePickerProvider.pickedEndTime),
                    pickerFunction: () {
                      showDialog(
                        context: context,
                        builder: (context) => CustomPicker(
                          cancelButtonFunction: () {},
                          pickerTitle: language.timeEndLabel,
                          child: EndTimePickerChild(),
                        ),
                      );
                    },
                    pickerLabel: language.timeEndLabel,
                  ),
                ],
              ),
              TextField(
                controller: serviceController,
                decoration: InputDecoration(
                  errorText: serviceIsEmpty ? "This field can't be empty" : null,
                  helperStyle: FontStyles.createRecordPagePickerLabelText,
                  labelText: language.serviceLabel,
                ),
              ),
              const SizedBox(height: 54.0),
              TextFormField(
                controller: descriptionController,
                decoration: InputDecoration(
                  labelText: language.descriptionLabel,
                  alignLabelWithHint: true,
                  floatingLabelBehavior: FloatingLabelBehavior.auto,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: AppColors.blackColor.withOpacity(0.2),
                    ),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: AppColors.blackColor.withOpacity(0.2),
                    ),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                ),
                maxLines: 5,
              ),
              const SizedBox(height: 32.0),
              Text(
                language.color,
                style: FontStyles.createRecordPagePickerLabelTextBlack,
              ),
              const SizedBox(height: 4.0),
              ColorPicker(),
              const SizedBox(height: 40.0),
              CreateButton(
                buttonFunction: () {
                  createRecordProvider.description = descriptionController.text;
                  if (nameController.text.isEmpty) {
                    setState(() {
                      nameIsEmpty = true;
                    });
                  } else {
                    createRecordProvider.name = nameController.text;
                    setState(() {
                      nameIsEmpty = false;
                    });
                  }
                  if (serviceController.text.isEmpty) {
                    setState(() {
                      serviceIsEmpty = true;
                    });
                  } else {
                    createRecordProvider.service = serviceController.text;
                    setState(() {
                      serviceIsEmpty = false;
                    });
                  }
                  if (nameIsEmpty == false && serviceIsEmpty == false) {
                    createRecordProvider.addMessage();
                    navigationProvider.navigateToPage(Routes.mainPage, context);
                  }
                },
                label: language.create,
              ),
              const SizedBox(height: 32.0),
            ],
          ),
        ),
      ),
    );
  }
}
