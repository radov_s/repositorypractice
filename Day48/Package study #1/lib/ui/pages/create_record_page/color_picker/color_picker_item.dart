import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ColorPickerItem extends StatelessWidget {
  final LinearGradient linearGradient;

  const ColorPickerItem({this.linearGradient});

  @override
  Widget build(BuildContext context) {
    CreateRecordProvider createRecordProvider =
        Provider.of<CreateRecordProvider>(context);
    return Padding(
      padding: EdgeInsets.only(right: 16.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: SizedBox(
          height: 42.0,
          width: 42.0,
          child: Material(
            child: DecoratedBox(
              decoration: BoxDecoration(
                gradient: linearGradient
              ),
              child: InkWell(
                onTap: () {
                  createRecordProvider.pickedGradient = linearGradient;
                },
                child: createRecordProvider.pickedGradient == linearGradient
                    ? Center(
                        child: Icon(
                          Icons.done_outlined,
                          size: 25.0,
                          color: AppColors.whiteColor,
                        ),
                      )
                    : SizedBox(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
