import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:flutter/material.dart';

import 'color_picker_item.dart';

class ColorPicker extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          ColorPickerItem(
            linearGradient: AppColors.dodgerBlueToDodgerBlue,
          ),
          ColorPickerItem(
            linearGradient: AppColors.brightBluSkyToBrightBlue,
          ),
          ColorPickerItem(
            linearGradient: AppColors.slateToDark,
          ),
          ColorPickerItem(
            linearGradient: AppColors.hotPinkToSalmon,
          ),
          ColorPickerItem(
            linearGradient: AppColors.neonPurpleToBluishPurple,
          ),
          ColorPickerItem(
            linearGradient: AppColors.tealishToBlueberry,
          ),
          ColorPickerItem(
            linearGradient: AppColors.blueberryToHotPink,
          ),
        ],
      ),
    );
  }
}
