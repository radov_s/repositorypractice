import 'package:Provider_template_v1/data/providers/date_picker_provider.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class DatePickerElement extends StatelessWidget {
  FixedExtentScrollController controller;
  int initialItem;
  Function onChangeFunction;
  int listLength;
  int indexCorrector;

  DatePickerElement({
    @required this.controller,
    @required this.initialItem,
    @required this.onChangeFunction,
    @required this.listLength,
    @required this.indexCorrector,
    // @required this.condition,
  });

  @override
  Widget build(BuildContext context) {
    DatePickerProvider datePickerProvider =
        Provider.of<DatePickerProvider>(context);
    this.controller = FixedExtentScrollController(initialItem: initialItem);
    return Container(
      height: 180,
      width: 90,
      child: ListWheelScrollView(
        controller: controller,
        itemExtent: 41,
        diameterRatio: 300,
        useMagnifier: true,
        overAndUnderCenterOpacity: 0.5,
        physics: FixedExtentScrollPhysics(),
        magnification: 1.33,
        onSelectedItemChanged: (item) {
          onChangeFunction(item);
          if (datePickerProvider.pickedDate
                  .isBefore(datePickerProvider.initialDate) ||
              datePickerProvider.pickedStartTime
                  .isBefore(datePickerProvider.initialTime)) {
            controller.jumpToItem(initialItem);
          }
          print("pickedTime${datePickerProvider.pickedDate}");
          print("time for record${DateTime.now()}");
        },
        children: [
          ...List.generate(
            listLength,
            (item) => Center(
              child: Text(
                (item + indexCorrector).toString(),
                style: FontStyles.datePickerDatesText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
