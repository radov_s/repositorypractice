import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomPicker extends StatelessWidget {
  final Widget child;
  final String pickerTitle;
  final Function cancelButtonFunction;

  CustomPicker(
      {@required this.child,
      @required this.pickerTitle,
      @required this.cancelButtonFunction});

  CreateRecordPageLanguage language =
      FlutterDictionary.instance.language?.createRecordPageLanguage ??
          en.createRecordPageLanguage;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            gradient: AppColors.timePickerBackground,
          ),
          height: 371.0,
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.all(24.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          cancelButtonFunction();
                          Navigator.pop(context);
                        },
                        child: Text(
                          language.cancel,
                          style: FontStyles.datePickerCancelText,
                        ),
                      ),
                    ),
                    Text(
                      pickerTitle,
                      style: FontStyles.datePickerTimeBeginText,
                    ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          language.save,
                          style: FontStyles.datePickerSaveText,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  height: 50.0,
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      border: Border.symmetric(
                        horizontal:
                            BorderSide(color: AppColors.whiteColor, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child: child,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
