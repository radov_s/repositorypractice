import 'package:Provider_template_v1/data/providers/date_picker_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'date_picker_element.dart';

class TimePickerChild extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    DatePickerProvider datePickerProvider =
    Provider.of<DatePickerProvider>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DatePickerElement(
          controller: datePickerProvider.hourController,
          initialItem: datePickerProvider.hour,
          listLength: 24 ,
          onChangeFunction: datePickerProvider.changeHour,
          indexCorrector: 0,
        ),
        DatePickerElement(
          controller: datePickerProvider.minuteController,
          initialItem: datePickerProvider.minute,
          listLength: 60 ,
          onChangeFunction: datePickerProvider.changeMinute,
          indexCorrector: 0,
        ),
      ],
    );
  }
}
