import 'package:Provider_template_v1/data/providers/date_picker_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'date_picker_element.dart';

class EndTimePickerChild extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DatePickerProvider datePickerProvider =
        Provider.of<DatePickerProvider>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DatePickerElement(
          controller: datePickerProvider.hourController,
          initialItem: datePickerProvider.endHour - 1,
          listLength: 24,
          onChangeFunction: datePickerProvider.changeEndHour,
          indexCorrector: 0,
        ),
        DatePickerElement(
          controller: datePickerProvider.minuteController,
          initialItem: datePickerProvider.endMinute,
          listLength: 60,
          onChangeFunction: datePickerProvider.changeEndMinute,
          indexCorrector: 0,
        ),
      ],
    );
  }
}
