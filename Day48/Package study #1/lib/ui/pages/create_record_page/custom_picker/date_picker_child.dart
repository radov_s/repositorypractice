import 'package:Provider_template_v1/data/providers/date_picker_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:date_utils/date_utils.dart';

import 'date_picker_element.dart';


class DatePickerChild extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    DatePickerProvider datePickerProvider =
    Provider.of<DatePickerProvider>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DatePickerElement(
          controller: datePickerProvider.monthController,
          initialItem: datePickerProvider.month-1,
          listLength: 12 ,
          onChangeFunction: datePickerProvider.changeMonth,
          indexCorrector: 1,
        ),
        DatePickerElement(
          controller: datePickerProvider.dayController,
          initialItem: datePickerProvider.day-1,
          listLength: Utils.lastDayOfMonth(datePickerProvider.pickedDate).day,
          onChangeFunction: datePickerProvider.changeDay,
          indexCorrector: 1,
        ),
        DatePickerElement(
          controller: datePickerProvider.yearController,
          initialItem: datePickerProvider.year - datePickerProvider.initialYear ,
          listLength: datePickerProvider.endYear - datePickerProvider.initialYear,
          onChangeFunction: datePickerProvider.changeYear,
          indexCorrector: datePickerProvider.initialYear,
        ),
      ],
    );
  }
}
