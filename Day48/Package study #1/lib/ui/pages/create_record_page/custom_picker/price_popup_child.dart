import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class PricePopupChild extends StatelessWidget {

  TextEditingController priceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    CreateRecordProvider createRecordProvider = Provider.of<CreateRecordProvider>(context);
    return TextField(
      controller: priceController..text = createRecordProvider.price,
      autofocus: true,
      decoration: InputDecoration(
          border: InputBorder.none
      ),
      inputFormatters: [LengthLimitingTextInputFormatter(8)],
      style: FontStyles.datePickerDatesText,
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      onChanged: (value) {
        print(value);
        createRecordProvider.changePrice(value);
        print(createRecordProvider.price);
      },
    );
  }
}
