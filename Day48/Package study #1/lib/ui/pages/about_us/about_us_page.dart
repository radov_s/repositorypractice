import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/images.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/shared/standart_app_bar.dart';
import 'package:flutter/material.dart';

class AboutUsPage extends StatelessWidget {
  static AboutUsPageLanguage language =
      FlutterDictionary.instance.language?.aboutUsPageLanguage ??
          en.aboutUsPageLanguage;


  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: SharedAppBar(
        appBarTitle: language.writeUs,
      ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Image.asset(AssetImages.aboutUs),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis at tellus at urna condimentum mattis pellentesque id. Nulla facilisi nullam vehicula ipsum a. Tincidunt eget nullam non nisi est sit. Sit amet massa vitae tortor condimentum lacinia. Sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit. Sem et tortor consequat id porta nibh venenatis cras. Nisi est sit amet facilisis magna etiam.',
                  style: FontStyles.createRecordPagePickerLabelTextBlack,),
                Text(language.writeUs),
                TextFormField(),
                TextFormField(),
                TextFormField(
                  maxLines: 5,
                ),
                const SizedBox(
                  height: 25.0,
                ),
                Container(
                  width: double.infinity,
                  height: 50.0,
                  decoration: BoxDecoration(
                      color: AppColors.kDodgerBlue,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: InkWell(
                    child: Center(
                        child: Text('SEND', style: FontStyles.messageText )),
                    onTap: () => print('tap'),
                  ),
                ),
              ],
            ),
          ),
        ), bottomNavBar: false,
    );
  }
}
