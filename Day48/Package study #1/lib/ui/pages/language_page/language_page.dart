import 'package:Provider_template_v1/data/providers/language_provider.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/language_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/dictionary/models/supported_locales.dart';
import 'package:Provider_template_v1/res/images.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/shared/standart_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'language_page_checkbox.dart';

// ignore: must_be_immutable
class LanguagePage extends StatelessWidget {
  LanguagePageLanguage language = FlutterDictionary.instance.language?.languagePageLanguage ?? en.languagePageLanguage;
  List<Locale> localeList = SupportedLocales.instance.getSupportedLocales;
  String currentLocale = SupportedLocales.instance.getCurrentLocale;

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageProvider>(
      builder: (BuildContext context, LanguageProvider languageProvider, Widget _) => MainLayout(
        appBar: SharedAppBar(
          appBarTitle: language.title,
        ),
        bottomNavBar: true,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () => {
                        languageProvider.setNewLang(localeList.firstWhere((element) => element.languageCode == 'en')),
                      },
                  child: LanguagePageItem(
                    itemLanguageCode: 'en',
                    currentLocale: currentLocale,
                    flagImage: AssetImages.flagGB,
                    itemName: 'English',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () => {
                        languageProvider.setNewLang(localeList.firstWhere((element) => element.languageCode == 'fr')),
                      },
                  child: LanguagePageItem(
                    itemLanguageCode: 'fr',
                    currentLocale: currentLocale,
                    flagImage: AssetImages.flagFR,
                    itemName: 'France',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () => {
                  languageProvider.setNewLang(localeList.firstWhere((element) => element.languageCode == 'it')),
                },
                child: LanguagePageItem(
                  itemLanguageCode: 'it',
                  currentLocale: currentLocale,
                  flagImage: AssetImages.flagIT,
                  itemName: 'Italia',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
