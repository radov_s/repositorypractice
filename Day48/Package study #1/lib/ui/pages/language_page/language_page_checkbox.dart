import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';

class LanguagePageItem extends StatelessWidget {
  final String currentLocale;
  final String itemLanguageCode;
  final String itemName;
  final String flagImage;

  const LanguagePageItem({
    @required this.currentLocale,
    @required this.itemLanguageCode,
    @required this.itemName,
    @required this.flagImage,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 128,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Image.asset(
                flagImage,
                height: 20.0,
                width: 28.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 9.0),
                child: Text(
                  itemName,
                  style: FontStyles.languagePageText,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 24.0,
            width: 24.0,
            child: DecoratedBox(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  border: Border.all(width: 1.0, color: Colors.grey)),
              child: currentLocale == itemLanguageCode
                  ? Center(
                      child: SizedBox(
                        height: 10,
                        width: 10,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              color: AppColors.dodgerBlueColor,
                              shape: BoxShape.circle),
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
          ),
        ],
      ),
    );
  }
}
