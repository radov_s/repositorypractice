import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/pages/settings_page/settings_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SharedAppBar extends StatelessWidget with PreferredSizeWidget {
  final String appBarTitle;

  const SharedAppBar({@required this.appBarTitle});

   String prevPage(condition, context) {
    switch (condition) {
      case (Routes.mainPage):
        return toBeginningOfSentenceCase(
            '${DateFormat('EEEE', Localizations.localeOf(context).toString()).format(DateTime.now())}');
      case (Routes.languagePage):
        return ' ';
      case (Routes.historyPage):
        return 'History';
      case (Routes.settingsPage):
        return 'Setting';
      case (Routes.createRecordPage):
        return 'Create Record';
      default:  return 'Lol';
    }
  }

  @override
  Widget build(BuildContext context) {
    NavigationProvider provider = Provider.of<NavigationProvider>(context);

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: SizedBox(
            height: 44.0,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: () {
                      provider.navigateToPreviousPage(context);
                    },
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Row(
                        children: [
                          Icon(
                            Icons.arrow_back_ios,
                            size: 16,
                            color: AppColors.blackColorOpacity50,
                          ),
                          Text(
                            prevPage(provider.previousPage, context),
                            // provider.previousPage,
                            style: FontStyles.standartAppBarDayText,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        appBarTitle,
                        maxLines: 2,
                        style: FontStyles.appBarDayText,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: SizedBox(),
                )
              ],
            )),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(44.0);
}
