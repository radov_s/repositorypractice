import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';

class CreateButton extends StatelessWidget {
  final String label;
  final Function buttonFunction;

  const CreateButton({this.label, this.buttonFunction});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: SizedBox(
        height: 48.0,
        width: double.infinity,
        child: Material(
          color: AppColors.dodgerBlueColor,
          child: InkWell(
            onTap: buttonFunction,
            child: Center(
              child: Text(
                label,
                style: FontStyles.createRecordButtonText,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
