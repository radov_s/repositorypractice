import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PlusButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NavigationProvider navigationProvider =
        Provider.of<NavigationProvider>(context);
    return Container(
      height: 64.0,
      width: 64.0,
      child: ElevatedButton(
        style: ButtonStyle(
          elevation: MaterialStateProperty.all(4),
            backgroundColor:
                MaterialStateProperty.all(AppColors.dodgerBlueColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32.0),
            ))),
        onPressed: () {
          navigationProvider.navigateToPage(Routes.createRecordPage, context);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              child: SizedBox(
                height: 36,
                width: 4,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: AppColors.whiteColor,
                    borderRadius: BorderRadius.circular(2.0)
                  ),
                ),
              ),
            ),
            Positioned(
              child: SizedBox(
                height: 4,
                width: 36,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                      color: AppColors.whiteColor,
                      borderRadius: BorderRadius.circular(2.0)
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
