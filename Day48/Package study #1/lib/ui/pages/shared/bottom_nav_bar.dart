import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/res/icons/bottom_bar_icons_icons.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key key}) : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size.width;
    NavigationProvider navigationProvider = Provider.of<NavigationProvider>(context, listen: false);
    return Container(
      padding: EdgeInsets.only(bottom: 30.0),
      height: bottomNavBarHeight,
      width: mediaQuery,
      color: AppColors.blackColor,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Spacer(),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () {
              navigationProvider.navigateToPage(Routes.historyPage, context);
            },
            child: Icon(BottomBarIcons.time_active,
              color: ModalRoute.of(context).settings.name == Routes.historyPage
                  ? AppColors.dodgerBlueColor
                  : AppColors.whiteColorOpacity40,
              size: 30.0,
            ),
          ),
          const SizedBox(width: 64.0),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () {
              navigationProvider.navigateToPage(Routes.mainPage, context);
            },
            child: Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Icon(BottomBarIcons.home_active,
                color: ModalRoute.of(context).settings.name == Routes.mainPage
                    ? AppColors.dodgerBlueColor
                    : AppColors.whiteColorOpacity40,
                size: 30.0,
              ),
            ),
          ),
          const SizedBox(width: 64.0),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () {
              navigationProvider.navigateToPage(Routes.settingsPage, context);
            },
            child: Icon(BottomBarIcons.settings_active,
              color: ModalRoute.of(context).settings.name == Routes.settingsPage || ModalRoute.of(context).settings.name == Routes.languagePage
                  ? AppColors.dodgerBlueColor
                  : AppColors.whiteColorOpacity40,
              size: 30.0,
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
