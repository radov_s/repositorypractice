
import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/icons/app_bar_info.dart';
import 'package:Provider_template_v1/ui/pages/main_page/carousel_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CustomAppbar extends StatefulWidget with PreferredSizeWidget {
  @override
  _CustomAppbarState createState() => _CustomAppbarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(44.0);
}

class _CustomAppbarState extends State<CustomAppbar> {
  DateTime currentDay = DateTime.now();

  @override
  Widget build(BuildContext context) {
    CreateRecordProvider createRecordProvider =
        Provider.of<CreateRecordProvider>(context);
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: SafeArea(
        child: SizedBox(
          height: 44.0,
          width: double.infinity,
          child: Stack(
            children: [
              Center(
                child: SizedBox(
                  width: 174.0,
                  child: Row(
                    children: [
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(width: 20.0),
                            SizedBox(
                              width: 100,
                              child: InkWell(
                                onTap: () async {
                                  {
                                    DateTime pickedDay = await showDatePicker(
                                        context: context,
                                        initialDate: createRecordProvider.mainPageChosenDay,
                                        firstDate: DateTime(1990),
                                        lastDate: DateTime(2050));
                                    if (pickedDay != null) {
                                      createRecordProvider
                                          .chooseMainPageDay(pickedDay);
                                    } else
                                      return;
                                  }
                                },
                                child: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    toBeginningOfSentenceCase(
                                        '${DateFormat('EEEE', Localizations.localeOf(context).toString()).format(createRecordProvider.mainPageChosenDay)}'),
                                    style: FontStyles.appBarDayText,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20.0,
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  '${DateFormat('d').format(createRecordProvider.mainPageChosenDay)}',
                                  style: FontStyles.appBarIndexDayText,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                child: Center(
                  child: SizedBox(
                    height: 44.0,
                    width: 186.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: SizedBox(
                            width: 30,
                            height: 30,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () {
                                  createRecordProvider.chooseMainPageDay(
                                    createRecordProvider.mainPageChosenDay
                                        .subtract(
                                      Duration(days: 1),
                                    ),
                                  );
                                },
                                child: Icon(
                                  Icons.arrow_back_ios_sharp,
                                  size: 16,
                                  color: AppColors.blackColorOpacity50,
                                ),
                              ),
                            ),
                          ),
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: SizedBox(
                            width: 30,
                            height: 30,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () {
                                  createRecordProvider.chooseMainPageDay(createRecordProvider.mainPageChosenDay.add(Duration(days: 1),),);
                                },
                                child: Icon(
                                  Icons.arrow_forward_ios_sharp,
                                  size: 16,
                                  color: AppColors.blackColorOpacity50,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                child: InkWell(
                   child: Icon(AppBarInfo.info),
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => CarouselHelper(),
                    );
                  },
                ),
                right: 0,
                top: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
