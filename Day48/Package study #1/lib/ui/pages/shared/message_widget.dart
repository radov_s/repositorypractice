import 'package:Provider_template_v1/data/models/message.dart';
import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MessageWidget extends StatelessWidget {
  final Message message;

  const MessageWidget({@required this.message});

  @override
  Widget build(BuildContext context) {
    CreateRecordProvider createRecordProvider =
        Provider.of<CreateRecordProvider>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Material(
          child: InkWell(
            onTap: () {
              createRecordProvider.chooseMessage(message.id);
            },
            child: DecoratedBox(
              decoration: BoxDecoration(
                gradient: message.pickedGradient,
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            message.name,
                            overflow: TextOverflow.ellipsis,
                            style: FontStyles.messageLabelText,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(Icons.delete_outline_sharp),
                        const SizedBox(width: 26.0),
                        Icon(Icons.edit),
                      ],
                    ),
                    const SizedBox(height: 27.0),
                    createRecordProvider.chosenMessage == message.id
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                message.service,
                                style: FontStyles.messageText,
                              ),
                              const SizedBox(height: 8.0),
                              Text(
                                message.description,
                                style: FontStyles.messageText.copyWith(
                                  color: AppColors.whiteColor.withOpacity(0.9),
                                ),
                              ),
                              Divider(
                                height: 8.0,
                              ),
                            ],
                          )
                        : const SizedBox(),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: Text(
                            DateFormat('dd.MM.yy').format(message.dateCome),
                            style: FontStyles.messageText.copyWith(
                              color: AppColors.whiteColor.withOpacity(0.7),
                            ),
                          ),
                        ),
                        Text(
                          DateFormat('hh.mm').format(message.timeBegin),
                          style: FontStyles.messageText.copyWith(
                            color: AppColors.whiteColor.withOpacity(0.7),
                          ),
                        ),
                        Spacer(),
                        Text(
                          message.price.toString(),
                          style: FontStyles.messageText.copyWith(
                            fontSize: 12.0,
                            height: 1.5,
                            color: AppColors.whiteColor.withOpacity(0.7),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
