import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:flutter/material.dart';

class PlusButton extends StatelessWidget {

  Function plusButtonFunction;

  PlusButton(this.plusButtonFunction);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
    borderRadius: BorderRadius.circular(32.0),
      child: SizedBox(
        height: 64.0,
        width: 64.0,
        child: Material(
          color: AppColors.dodgerBlueColor,
          child: InkWell(
            // splashColor: Colors.red,
            onTap: plusButtonFunction,
            child: Stack(
              alignment: Alignment.center,
              children: [Positioned(
                child: SizedBox(
                  height: 36,
                  width: 4,
                  child: ColoredBox(
                    color: AppColors.whiteColor,
                  ),
                ),
              ),
                Positioned(
                  child: SizedBox(
                    height: 4,
                    width: 36,
                    child: ColoredBox(
                      color: AppColors.whiteColor,
                    ),
                  ),
                ),],
            ),
          ),
        ),
      ),
    );
  }
}
