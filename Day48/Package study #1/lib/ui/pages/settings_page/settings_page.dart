import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/ui/pages/shared/standart_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatelessWidget {
  SettingsPageLanguage language =
      FlutterDictionary.instance.language?.settingsPageLanguage ??
          en.settingsPageLanguage;

  @override
  Widget build(BuildContext context) {
    NavigationProvider navProvider = Provider.of<NavigationProvider>(context);
    return MainLayout(
      appBar: SharedAppBar(
        appBarTitle: language.title,
      ),
      bottomNavBar: true,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 32.0),
            child: InkWell(
              onTap: () {
                navProvider.navigateToPage(Routes.languagePage, context);
              },
              child: Text(language.language, style: FontStyles.settingText),
            ),
          ),
          Divider(
            height: 24.0,
            thickness: 1.0,
          ),
          InkWell(
            child: Text(
              language.aboutUs,
              style: FontStyles.settingText,
            ),
            onTap: () {navProvider.navigateToPage(Routes.aboutUsPage, context);},
          ),
          Divider(
            thickness: 1.0,
          ),
        ],
      ),
    );
  }
}
