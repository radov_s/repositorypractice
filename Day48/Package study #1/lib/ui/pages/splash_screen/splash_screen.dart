import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/images.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/pages/main_page/carousel_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nuts_activity_indicator/nuts_activity_indicator.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);


  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 3000), () {
      Navigator.of(context).pushReplacementNamed(Routes.mainPage);
      showDialog(
          context: context,
          builder: (BuildContext context) => CarouselHelper(),
        );
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: AppColors.splashScreenColor,
    ));

    return Material(
      child: ColoredBox(
        color: AppColors.splashScreenColor,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 52.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: double.infinity,
                padding: const EdgeInsets.only(left: 18.0),
                child: Image.asset(
                  AssetImages.splashPageLogo,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(height: 54.0),
              NutsActivityIndicator(
                radius: 12.0,
                tickCount: 8,
                startRatio: 0.4,
                endRatio: 1.0,
                relativeWidth: 0.5,
                activeColor: AppColors.whiteColor.withOpacity(0.0),
                inactiveColor: AppColors.whiteColor,
              ),
              const SizedBox(height: 8.0),
              Text(
                splashPageLoading,
                style: FontStyles.splashPageLoadingText,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
