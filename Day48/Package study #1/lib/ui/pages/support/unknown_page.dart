import 'package:Provider_template_v1/data/dto/login_dto.dart';
import 'package:Provider_template_v1/data/models/user.dart';
import 'package:Provider_template_v1/repositories/auth_repository.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UnknownPage extends StatefulWidget {
  @override
  _UnknownPageState createState() => _UnknownPageState();
}

class _UnknownPageState extends State<UnknownPage> {
  @override
  void initState() {
    loginWithEmailAndPassword();
    super.initState();
  }

  Future<void> loginWithEmailAndPassword() async {
    String email = 'email';
    String password = 'password';

    User user = await AuthRepository.instance.loginWithEmailAndPassword(
      LoginDto(
        email: email,
        password: password,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        child: Text(
          'Unknown Page',
          style: GoogleFonts.montserrat(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
