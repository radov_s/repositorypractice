import 'package:Provider_template_v1/data/models/message.dart';
import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';

import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/ui/layouts/focus_layout.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/shared/buttons/plus_button.dart';
import 'package:Provider_template_v1/ui/pages/shared/message_widget.dart';
import 'package:Provider_template_v1/ui/pages/shared/standart_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HistoryPage extends StatelessWidget {
  HistoryPageLanguage language = FlutterDictionary.instance.language?.historyPageLanguage ?? en.historyPageLanguage;

  @override
  Widget build(BuildContext context) {
    CreateRecordProvider createRecordProvider = Provider.of<CreateRecordProvider>(context);
    return FocusLayout(
      child: MainLayout(
        appBar: SharedAppBar(
          appBarTitle: language.title,
        ),
        bottomNavBar: true,
        body: Stack(
          fit: StackFit.expand,
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: SizedBox(
                      height: 50,
                      child: TextFormField(
                        onChanged: (value) {
                          if (value.isEmpty){
                            createRecordProvider.clearHistoryList();
                          }else {
                            createRecordProvider.searchMessage(value);
                          }
                        },
                        decoration: InputDecoration(
                          hintText: language.searchFieldHintText,
                          hintStyle: FontStyles.historySearchHintText,
                          alignLabelWithHint: true,
                          prefixIcon: Icon(Icons.search),
                          contentPadding: EdgeInsets.symmetric(horizontal: 15),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: AppColors.blackColor.withOpacity(0.2),
                            ),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: AppColors.blackColor.withOpacity(0.2),
                            ),
                            borderRadius: BorderRadius.circular(12.0),
                          ),

                        ),
                      ),
                    ),
                  ),
                  createRecordProvider.messageList.length <= 0
                      ? SizedBox()
                      : Column(
                        children: [
                          ...createRecordProvider.historyPageMessageList.map(
                                (message) => MessageWidget(message: message),
                          ),
                        ],
                      ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: PlusButton(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
