import 'package:flutter/material.dart';

class FocusLayout extends StatelessWidget {

  final Widget child;

  const FocusLayout({@required this.child});

  @override
  Widget build(BuildContext context) {
    return Material(
      // color: Colors.black38,
      child: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: (){
          FocusScope.of(context).unfocus();
        },
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: child,
        ),
      ),
    );
  }
}
