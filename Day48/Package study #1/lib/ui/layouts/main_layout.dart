import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/pages/shared/bottom_nav_bar.dart';
import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class MainLayout extends StatelessWidget {
  final Widget appBar;
  final Widget body;
  final bool bottomNavBar;

  const MainLayout({
    @required this.appBar,
    @required this.body,
    @required this.bottomNavBar,
  });

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      systemNavigationBarColor: AppColors.blackColor,
      statusBarColor: Colors.transparent,
    ));

    NavigationProvider provider = Provider.of(context);
    return DeviceSimulator(
      enable: true,
      child: WillPopScope(
        onWillPop: () async {
          provider.currentPage == Routes.mainPage ? null : provider.navigateToPreviousPage(context);
          return false;
        },
        child: Material(
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 16.0),
                child: Scaffold(
                  appBar: appBar,
                  bottomNavigationBar: bottomNavBar ? BottomNavBar() : SizedBox(),
                  body: Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: body,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
