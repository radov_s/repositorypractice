import 'dart:ui';

import 'package:flutter/cupertino.dart';

class AppColors {
  static const kAzure = Color(0xFF009EF8);
  static const kDodgerBlue = Color(0xFF33B5FF);
  static const kBlack = Color(0xFF272727);
  static const kWhiteTwo = Color(0xFFF2F2F2);
  static const kSoftDangerRed = Color(0xFFEB5757);
  static const kBlackTwo = Color(0xFF373737);
  static const kWhite = Color(0xFFFFFFFF);
  static const kTransparent = Color(0x00000000);


  static const kBlack80 = Color.fromARGB(204, 39, 39, 39);
  static const kBlack0 = Color.fromARGB(0, 0, 0, 0);

  static const kBrightSkyBlue = Color(0xFF00C6FF);
  static const kBrightBlue = Color(0xFF0072FF);

  static const kSlate = Color(0xFF536976);
  static const kDart = Color(0xFF292E49);

  static const kHotPink = Color(0xFFEC008C);
  static const kSalmon = Color(0xFFFC6767);

  static const kNeonPurple = Color(0xFFDA22FF);
  static const kBluishPurple = Color(0xFF9733EE);

  static const kTealish = Color(0xFF24C6DC);
  static const kBlueberry = Color(0xFF514A9D);
  static const Color dodgerBlueColor = Color(0xFF33b5ff);

  static const Color splashScreenColor = Color(0xFF009ef8);

  static const Color whiteColor = Color(0xFFffffff);
  static const Color whiteColorOpacity40 = Color(0x64ffffff);

  static const Color blackColor = Color(0xFF272727);
  static const Color blackColorOpacity50 = Color(0x80272727);

  static const LinearGradient timePickerBackground = LinearGradient(
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[
        Color(0xFF000000),
        Color(0xFF393939),
        Color(0xFF000000),
      ]);

  static const LinearGradient brightBluSkyToBrightBlue = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[
        Color(0xFF00c6ff),
        Color(0xFF0072ff),
      ]);

  static const LinearGradient dodgerBlueToDodgerBlue = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[
        dodgerBlueColor,
        dodgerBlueColor,
      ]);

  static const LinearGradient slateToDark = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[Color(0xFF536976), Color(0xFF292e49)]);

  static const LinearGradient neonPurpleToBluishPurple = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[Color(0xFFda22ff), Color(0xFF9733ee)]);

  static const LinearGradient hotPinkToSalmon = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[Color(0xFFec008c), Color(0xFFfc6767)]);

  static const LinearGradient tealishToBlueberry = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[Color(0xFF24c6dc), Color(0xFF514a9d)]);

  static const LinearGradient blueberryToHotPink = LinearGradient(
      transform: GradientRotation(1.5708),
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[Color(0xFF514a9d), Color(0xFFec008c)]);
}
