
class Routes {
  static const String mainPage = 'main-page';
  static const String settingsPage = 'settings-page';
  static const String languagePage = 'language-page';
  static const String historyPage = 'history-page';
  static const String createRecordPage = 'create-record-page';
  static const String aboutUsPage = 'about-us-page';
}