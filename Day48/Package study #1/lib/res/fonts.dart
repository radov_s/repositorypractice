import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';

class FontStyles {
  static get splashPageLoadingText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w400,
      fontSize: 14,
    );
  }

  static get appBarDayText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w700, fontSize: 24, color: AppColors.blackColor);
  }

  static get appBarIndexDayText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w600,
        fontSize: 14,
        height: 1.3,
        color: AppColors.blackColorOpacity50);
  }

  static get standartAppBarDayText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w600,
        fontSize: 12,
        height: 1.5,
        color: AppColors.blackColorOpacity50);
  }

  static get mainPageEmptyText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w700, fontSize: 24, color: AppColors.blackColor);
  }

  static get languagePageText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w600, fontSize: 14, color: AppColors.blackColor);
  }

  static get createRecordPagePickerText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w600, fontSize: 14, color: AppColors.whiteColor);
  }

  static get createRecordPagePickerLabelText {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w500,
        fontSize: 12,
        height: 1.5,
        color: AppColors.blackColorOpacity50);
  }

  static get createRecordPagePickerLabelTextBlack {
    return GoogleFonts.montserrat(
        fontWeight: FontWeight.w500,
        fontSize: 12,
        height: 1.5,
        color: AppColors.blackColor);
  }

  static get datePickerDatesText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w700,
      fontSize: 24,
      color: AppColors.whiteColor,
    );
  }

  static get createRecordButtonText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w700,
      fontSize: 18,
      color: AppColors.whiteColor,
    );
  }

  static get datePickerCancelText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w500,
      fontSize: 12,
      color: AppColors.whiteColor.withOpacity(0.5),
    );
  }

  static get datePickerSaveText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w700,
      fontSize: 12,
      color: AppColors.dodgerBlueColor,
    );
  }

  static get datePickerTimeBeginText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w600,
      fontSize: 14,
      color: AppColors.whiteColor,
    );
  }

  static get messageLabelText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w700,
      fontSize: 18,
      height: 1.28,
      color: AppColors.whiteColor,
    );
  }

  static get messageText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w600,
      fontSize: 14,
      height: 1.28,
      color: AppColors.whiteColor,
    );
  }

  static get historySearchHintText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w400,
      fontSize: 17,
      height: 1.3,
      color: AppColors.blackColor.withOpacity(0.4),
    );
  }

  static get onBoardingText {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w600,
      color: AppColors.kWhite
    );
  }
  static get settingText {
    return GoogleFonts.montserrat(
      fontSize: 14,
      fontWeight: FontWeight.w600,
    );
  }
}
