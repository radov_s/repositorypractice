import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:flutter/material.dart';

class AppGradients {
  static const kBrightSkyBlueBrightBlue = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [AppColors.kBrightSkyBlue, AppColors.kBrightBlue],
  );
  static const kSlateDark = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [AppColors.kSlate, AppColors.kDart],
  );
  static const kHotPinkSalmon = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [AppColors.kHotPink, AppColors.kSalmon],
  );
  static const kNeonPurpleBluishPurple = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [AppColors.kNeonPurple, AppColors.kBluishPurple],
  );
  static const kTealishBlueberry = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [AppColors.kTealish, AppColors.kBlueberry],
  );
  static const kCustomAlert = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [AppColors.kBlack80, AppColors.kBlack0, AppColors.kBlack80],
  );
}
