class AssetImages {

  static const String splashPageLogo = 'assets/logo@2x.png';
  static const String flagGB = 'assets/flags/flags_GB.png';
  static const String flagFR = 'assets/flags/flags_FR.png';
  static const String flagIT = 'assets/flags/flags_IT.png';
  static const String aboutUs = 'assets/about_us.png';
  static final helpScreenFirst = 'assets/on_boarding_image_1.png';
  static final helpScreenSecond = 'assets/on_boarding_image_2.png';
  static final helpScreenThird = 'assets/on_boarding_image_3.png';
}

class SVGImages {


}

class NetworkImages {

}