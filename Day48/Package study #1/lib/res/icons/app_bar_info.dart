import 'package:flutter/widgets.dart';

class AppBarInfo {
  AppBarInfo._();

  static const _kFontFam = 'AppBarInfo';
  static const String _kFontPkg = null;
  static const IconData info = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}