import 'package:Provider_template_v1/data/providers/create_record_provider.dart';
import 'package:Provider_template_v1/data/providers/date_picker_provider.dart';
import 'package:Provider_template_v1/data/providers/language_provider.dart';
import 'package:Provider_template_v1/data/providers/navigation_provider.dart';
import 'package:Provider_template_v1/dictionary/flutter_delegate.dart';
import 'package:Provider_template_v1/dictionary/models/supported_locales.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/ui/pages/splash_screen/splash_screen.dart';
import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helpers/route_helper.dart';
import '../res/consts.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => NavigationProvider()),
        ChangeNotifierProvider(create: (ctx) => DatePickerProvider()),
        ChangeNotifierProvider(create: (ctx) => CreateRecordProvider()),
        ChangeNotifierProxyProvider<DatePickerProvider, CreateRecordProvider>(
            create: (_) => CreateRecordProvider(),
            update: (_, datePickerProvider, createRecordProvider) => createRecordProvider
              ..pickedDate = datePickerProvider.pickedDate
              ..timeBegin = datePickerProvider.pickedStartTime
              ..timeEnd = datePickerProvider.pickedEndTime)
      ],
      child: Consumer<LanguageProvider>(
        builder: (BuildContext context, _, __) => MaterialApp(
          locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
          supportedLocales: SupportedLocales.instance.getSupportedLocales,
          localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
          title: EMPTY_STRING,
          home: SplashScreen(),
          debugShowCheckedModeBanner: false,
          onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
        ),
      ),
    );
  }
}
