
import 'package:Provider_template_v1/res/routes.dart';
import 'package:flutter/material.dart';

class NavigationProvider with ChangeNotifier {
  String _currentPage = Routes.mainPage;
  String _previousPage = '';

  String get currentPage {
    return _currentPage;
  }

  String get previousPage {
    return _previousPage;
  }

  void navigateToPage(routeName, context) {
    if (routeName == _currentPage) {
      return;
    }
    _previousPage = _currentPage;
    _currentPage = routeName;
    Navigator.of(context).pushReplacementNamed(currentPage);
    notifyListeners();
  }

  void navigateToPreviousPage(context) {
    _previousPage == ''
        ? Navigator.of(context).pushReplacementNamed(Routes.mainPage)
        : Navigator.of(context).pushReplacementNamed(_previousPage);
    Navigator.of(context).pushReplacementNamed(previousPage);
    _currentPage = _previousPage;
    _previousPage = Routes.mainPage;
    notifyListeners();
  }
}
