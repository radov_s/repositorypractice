import 'package:flutter/cupertino.dart';

class DatePickerProvider with ChangeNotifier {
  int _initialYear = 1990;
  int _endYear = 2050;
  int _year = DateTime.now().year;
  int _month = DateTime.now().month;
  int _day = DateTime.now().day;
  int _hour = DateTime.now().hour;
  int _minute = DateTime.now().minute;
  int _endHour = DateTime.now().hour;
  int _endMinute = DateTime.now().minute;

  DateTime _initialDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
  );
  DateTime _initialTime = DateTime.now();
  DateTime _pickedDate = DateTime.now();
  DateTime _pickedStartTime = DateTime.now();
  DateTime _pickedEndTime = DateTime.now();

  FixedExtentScrollController _yearController =
      FixedExtentScrollController(initialItem: 0);
  FixedExtentScrollController _monthController =
      FixedExtentScrollController(initialItem: 0);
  FixedExtentScrollController _dayController =
      FixedExtentScrollController(initialItem: 0);
  FixedExtentScrollController _hourController =
      FixedExtentScrollController(initialItem: 0);
  FixedExtentScrollController _minuteController =
      FixedExtentScrollController(initialItem: 0);

  int get initialYear => _initialYear;
  int get endYear => _endYear;
  int get year => _year;
  int get month => _month;
  int get day => _day;
  int get hour => _hour;
  int get minute => _minute;
  int get endHour => _endHour;
  int get endMinute => _endMinute;

  DateTime get pickedDate => _pickedDate;
  DateTime get initialDate => _initialDate;
  DateTime get initialTime => _initialTime;
  DateTime get pickedStartTime => _pickedStartTime;
  DateTime get pickedEndTime => _pickedEndTime;

  FixedExtentScrollController get yearController => _yearController;
  FixedExtentScrollController get monthController => _monthController;
  FixedExtentScrollController get dayController => _dayController;
  FixedExtentScrollController get hourController => _hourController;
  FixedExtentScrollController get minuteController => _minuteController;

  void changeYear(int year) {
    _year = year + initialYear;
    _pickedDate = DateTime(_year, _month, _day);
    notifyListeners();
  }

  void changeMonth(int month) {
    _month = month + 1;
    _pickedDate = DateTime(_year, _month, _day);
    notifyListeners();
  }

  void changeDay(int day) {
    _day = day + 1;
    _pickedDate = DateTime(_year, _month, _day);
    notifyListeners();
  }

  void changeHour(int hour) {
    _hour = hour;
    _pickedStartTime = DateTime(_year, _month, _day, _hour, _minute);
    notifyListeners();
  }

  void changeMinute(int minute) {
    _minute = minute;
    _pickedStartTime = DateTime(_year, _month, _day, _hour, _minute);
    notifyListeners();
  }

  void changeEndHour(int hour) {
    _endHour = hour;
    _pickedEndTime = DateTime(_year, _month, _day, _endHour, _endMinute);
    notifyListeners();
  }

  void changeEndMinute(int minute) {
    _endMinute = minute;
    _pickedEndTime = DateTime(_year, _month, _day, _endHour, _endMinute);
    notifyListeners();
  }

  void setPickedDate() {
    _pickedDate = _initialDate;
    _year = DateTime.now().year;
   _month = DateTime.now().month;
    _day = DateTime.now().day;
    notifyListeners();
  }

  void setPickedStartTime() {
    _pickedStartTime = _initialTime;
    _hour = DateTime.now().hour;
    _minute = DateTime.now().minute;
    notifyListeners();
  }

}
