import 'package:flutter/cupertino.dart';
import '../../dictionary/flutter_dictionary.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLang(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}
