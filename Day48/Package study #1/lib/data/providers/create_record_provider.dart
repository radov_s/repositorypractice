import 'package:Provider_template_v1/data/models/message.dart';
import 'package:Provider_template_v1/res/app_colors.dart';
import 'package:flutter/cupertino.dart';

class CreateRecordProvider with ChangeNotifier {
  List<Message> _messageList = [];
  List<Message> _historyPageMessageList = [];

  DateTime _chosenMessage;

  DateTime _mainPageChosenDay = DateTime.now();

  String _name;
  LinearGradient _pickedGradient = AppColors.dodgerBlueToDodgerBlue;
  String _description = '';
  String _service = '';
  String _price = '';
  DateTime _pickedDate;
  DateTime _timeBegin;
  DateTime _timeEnd;

  String get name => _name;

  List<Message> get messageList {
    return _messageList;
  }

  List<Message> get historyPageMessageList {
    return _historyPageMessageList;
  }

  DateTime get mainPageChosenDay {
    return _mainPageChosenDay;
  }

  void chooseMainPageDay(DateTime value) {
    _mainPageChosenDay = value;
    notifyListeners();
  }

  DateTime get chosenMessage {
    return _chosenMessage;
  }

  void chooseMessage(DateTime id) {
    _chosenMessage = id;
    notifyListeners();
  }

  void addMessage() {
    _messageList.add(
      Message(
        id: DateTime.now(),
        name: _name,
        pickedGradient: _pickedGradient,
        description: _description,
        service: _service,
        price: _price,
        dateCome: _pickedDate,
        timeBegin: _timeBegin,
        timeEnd: _timeEnd,
      ),
    );
    _price = '';
    notifyListeners();
  }

  void searchMessage(String value) {
    for (Message message in _messageList) {
      if (message.name.toLowerCase().contains(value.toLowerCase())) {
        _historyPageMessageList.add(message);
      }
    }

    notifyListeners();
  }

  void clearHistoryList() {
    _historyPageMessageList = [];
    notifyListeners();
  }

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  LinearGradient get pickedGradient => _pickedGradient;

  set pickedGradient(LinearGradient value) {
    _pickedGradient = value;
    notifyListeners();
  }

  String get description => _description;

  set description(String value) {
    _description = value;
    notifyListeners();
  }

  String get service => _service;

  set service(String value) {
    _service = value;
    notifyListeners();
  }

  String get price => _price;

  void changePrice(value) {
    _price = value;
    notifyListeners();
  }

  DateTime get pickedDate => _pickedDate;

  set pickedDate(DateTime value) {
    _pickedDate = value;
    notifyListeners();
  }

  DateTime get timeBegin => _timeBegin;

  set timeBegin(DateTime value) {
    _timeBegin = value;
    notifyListeners();
  }

  DateTime get timeEnd => _timeEnd;

  set timeEnd(DateTime value) {
    _timeEnd = value;
    notifyListeners();
  }
}
