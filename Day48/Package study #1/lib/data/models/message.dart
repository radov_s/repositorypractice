import 'dart:ui';

import 'package:flutter/material.dart';

class Message {
  final DateTime id;
  final String name;
  final LinearGradient pickedGradient;
  final String description;
  final String service;
  final String price;
  final DateTime dateCome;
  final DateTime timeBegin;
  final DateTime timeEnd;

  Message({
    @required this.id,
    @required this.name,
    @required this.pickedGradient,
    @required this.description,
    @required this.service,
    @required this.price,
    @required this.dateCome,
    @required this.timeBegin,
    @required this.timeEnd,
  });
}
