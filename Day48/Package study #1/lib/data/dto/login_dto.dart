class LoginDto {
  final String email;
  final String password;

  LoginDto({
    this.email,
    this.password,
  });

  Map<String, dynamic> toMap() {
    return {
      'email': this.email,
      'password': this.password,
    };
  }

  factory LoginDto.fromMap(Map<String, dynamic> map) {
    return new LoginDto(
      email: map['email'] as String,
      password: map['password'] as String,
    );
  }
}