import 'package:flutter/material.dart';
import 'package:lecture_practice_15/shared/custom_drawer.dart';

class FourthPage extends StatelessWidget {
  const FourthPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomDrawer(
      child: Center(
          child: Text(
            'FOURTH_PAGE',
          )
      ),
    );
  }
}
