import 'package:flutter/material.dart';
import 'package:lecture_practice_15/shared/custom_drawer.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomDrawer(
      child: Center(
        child: Text(
          'FIRST_PAGE',
        )
      ),
    );
  }
}
