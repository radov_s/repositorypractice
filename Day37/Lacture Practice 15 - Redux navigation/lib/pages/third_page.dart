import 'package:flutter/material.dart';
import 'package:lecture_practice_15/shared/custom_drawer.dart';

class ThirdPage extends StatelessWidget {
  const ThirdPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomDrawer(
      child: Center(
          child: Text(
            'THIRD_PAGE',
          )
      ),
    );
  }
}
