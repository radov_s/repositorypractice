import 'package:lecture_practice_15/navigation_service/app_state.dart';
import 'package:lecture_practice_15/navigation_service/route_selectors.dart';
import 'package:redux/redux.dart';

class NavigationViewModel {
  final void Function() goToFirstPage;
  final void Function() goToSecondPage;
  final void Function() goToThirdPage;
  final void Function() goToFourthPage;
  final void Function() popAction;
  final String getCurrentPage;

  NavigationViewModel({
    this.goToFirstPage,
    this.goToSecondPage,
    this.goToThirdPage,
    this.goToFourthPage,
    this.popAction,
    this.getCurrentPage,
  });

  static NavigationViewModel fromStore(Store<AppState> store) {
    return NavigationViewModel(
      goToFirstPage: RouteSelectors.goToFirstPage(store),
      goToSecondPage: RouteSelectors.goToSecondPage(store),
      goToThirdPage: RouteSelectors.goToThirdPage(store),
      goToFourthPage: RouteSelectors.goToFourthPage(store),
      popAction: RouteSelectors.pop(store),
    );
  }
}
