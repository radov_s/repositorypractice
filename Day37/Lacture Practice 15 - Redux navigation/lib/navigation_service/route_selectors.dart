import 'package:lecture_practice_15/navigation_service/route_service.dart';
import 'package:lecture_practice_15/navigation_service/routes.dart';
import 'package:redux/redux.dart';

import 'app_state.dart';

class RouteSelectors {
  static void Function() goToFirstPage(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.push(Routes.firstPage));
  }

  static void Function() goToSecondPage(Store<AppState> store) {
    return () =>
        store.dispatch(RouteService.instance.push(Routes.secondPage));
  }

  static void Function() goToThirdPage(Store<AppState> store) {
    return () =>
        store.dispatch(RouteService.instance.push(Routes.thirdPage));
  }

  static void Function() goToFourthPage(Store<AppState> store) {
    return () =>
        store.dispatch(RouteService.instance.push(Routes.fourthPage));
  }

  static void Function() pop(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.pop());
  }

  static String getCurrentPage(Store<AppState> store) {
    return RouteService.instance.routesStackLast;
  }
}
