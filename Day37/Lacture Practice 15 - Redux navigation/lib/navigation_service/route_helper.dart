import 'package:flutter/material.dart';
import 'package:lecture_practice_15/navigation_service/routes.dart';
import 'package:lecture_practice_15/pages/first_page.dart';
import 'package:lecture_practice_15/pages/fourth_page.dart';
import 'package:lecture_practice_15/pages/second_page.dart';
import 'package:lecture_practice_15/pages/third_page.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // WidgetsFlutterBinding.ensureInitialized();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // isviewed = prefs.getInt('onBoard');
  // endregion

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case (Routes.firstPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => FirstPage(),);
      case (Routes.secondPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => SecondPage(),);
      case (Routes.thirdPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => ThirdPage(),);
      case (Routes.fourthPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => FourthPage(),);
      default:
        return _defaultRoute(
          settings: settings,
          page: FirstPage(),
        );
    }
  }

  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
