import 'package:flutter/material.dart';
import 'package:redux_epics/redux_epics.dart';
class AppState {

  AppState();

  ///All states are initialized in the [initial] function.
  factory AppState.initial() {
    return AppState(
    );
  }

  ///The [getReducer] function is the main Reducer in which you call Reducer, all other states.
  static AppState getReducer(AppState state, dynamic action) {
    return AppState(
    );
  }

  ///In [getAppEpic], call the main epic.
  static final getAppEpic = combineEpics<AppState>([]);
}
