import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:lecture_practice_15/navigation_service/app_state.dart';
import 'package:lecture_practice_15/navigation_service/navigation_viewmodel.dart';
import 'package:redux/redux.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key key, this.child}) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: (Store<dynamic> store) => NavigationViewModel.fromStore(store),
      builder: (BuildContext context, NavigationViewModel vm) {
        Widget _buildButton(Color color, Function function, String label) {
          return Container(
            width: double.infinity,
            height: 50,
            color: color,
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  function();
                },
                child: Center(child: Text(label)),
              ),
            ),
          );
        }

        return Scaffold(
          appBar: AppBar(),
          drawer: Drawer(
            child: Column(
              children: [
                SizedBox(
                  height: 88.0,
                ),
                _buildButton(Colors.orange, vm.goToFirstPage, 'FIRST'),
                _buildButton(Colors.red, vm.goToSecondPage, 'SECOND'),
                _buildButton(Colors.blue, vm.goToThirdPage, 'THIRD'),
                _buildButton(Colors.greenAccent, vm.goToFourthPage, 'FOURTH'),
              ],
            ),
          ),
          body: child,
        );
      },
    );
  }
}
