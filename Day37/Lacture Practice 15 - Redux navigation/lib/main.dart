import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:lecture_practice_15/application.dart';
import 'package:lecture_practice_15/navigation_service/app_state.dart';
import 'package:redux/redux.dart';

void main() {
  final Store store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
    middleware: [
      //EpicMiddleware(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(Application(store: store));
}


