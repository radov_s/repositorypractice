import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux_study_6/dictionary/flutter_dictionary.dart';
import 'package:redux_study_6/dictionary/locales.dart';
import 'package:redux_study_6/store/application/reducer.dart';
import 'package:redux_study_6/store/homepage_state/actions.dart';


class LanguagePageState {
  String language;
  ThemeData themeData;

  LanguagePageState({
    this.language,
    this.themeData
  });

  factory LanguagePageState.initial() {
    return LanguagePageState(
      language: Locales.en,
      themeData: ThemeData(),
    );
  }

  LanguagePageState copyWith({
    String language,
    ThemeData themeData
  }) {
    return LanguagePageState(
      language: language ?? this.language,
      themeData: themeData ?? this.themeData
    );
  }

  LanguagePageState reducer(dynamic action) {
    return Reducer<LanguagePageState>(
      actions: HashMap.from({
        ChangeLanguageAction: (dynamic action) => _changeLanguage(action),
        ChangeThemeDataAction: (dynamic action) => _changeTheme(action),
      }),
    ).updateState(action, this);
  }

  LanguagePageState _changeLanguage(ChangeLanguageAction languageAction) {
    FlutterDictionary.instance.setNewLanguage(languageAction.newLanguage);
    language = FlutterDictionary.instance.language.toString();
    return copyWith(language: languageAction.newLanguage);
  }

  LanguagePageState _changeTheme(ChangeThemeDataAction action) {
    return copyWith(themeData: ThemeData(accentColor: action.color, primaryColor: action.color, backgroundColor: action.color));
  }
}