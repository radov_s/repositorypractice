import 'package:flutter/material.dart';

class ChangeLanguageAction {
  final String newLanguage;

  ChangeLanguageAction({this.newLanguage});
}

class ChangeThemeDataAction {
  final Color color;

  ChangeThemeDataAction({this.color});
}
