
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_6/store/application/app_state.dart';
import 'package:redux_study_6/store/homepage_state/actions.dart';


class LanguagePageSelectors {
  static void Function(String newLanguage) changeLanguageFunction(Store<AppState> store){
    return (String newLanguage) => store.dispatch(ChangeLanguageAction(newLanguage: newLanguage));
  }

  static void Function(Color newColor) changeDataThemeFunction(Store<AppState> store){
    return (Color newColor) => store.dispatch(ChangeThemeDataAction(color: newColor));
  }

  static String getCurrent(Store<AppState> store){
    return store.state.languagePageState.language;
  }

  static ThemeData getCurrentDataTheme(Store<AppState> store){
    return store.state.languagePageState.themeData;
  }

}