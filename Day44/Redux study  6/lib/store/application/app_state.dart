import 'package:redux_epics/redux_epics.dart';
import 'package:redux_study_6/store/homepage_state/state.dart';

class AppState {
  final LanguagePageState languagePageState;

  AppState({this.languagePageState});

  factory AppState.initial() {
    return AppState(
      languagePageState: LanguagePageState.initial(),
    );
  }

  static AppState getReducer(AppState state, dynamic action) {
    return AppState(
      languagePageState: state.languagePageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([]);
}
