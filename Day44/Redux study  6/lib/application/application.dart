import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_6/dictionary/flutter_delegate.dart';
import 'package:redux_study_6/dictionary/flutter_dictionary.dart';
import 'package:redux_study_6/dictionary/locales.dart';
import 'package:redux_study_6/store/application/app_state.dart';
import 'package:redux_study_6/ui/home_page.dart';

class Application extends StatelessWidget {
  final Store<AppState> store;

  Application({this.store});

  @override
  Widget build(BuildContext context) {
    FlutterDictionary.instance.setNewLanguage(FlutterDictionaryDelegate.getCurrentLocale);
    return StoreProvider(
      store: store,
      child: StoreConnector<AppState, AppState>(
        converter: (Store<AppState> store) => store.state,
        builder: (BuildContext context, AppState state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: HomePage(),
            onGenerateRoute: (RouteSettings settings) => PageRouteBuilder(pageBuilder: (context, _, __) => HomePage(),),
            locale: Locale(Locales.base),
            supportedLocales: FlutterDictionaryDelegate.getSupportedLocales,
            localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
            builder: (context, child) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor: 1.0,
                ),
                child: child,
              );
            },
          );
        },
      ),
    );
  }
}
