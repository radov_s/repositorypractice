import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_6/store/application/app_state.dart';
import 'package:redux_study_6/store/homepage_state/selector.dart';

class HomePageViewModel {
  final void Function(String newLanguage) changeLanguage;
  final void Function(Color newColor) changeThemeData;

  final String getCurrentLanguage;
  final ThemeData getCurrentThemeData;

  HomePageViewModel({
    @required this.changeLanguage,
    @required this.getCurrentLanguage,
    @required this.getCurrentThemeData,
    @required this.changeThemeData,
  });

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      changeLanguage: LanguagePageSelectors.changeLanguageFunction(store),
      getCurrentLanguage: LanguagePageSelectors.getCurrent(store),
      changeThemeData: LanguagePageSelectors.changeDataThemeFunction(store),
      getCurrentThemeData: LanguagePageSelectors.getCurrentDataTheme(store),
    );
  }
}
