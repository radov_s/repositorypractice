import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_6/dictionary/dictionary_classes/home_page.dart';
import 'package:redux_study_6/dictionary/flutter_dictionary.dart';
import 'package:redux_study_6/store/application/app_state.dart';
import 'package:redux_study_6/ui/home_page_viewmodel.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String dropDownLanguage = 'EN';
  String dropDownThemeData = 'BLUE';

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
      converter: (Store<dynamic> store) => HomePageViewModel.fromStore(store),
      builder: (BuildContext context, HomePageViewModel vm) {
        final HomePageLanguage homePageLanguage = FlutterDictionary.instance.language.homePageLanguage;
        return MaterialApp(
          theme: vm.getCurrentThemeData,
          home: Scaffold(
            appBar: AppBar(),
            drawer: Drawer(
              child: Column(
                children: [
                  SizedBox(
                    height: 50.0,
                  ),
                  DropdownButton(
                    value: dropDownLanguage,
                    onChanged: (String newValue) {
                      setState(() {
                        vm.changeLanguage(newValue.toLowerCase());
                        dropDownLanguage = newValue;
                      });
                    },
                    items: <String>['EN', 'RU'].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                  DropdownButton(
                    value: dropDownThemeData,
                    onChanged: (String newValue) {
                      setState(() {
                        dropDownThemeData = newValue;
                        if (newValue == 'BLACK') {
                          vm.changeThemeData(Colors.black);
                        }
                        if (newValue == 'ORANGE') {
                          vm.changeThemeData(Colors.orange);
                        }
                        if (newValue == 'BLUE') {
                          vm.changeThemeData(Colors.blueAccent);
                        }
                      });
                    },
                    items: <String>['BLUE', 'ORANGE', 'BLACK'].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                      child: Text(
                        homePageLanguage.text,
                        style: TextStyle(fontSize: 20.0, color: vm.getCurrentThemeData.primaryColor),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 50.0,
                    child: InkWell(
                      onTap: () => vm.changeThemeData(Colors.yellowAccent),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
