import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(HomePage());

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Foo(
                duration: Duration(seconds: 5),
              ),
              AnimatedSwitcher(
                duration: Duration(seconds: 2),
                transitionBuilder: (Widget child, Animation<double> animation) => ScaleTransition(
                  child: child,
                  scale: animation,
                ),
                child: Image.network(
                    'https://cdn.akamai.steamstatic.com/steam/apps/211420/ss_c34cdf130b9ac71c99196007d1e78c05305652b9.1920x1080.jpg?t=1580310551'),
              ),
              AnimatedCrossFade(
                firstChild: Image.network('https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg'),
                secondChild: Image.network('https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg'),
                crossFadeState: CrossFadeState.showFirst,
                duration: Duration(seconds: 3),
              ),
              MyStatefulWidget(),
            ],
          ),
        ),
      ),
    );
  }
}

class Foo extends StatefulWidget {
  const Foo({Key key, this.duration}) : super(key: key);

  final Duration duration;

  @override
  _FooState createState() => _FooState();
}

class _FooState extends State<Foo> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: widget.duration,
    )..repeat();
  }

  @override
  void didUpdateWidget(Foo oldWidget) {
    super.didUpdateWidget(oldWidget);
    _controller.duration = widget.duration;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: [
          Container(
            width: 50.0,
            height: 50.0,
            decoration: BoxDecoration(color: Colors.orange, shape: BoxShape.circle),
          ),
          Positioned(
            left: 10,
            top: 10,
            child: AnimatedBuilder(
              animation: _controller,
              child: Container(
                width: 20,
                height: 1,
                color: Colors.black,
                child: const Center(),
              ),
              builder: (BuildContext context, Widget child) {
                return Transform.rotate(
                  angle: _controller.value * 2.0 * pi,
                  child: child,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class SpinningContainer extends AnimatedWidget {
  const SpinningContainer({
    Key key,
    AnimationController controller,
  }) : super(key: key, listenable: controller);

  Animation<double> get _progress => listenable as Animation<double>;

  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: _progress.value * 2.0 * pi,
      child: Container(width: 200.0, height: 200.0, color: Colors.green),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(duration: const Duration(seconds: 10), vsync: this)..repeat();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SpinningContainer(controller: _controller);
  }
}
