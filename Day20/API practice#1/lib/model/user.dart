

class User {
  String name;
  String email;
  int age;
  String avatarPictureUrl;

  User({this.name, this.email, this.age, this.avatarPictureUrl});
}