import 'package:api_practice_1/model/user.dart';

class UserService {
  static User getUser(Map<String, dynamic> map) {
    return User(
      name: map['results'].first['name']['first'] + ' ' + map['results'].first['name']['last'],
      age: map['results'].first['dob']['age'],
      email: map['results'].first['email'],
      avatarPictureUrl: map['results'].first['picture']['large'],
    );
  }
}
