import 'dart:convert';

import 'package:api_practice_1/model/user.dart';
import 'package:api_practice_1/services/user_service.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<User> users = [];

  void _addPerson() async {
    final data = await http.get('https://randomuser.me/api');
    users.add(UserService.getUser(jsonDecode(data.body)));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int i) {
          return ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(users[i].avatarPictureUrl),
            ),
            title: Text(users[i].name),
            subtitle: Text(users[i].email),
            trailing: Text(users[i].age.toString()),
          );
        },
        itemCount: users.length,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addPerson,
        child: Icon(Icons.add),
      ),
    );
  }
}
