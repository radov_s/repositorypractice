import 'package:flutter/material.dart';
import 'location_set.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hi there'),
        backgroundColor: Color(0xFFD9A855),
      ),
      body: LocationInput(),
    );
  }
}
