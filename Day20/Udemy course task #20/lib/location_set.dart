import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:location/location.dart';
import 'package:latlong/latlong.dart';

class LocationInput extends StatefulWidget {
  @override
  _LocationInputState createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  bool _previewImageUrl;
  double a;
  double b;

  Future<void> _getCurrentUserLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      a = locData.latitude;
      b = locData.longitude;
      _previewImageUrl = false;
    });
    print(locData.latitude);
    print(locData.longitude);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            height: 340,
            width: double.infinity,
            alignment: Alignment.centerLeft,
            child: _previewImageUrl != false
                ? Center(
                  child: Text(
              'No location',
              textAlign: TextAlign.center,
            ),
                )
                : FlutterMap(
              options: new MapOptions(
                center: new LatLng(a, b),
                zoom: 15.0,
              ),
              layers: [
                new TileLayerOptions(urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", subdomains: ['a', 'b', 'c']),
                new MarkerLayerOptions(
                  markers: [
                    new Marker(
                      width: 80.0,
                      height: 80.0,
                      point: new LatLng(a, b),
                      builder: (ctx) => new Container(
                        child: Icon(
                          Icons.location_on,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 80.0,
          color: Color(0xffeaa544),
          child: InkWell(
            onTap: _getCurrentUserLocation,
            child: Icon(
              Icons.location_on,
            ),
          ),
        ),
      ],
    );
  }
}
