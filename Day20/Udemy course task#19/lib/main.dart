import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';

List<CameraDescription> cameras;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await availableCameras();
  runApp(CameraApp());
}

class CameraApp extends StatefulWidget {
  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {
  CameraController controller;
  String _path;

  @override
  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.medium);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void getScreenshot() {
    controller.takePicture().then((value) {
      _path = value.path;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return MaterialApp(
        home: Center(
          child: Text('do not display'),
        ),
      );
    }
    return MaterialApp(
      home: Scaffold(
        body: Column(
          children: [
            AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: CameraPreview(controller),
            ),
             _path == null
                ? Center(child: (Text('do not have screen')))
                : Expanded(child: Image.file(File(_path), width: double.infinity, fit: BoxFit.fill,)),
            Container(
              color: Color(0xFF0AAA99),
              width: double.infinity,
              height: 80.0,
              child: InkWell(
                child: Center(child: Text('Screenshot')),
                onTap: () => getScreenshot(),
              ),
            ),    
          ],
        ),
      ),
    );
  }
}
