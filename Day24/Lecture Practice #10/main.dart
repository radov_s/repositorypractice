import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          width: 150,
          height: 150,
          child: Center(child: Text('some text')),
        ),
      ),
    );
  }
}

class Singleton {
  static final Singleton _singleton = Singleton._internal();

  factory Singleton() {
    return _singleton;
  }

  Singleton._internal();
}

class User {
  final String name;
  final String id;
  final String email;
  final String phone;

  User({this.name, this.id, this.email, this.phone});

  User copyWith(String name, String id, String email, String phone) => User(
        name: name ?? this.name,
        id: id ?? this.id,
        email: email ?? this.email,
        phone: phone ?? this.phone,
      );
}

class Facade {
  List<User> list = [];
  String returnByIndex(int index) {
    return list.elementAt(index).email;
  }
}

class Iterable {
  List<User> list = [];

  void displayUsers() {
    for (var el in list) {
      print(el);
    }
  }
}

class Adapter {
  static double intToDouble(int a) {
    return a.toDouble();
  }
}

class PizzaBuilder {
  String crust;
  int diameter;
  Set<String> toppings;

  PizzaBuilder(this.diameter);

  Pizza build() {
    return Pizza(this);
  }
}

class Pizza {
  String crust;
  int diameter;
  Set<String> toppings;

  Pizza(PizzaBuilder builder) {
    crust = builder.crust;
    diameter = builder.diameter;
    toppings = builder.toppings;
  }

  String _stringifiedToppings() {
    var stringToppings = toppings.join(", ");
    var lastComma = stringToppings.lastIndexOf(",");
    var replacement =
        ",".allMatches(stringToppings).length > 1 ? ", and" : " and";

    return stringToppings.replaceRange(lastComma, lastComma + 1, replacement);
  }

  @override
  String toString() {
    return "A delicous $diameter\" pizza with $crust crust covered in $toppings";
  }
}
