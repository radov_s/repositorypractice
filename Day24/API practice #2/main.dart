import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<List<University>> fetchData(http.Client client, String country) async {
  final response = await client.get(
      Uri.parse('http://universities.hipolabs.com/search?country=$country'));
  return compute(parseData, response.body);
}

List<University> parseData(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<University>((json) => University.fromJson(json)).toList();
}

class University {
  final String domain;
  final String name;
  final String country;
  final String alphaCode;

  University({this.domain, this.name, this.country, this.alphaCode});

  factory University.fromJson(Map<String, dynamic> json) {
    return University(
      domain: json['domains'][0],
      name: json['name'],
      country: json['country'],
      alphaCode: json['alpha_two_code'],
    );
  }
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Isolate Demo';

    return MaterialApp(
      title: appTitle,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String countryFor = 'Ukraine';
  final fieldText = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[100],
      ),
      body: Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0),
            child: TextField(
              controller: fieldText,
              onSubmitted: (value) {
                setState(() {
                  countryFor = value;
                });
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                prefixIcon: Icon(Icons.search),
                contentPadding:
                    const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                hintText: 'Country',
                suffix: InkWell(
                  onTap: () => clearInput(),
                  child: Text(
                    'Clear',
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<List<University>>(
              future: fetchData(http.Client(), countryFor),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);

                return snapshot.hasData
                    ? UniversityList(universities: snapshot.data)
                    : Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }
  void clearInput() {
    fieldText.clear();
  }
}

class UniversityList extends StatelessWidget {
  final List<University> universities;

  UniversityList({Key key, this.universities}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: universities.length,
      itemBuilder: (context, index) {
        return Container(
          width: double.infinity,
          height: 120.0,
          margin: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
          decoration: BoxDecoration(
              color: Colors.blue[400],
              shape: BoxShape.rectangle,
              borderRadius: const BorderRadius.all(Radius.circular(15.0))),
          child: ListTile(
            title: Text(
              universities[index].name,
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
                'Page: ${universities[index].domain} | ${universities[index].alphaCode}'),
            leading: Icon(
              Icons.school,
              color: Colors.blue[200],
            ),
          ),
        );
      },
    );
  }
}
