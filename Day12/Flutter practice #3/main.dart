import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              leading: Icon(Icons.web),
              pinned: false,
              snap: true,
              floating: true,
              expandedHeight: 100.0,
              centerTitle: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Image.network('https://media.freestocktextures.com/cache/92/0b/920b3c9147d15f09baad7a568db70958.jpg', fit: BoxFit.cover),
              ),
              backgroundColor: Color.fromRGBO(1, 31, 75, 0.99),
              title: Text('SliverAppBar'),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Container(
                    color: index.isOdd
                        ? Color.fromRGBO(3, 57, 108, 1)
                        : Color.fromRGBO(100, 151, 177, 1),
                    height: 200.0,
                    child: Center(
                      child: Text('', textScaleFactor: 5),
                    ),
                  );
                },
                childCount: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
