


import 'package:flutter/material.dart';

class Dialog {
  static void show;
  Widget widget;
}

class DefaultErrorDialog implements Dialog {
  @override
  Widget widget;
  @override
  static void show(BuildContext ctx) {
    showDialog<String>(
      context: ctx,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('U pick a wrong house, fool'),
        content: const Text('Maybe im your friend, ...'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Its me'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('Amigos'),
          ),
        ],
      ),
    );
  }
}

class DialogHelper{
  static void show(BuildContext context) {
    DefaultErrorDialog.show(context);
  }
}