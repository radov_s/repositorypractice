import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextProvider(),
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: Colors.red,
          accentColor: Colors.red,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextProvider provider = Provider.of<TextProvider>(context);
    List<String> listOfStrings = provider.list;
    var sizeHeight = MediaQuery.of(context).size.height * 0.5 -
        AppBar().preferredSize.height;
    Widget _buildButton(IconData icon, Function function) {
      return Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Theme
              .of(context)
              .primaryColor,
        ),
        child: InkWell(
          child: Icon(icon),
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: function,
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('funny'),
        backgroundColor: Theme
            .of(context)
            .primaryColor,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              width: double.maxFinite,
              height: sizeHeight,
              child: ListView.builder(
                itemCount: listOfStrings.length,
                itemBuilder: (ctx, i) {
                  return ListTile(
                    title: Text('${listOfStrings[i]}'),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildButton(
                    Icons.remove,
                    provider.decrementSize,
                  ),
                  Divider(
                    height: sizeHeight,
                  ),
                  _buildButton(
                    Icons.add,
                    provider.incrementSize,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TextProvider with ChangeNotifier {
  List<String> list = [];

  List<String> get listOfString {
    return [...list];
  }

  void decrementSize() {
    list.removeLast();
    notifyListeners();
  }

  void incrementSize() {
    list.add('Item ${list.length}');
    notifyListeners();
  }
}
