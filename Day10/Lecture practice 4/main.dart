import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'flutter animation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _animationController;
  AnimationController _colorAnimationController;

  Animation<double> _size;
  Animation<Color> _color;
  bool hide = false;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
      reverseDuration: Duration(seconds: 1),
    );
    _colorAnimationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
      reverseDuration: Duration(seconds: 1),
    );
    _size = Tween<double>(begin: 0, end: 300).animate(_animationController);
    _color = ColorTween(
      begin: Color.fromRGBO(238, 227, 231, 1),
      end: Color.fromRGBO(246, 171, 182, 1),
    ).animate(_colorAnimationController);

    _colorAnimationController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: AnimatedBuilder(
        animation: _animationController,
        builder: (BuildContext context, Widget _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                height: 100,
                color: _color.value,
                child: InkWell(
                  onTap: () {
                    if (_animationController.isCompleted &&
                        _colorAnimationController.isCompleted) {
                      _colorAnimationController.reverse();
                      _animationController.reverse();
                    } else {
                      _colorAnimationController.forward();
                      _animationController.forward();
                    }
                  },
                ),
              ),
              Container(
                width: double.infinity,
                height: _size.value,
                color: Color.fromRGBO(133, 30, 62, 1),
                child: Opacity(
                  opacity: _animationController.isCompleted ? 1.0 : 0,
                  child: Center(
                    child: Text(
                      'EXAMPLE TEXT',
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
