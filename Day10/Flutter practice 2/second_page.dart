import 'package:flutter/material.dart';
import './main.dart';

class SecondPage extends StatelessWidget {
  static const routeName = 'second-page';

  final List<String> photos = [
    'https://images.unsplash.com/photo-1502899576159-f224dc2349fa?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjR8fGNpdHl8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80',
    'https://city.diia.gov.ua/assets/img/pages/city.jpg',
    'https://blog.politics.ox.ac.uk/wp-content/uploads/2020/10/future-city-scaled.jpg',
    'https://wallpaperaccess.com/full/702551.jpg',
    'https://c.wallhere.com/photos/13/82/digital_art_artwork_cyberpunk_cyber_city_futuristic-1933521.jpg!d',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () =>
            Navigator.of(context).pushReplacementNamed(MyHomePage.routeName),
        child: ListView.builder(
          itemCount: photos.length,
          itemBuilder: (btx, index) {
            return ClipRRect(
              child: Image.network(photos[index]),
            );
          },
        ),
      ),
    );
  }
}
