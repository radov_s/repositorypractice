import 'package:flutter/material.dart';
import './second_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.black,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: (setting) {
        if (setting.name == SecondPage.routeName) {
          return MaterialPageRoute(builder: (ctx) => SecondPage());
        }
        return MaterialPageRoute(builder: (ctx) => MyHomePage());
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  static const routeName = './';

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TEST APP'),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.4),
              child: Text(
                'You have pushed the button this many times:',
              ),
            ),
            Expanded(
              child: Text(
                '$_counter',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 50.0,
                  height: 50.0,
                  margin: EdgeInsets.only(left: 14, bottom: 14),
                  decoration: BoxDecoration(
                      color: Colors.orange, shape: BoxShape.circle),
                  child: InkWell(
                    onTap: _decrementCounter,
                    child: Icon(Icons.remove),
                  ),
                ),
                Container(
                  width: 50.0,
                  height: 50.0,
                  margin: EdgeInsets.only(right: 14, bottom: 14),
                  decoration: BoxDecoration(
                      color: Colors.black, shape: BoxShape.circle),
                  child: InkWell(
                    onTap: _incrementCounter,
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              height: 50.0,
              color: Colors.black,
              child: InkWell(
                onTap: () => Navigator.of(context)
                    .pushReplacementNamed(SecondPage.routeName),
                child: Center(
                  child: Text(
                    'dramatic transition'.toUpperCase(),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
