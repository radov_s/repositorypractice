import 'package:redux/redux.dart';
import 'package:redux_conceptions/navigation_service/route_selectors.dart';
import 'package:redux_conceptions/store/app/app_state.dart';

class NavigationViewModel {
  final void Function() goToUserPage;
  final void Function() goToImagePage;
  final void Function() popAction;
  final String getCurrentPage;

  NavigationViewModel({
    this.goToUserPage,
    this.goToImagePage,
    this.popAction,
    this.getCurrentPage,
  });

  static NavigationViewModel fromStore(Store<AppState> store) {
    return NavigationViewModel(
        goToUserPage: RouteSelectors.goToUserPage(store),
        goToImagePage: RouteSelectors.goToImagePage(store),
        popAction: RouteSelectors.pop(store),
        getCurrentPage: RouteSelectors.getCurrentPage(store));
  }
}
