import 'package:flutter/material.dart';
import 'package:redux_conceptions/navigation_service/routes.dart';
import 'package:redux_conceptions/ui/image_page/image_page.dart';
import 'package:redux_conceptions/ui/user_page/user_page.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // WidgetsFlutterBinding.ensureInitialized();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // isviewed = prefs.getInt('onBoard');
  // endregion

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case (Routes.imagesPage):
        return PageRouteBuilder(
          pageBuilder: (context, _, __) => ImagePage(),
        );
      case (Routes.usersPage):
        return PageRouteBuilder(
          pageBuilder: (context, _, __) => UserPage(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: UserPage(),
        );
    }
  }

  static PageRoute _defaultRoute(
      {@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
