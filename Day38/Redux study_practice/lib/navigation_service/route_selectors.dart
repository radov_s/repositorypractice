import 'package:redux/redux.dart';
import 'package:redux_conceptions/navigation_service/route_service.dart';
import 'package:redux_conceptions/navigation_service/routes.dart';
import 'package:redux_conceptions/store/app/app_state.dart';

class RouteSelectors {
  static void Function() goToUserPage(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.push(Routes.usersPage));
  }

  static void Function() goToImagePage(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.push(Routes.imagesPage));
  }

  static void Function() pop(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.pop());
  }

  static String getCurrentPage(Store<AppState> store) {
    return RouteService.instance.routesStackLast;
  }
}
