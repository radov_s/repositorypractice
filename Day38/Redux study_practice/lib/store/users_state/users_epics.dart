import 'package:redux_conceptions/dto/user_dto.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/repositories/unsplash_user_repository.dart';
import 'package:redux_conceptions/store/users_state/users_action.dart';

import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class UsersEpics {
  static final indexEpic = combineEpics<AppState>([
    getImagesEpic,
  ]);

  static Stream<dynamic> getImagesEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetUsersAction>().switchMap(
      (action) {
        return Stream.fromFuture(
          UnsplashUserRepository.instance.getUsers().then(
            (List<UserDtO> users) {
              if (users == null) {
                return EmptyAction();
              }
              return SaveUsersAction(
                users: users,
              );
            },
          ),
        );
      },
    );
  }
}
