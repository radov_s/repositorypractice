import 'dart:collection';

import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/dto/user_dto.dart';
import 'package:redux_conceptions/store/app/reducer.dart';
import 'package:redux_conceptions/store/images_state/images_action.dart';
import 'package:redux_conceptions/store/users_state/users_action.dart';

class ImagesState {
  final List<Image> images;
  final Image selectedImage;

  ImagesState({
    this.images,
    this.selectedImage,
  });

  factory ImagesState.initial() =>
      ImagesState(images: [], selectedImage: Image());

  ImagesState copyWith({
    List<Image> images,
    Image image,
  }) {
    return ImagesState(
      images: images ?? this.images,
      selectedImage: selectedImage ?? this.selectedImage,
    );
  }

  ImagesState reducer(dynamic action) {
    return Reducer<ImagesState>(
      actions: HashMap.from({
        SaveImagesAction: (dynamic action) =>
            saveImages((action as SaveImagesAction).images),
      }),
    ).updateState(action, this);
  }

  ImagesState saveImages(List<Image> images) {
    return copyWith(images: images);
  }
}
