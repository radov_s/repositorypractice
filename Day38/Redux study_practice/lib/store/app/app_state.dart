import 'package:flutter/foundation.dart';
import 'package:redux_conceptions/store/images_state/images_epics.dart';
import 'package:redux_conceptions/store/images_state/images_state.dart';
import 'package:redux_conceptions/store/users_state/users_epics.dart';

import 'package:redux_conceptions/store/users_state/users_state.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final UsersState usersState;
  final ImagesState imageState;

  AppState({
    @required this.usersState,
    @required this.imageState,
  });

  factory AppState.initial() {
    return AppState(
      usersState: UsersState.initial(),
      imageState: ImagesState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      usersState: state.usersState.reducer(action),
      imageState: state.imageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    UsersEpics.indexEpic,
    ImagesEpics.indexEpic,
  ]);
}
