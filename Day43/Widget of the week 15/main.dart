import 'package:flutter/material.dart';
import 'dart:ui' as ui;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<bool> isSelected = [false, false, false];
  List<Widget> dayList = [Text('Monday'), Text('Tuesday'), Text('Wednesday'), Text('Thursday'), Text('Friday'), Text('Saturday'), Text('Sunday')];

  @override
  Widget build(BuildContext context) {
    return PageView(
      children: [
        Scaffold(
          body: SingleChildScrollView(
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.network(
                            'https://assets-global.website-files.com/6005fac27a49a9cd477afb63/60576840e7d265198541a372_bavassano_homepage_gp.jpg'),
                        Center(
                          child: BackdropFilter(
                            filter: ui.ImageFilter.blur(
                              sigmaX: 5.0,
                              sigmaY: 5.0,
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              width: 200.0,
                              height: 200.0,
                              color: Color(0xdd4c5174),
                              child: ShaderMask(
                                blendMode: BlendMode.srcATop,
                                shaderCallback: (Rect bounds) {
                                  return LinearGradient(
                                    colors: [Colors.white, Colors.black],
                                  ).createShader(bounds);
                                },
                                child: Text('Wild life, cruel rules'),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  ToggleButtons(
                    children: [
                      Icon(Icons.favorite),
                      Icon(Icons.padding),
                      Icon(Icons.email),
                    ],
                    onPressed: (int index) {
                      setState(() {
                        for (int buttonIndex = 0; buttonIndex < isSelected.length; buttonIndex++) {
                          if (buttonIndex == index) {
                            isSelected[buttonIndex] = true;
                          } else {
                            isSelected[buttonIndex] = false;
                          }
                        }
                      });
                    },
                    isSelected: isSelected,
                    color: Color(0xff4c5174),
                    selectedColor: Colors.black,
                  ),
                ],
              ),
            ),
          ),
        ),
        Material(
          child: DraggableScrollableSheet(
            builder: (BuildContext context, ScrollController scrollController) {
              return Container(
                color: Colors.blue[100],
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: 20,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      tileColor: Color(0xdd4c5174).withOpacity(0.05 * index),
                    );
                  },
                ),
              );
            },
          ),
        ),
        Material(
          child: ListWheelScrollView(
            children: dayList,
            itemExtent: 50.0,
            useMagnifier: true,
            perspective: 0.01,
            offAxisFraction: -0.5,
          ),
        ),
      ],
    );
  }
}
