import 'package:bloc_example/model/image_bloc/image_bloc.dart';
import 'package:bloc_example/model/image_dto/image_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ImageBloc imageBLoC = ImageBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Images'),
        actions: [
          Chip(
            label: StreamBuilder<int>(
                stream: imageBLoC.imageCounter,
                builder: (context, snapshot) {
                  return Text(
                    (snapshot.data ?? 0).toString(),
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                  );
                }),
            backgroundColor: Colors.red,
          ),
          Padding(
            padding: EdgeInsets.only(right: 16),
          )
        ],
      ),
      body: StreamBuilder(
        stream: imageBLoC.imageList,
        // ignore: missing_return
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              if (snapshot.hasError) return Text('There was an error : ${snapshot.error}');
              List<ImageDto> images = snapshot.data;
              return ListView.builder(
                itemCount: images?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  ImageDto _image = images[index];
                  return InkWell(
                    child: SizedBox(
                      width: double.infinity,
                      height: 300.0,
                      child: BlurHash(
                        hash: _image.blurHash,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) {
                        return DetailPage(
                          index: index,
                          image: _image,
                        );
                      }));
                    },
                  );
                },
              );
          }
        },
      ),
    );
  }
}

class DetailPage extends StatelessWidget {
  const DetailPage({Key key, this.index, this.image}) : super(key: key);
  final int index;
  final ImageDto image;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Hero(
        tag: index,
        child: Column(
          children: [
            Image.network(image.url),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(image.likes.toString()),
                Icon(
                  Icons.favorite,
                  color: Colors.red,
                )
              ],
            ),
            Text(image.altDescription ?? 'empty')
          ],
        ),
      ),
    );
  }
}
