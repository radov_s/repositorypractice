import 'package:json_annotation/json_annotation.dart';

part 'image_dto.g.dart';

@JsonSerializable()
class ImageDto {
  final String id;
  final String blurHash;
  final String altDescription;
  final String url;
  final int likes;

  ImageDto({
    this.id,
    this.blurHash,
    this.altDescription,
    this.url,
    this.likes,
  });

  factory ImageDto.fromJson(Map<String, dynamic> json) => _$ImageDtoFromJson(json);

  Map<String, dynamic> toJson() => _$ImageDtoToJson(this);
}
