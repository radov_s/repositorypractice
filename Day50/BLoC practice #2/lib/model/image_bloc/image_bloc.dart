import 'dart:async';

import 'package:bloc_example/model/image_dto/image_dto.dart';
import 'package:bloc_example/services/image_service.dart';

class ImageBloc {
  Stream<List<ImageDto>> get imageList async* {
    yield await ImageService.fetchImages();
  }

  final StreamController<int> _imageCounter = StreamController<int>();

  Stream<int> get imageCounter => _imageCounter.stream;

  ImageBloc() {
    imageList.listen((event) {
      _imageCounter.add(event.length);
    });
  }
}
