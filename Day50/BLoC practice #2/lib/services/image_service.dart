import 'package:bloc_example/model/image_dto/image_dto.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class ImageService {
  static String _url = 'https://api.unsplash.com/photos/?client_id=aN27naK6D0QLKPxDkTHn3oduTLCNLivQHKqvO1X2408';

  static Future fetchImages() async {
    List collection;
    List<ImageDto> _images;
    var response = await http.get(_url);
    if (response.statusCode == 200) {
      collection = convert.jsonDecode(response.body);
      _images = collection.map((json) => ImageDto.fromJson(json)).toList();
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
    return _images;
  }
}
