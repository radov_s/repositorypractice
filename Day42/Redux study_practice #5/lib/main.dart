import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'package:redux/redux.dart';
import 'package:redux_study_5/application/application.dart';
import 'package:redux_study_5/store/app/app_state.dart';

void main() {
  final Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(Application(store: store));
}


