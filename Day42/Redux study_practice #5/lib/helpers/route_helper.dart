import 'package:flutter/material.dart';
import 'package:redux_study_5/store/navigation/routes.dart';
import 'package:redux_study_5/ui/pages/first_page.dart';
import 'package:redux_study_5/ui/pages/second_page.dart';
import 'package:redux_study_5/ui/pages/third_page.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case (Routes.firstPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => FirstPage(),);
      case (Routes.secondPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => SecondPage(),);
      case (Routes.thirdPage) : return PageRouteBuilder(pageBuilder: (context, _, __) => ThirdPage(),);
      default:
        return _defaultRoute(
          settings: settings,
          page: FirstPage(),
        );
    }
  }

  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
