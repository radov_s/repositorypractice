import 'package:flutter/foundation.dart';
import 'package:redux_study_5/store/conter_page/counter_state.dart';

class AppState {
  final CounterState counterState;


  AppState({
    @required this.counterState,
  });

  factory AppState.initial() {
    return AppState(
      counterState: CounterState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterState: state.counterState.reducer(action),
    );
  }
}