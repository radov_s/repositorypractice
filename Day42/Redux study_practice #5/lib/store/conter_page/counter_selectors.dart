import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';

import 'conter_actions.dart';



class CounterSelectors {

  static double getCounterValue (Store<AppState> store) {
    return store.state.counterState.counter;
  }
  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }
}