import 'dart:collection';

import 'package:redux_study_5/store/app/reducer.dart';
import 'package:redux_study_5/store/conter_page/conter_actions.dart';

class CounterState {
  double counter;

  CounterState(this.counter);

  factory CounterState.initial() => CounterState(0);

  CounterState copyWith({double counter}) {
    return CounterState(counter ?? this.counter);
  }

  CounterState reducer(dynamic action) {
    return Reducer<CounterState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => incrementCounter(),
        DecrementAction: (dynamic action) => decrementCounter(),
      }),
    ).updateState(action, this);
  }

  CounterState incrementCounter() {
    return copyWith(counter: ++counter);
  }
  CounterState decrementCounter() {
    return copyWith(counter: --counter);
  }

}
