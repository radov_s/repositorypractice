import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_study_5/store/navigation/routes.dart';

class RouteService {
  static const tag = '[RouteService]';

  RouteService._privateConstructor();

  static final RouteService _instance = RouteService._privateConstructor();

  static RouteService get instance => _instance;

  List<String> _routesStack = [
    Routes.firstPage
  ];

  String get routesStackLast => _routesStack.last;
  String get routesStackBeforeLast => _routesStack.last;

  bool canPop() {
    if (_routesStack.length <= 1) {
      return false;
    }
    return true;
  }

  NavigateToAction pop() {
    if (canPop()) {
      _routesStack.removeLast();
      return NavigateToAction.pop();
    }
  }

  bool canPush() {
    return true;
  }

  NavigateToAction push(String routeName) {
    if (routeName == _routesStack.last) {
      return pushReplacedNamed(routeName);
    }
    if (canPush()) {
      _routesStack.add(routeName);
      return NavigateToAction.push(routeName);
    }
  }

  NavigateToAction pushReplacedNamed(String routeName) {
    if (canPush()) {
      _routesStack.removeLast();
      _routesStack.add(routeName);
      return NavigateToAction.replace(routeName);
    }
  }
}
