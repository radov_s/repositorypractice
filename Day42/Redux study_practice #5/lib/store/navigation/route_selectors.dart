
import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/store/navigation/route_service.dart';
import 'package:redux_study_5/store/navigation/routes.dart';


class RouteSelectors {
  static void Function() goToFirstPage(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.push(Routes.firstPage));
  }

  static void Function() goToSecondPage(Store<AppState> store) {
    return () =>
        store.dispatch(RouteService.instance.push(Routes.secondPage));
  }

  static void Function() goToThirdPage(Store<AppState> store) {
    return () =>
        store.dispatch(RouteService.instance.push(Routes.thirdPage));
  }

  static void Function() pop(Store<AppState> store) {
    return () => store.dispatch(RouteService.instance.pop());
  }

  static String getCurrentPage(Store<AppState> store) {
    return RouteService.instance.routesStackLast;
  }
}
