import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/store/conter_page/counter_selectors.dart';

class DrawerViewModel {
  final double counter;
  final void Function() incrementCounter;
  final void Function() decrementCounter;

  DrawerViewModel({
    @required this.counter,
    @required this.incrementCounter,
    @required this.decrementCounter,
  });

  static DrawerViewModel fromStore(Store<AppState> store) {
    return DrawerViewModel(
      counter: CounterSelectors.getCounterValue(store),
      incrementCounter: CounterSelectors.getIncrementCounterFunction(store),
      decrementCounter: CounterSelectors.getDecrementCounterFunction(store),
    );
  }
}