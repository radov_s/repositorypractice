import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/ui/shared/drawer/drawer_viewmodel.dart';

class SharedDrawer extends StatelessWidget {
  const SharedDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DrawerViewModel>(
      converter: (Store<dynamic> store) => DrawerViewModel.fromStore(store),
      builder: (BuildContext context, DrawerViewModel vm) {
        Widget _buildButton(Function function, IconData icon) {
          return Container(
            width: double.infinity,
            height: 80.0,
            color: Colors.blueAccent,
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  function();
                },
                child: Center(
                  child: Icon(icon),
                ),
              ),
            ),
          );
        }
        Widget _buildDivider() {
          return Container(
            width: double.infinity,
            height: 1.0,
            color: Colors.black,
          );
        }

        return Drawer(
          child: Column(
            children: [
              SizedBox(
                height: 80.0,
              ),
              _buildDivider(),
              _buildButton(vm.incrementCounter, Icons.add),
              _buildDivider(),
              _buildButton(vm.decrementCounter, Icons.remove),
              _buildDivider(),
            ],
          ),
        );
      },
    );
  }
}
