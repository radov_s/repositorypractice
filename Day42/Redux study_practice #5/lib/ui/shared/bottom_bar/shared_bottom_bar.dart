import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/store/navigation/routes.dart';
import 'package:redux_study_5/ui/shared/bottom_bar/navigation_viewmodel.dart';

class SharedBottomBar extends StatelessWidget {
  const SharedBottomBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: (Store<dynamic> store) => NavigationViewModel.fromStore(store),
      builder: (BuildContext context, NavigationViewModel vm) {
        Widget _buildButton(IconData iconData, String nameRoute, Function function) {
          return InkWell(
            onTap: () {
              function();
            },
            child: Icon(
              iconData,
              size: 25.0,
              color: vm.getCurrentPage == nameRoute ? Colors.blueAccent : Colors.black26,
            ),
          );
        }

        return Container(
          width: double.infinity,
          height: 88.0,

          color: Color(0xfffed766),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton(Icons.looks_one, Routes.firstPage, vm.goToFirstPage),
              _buildButton(Icons.looks_two, Routes.secondPage, vm.goToSecondPage),
              _buildButton(Icons.looks_3, Routes.thirdPage, vm.goToThirdPage),
            ],
          ),
        );
      },
    );
  }
}
