
import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/store/navigation/route_selectors.dart';

class NavigationViewModel {
  final void Function() goToFirstPage;
  final void Function() goToSecondPage;
  final void Function() goToThirdPage;
  final void Function() popAction;
  final String getCurrentPage;

  NavigationViewModel({
    this.goToFirstPage,
    this.goToSecondPage,
    this.goToThirdPage,
    this.popAction,
    this.getCurrentPage,
  });

  static NavigationViewModel fromStore(Store<AppState> store) {
    return NavigationViewModel(
      goToFirstPage: RouteSelectors.goToFirstPage(store),
      goToSecondPage: RouteSelectors.goToSecondPage(store),
      goToThirdPage: RouteSelectors.goToThirdPage(store),
      getCurrentPage: RouteSelectors.getCurrentPage(store),
      popAction: RouteSelectors.pop(store),
    );
  }
}
