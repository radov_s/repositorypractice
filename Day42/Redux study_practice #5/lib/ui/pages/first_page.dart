import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/ui/shared/bottom_bar/shared_bottom_bar.dart';
import 'package:redux_study_5/ui/shared/drawer/drawer_viewmodel.dart';
import 'package:redux_study_5/ui/shared/drawer/shared_drawer.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DrawerViewModel>(
      converter: (Store<dynamic> store) => DrawerViewModel.fromStore(store),
      builder: (BuildContext context, DrawerViewModel vm) {
        return Scaffold(
          backgroundColor: Color(0xfffe4a49),
          appBar: AppBar(),
          drawer: SharedDrawer(),
          body: Center(
            child: Text(
              '${vm.counter}'
            ),
          ),
          bottomNavigationBar: SharedBottomBar(),
        );
      },
    );
  }
}
