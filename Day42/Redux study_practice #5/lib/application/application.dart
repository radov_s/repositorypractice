import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'package:redux/redux.dart';
import 'package:redux_study_5/helpers/route_helper.dart';
import 'package:redux_study_5/store/app/app_state.dart';
import 'package:redux_study_5/ui/pages/first_page.dart';


class Application extends StatelessWidget {
  final Store<AppState> store;

  const Application({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: StoreConnector<AppState, AppState>(
        converter: (Store<AppState> store) => store.state,
        builder: (BuildContext context, AppState state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            navigatorKey: NavigatorHolder.navigatorKey,
            home: FirstPage(),
            onGenerateRoute:  (RouteSettings settings) =>
                RouteHelper.instance.onGenerateRoute(settings),
            builder: (context, child) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor: 1.0,
                ),
                child: child,
              );
            },
          );
        },
      ),
    );
  }
}
