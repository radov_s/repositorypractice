
import 'package:flutter/material.dart';

class CustomModel with ChangeNotifier {
  DateTime dateTime;
  TimeOfDay timeOfDay;
  double slider = 0;
  String genderPeople;

  String toString() {
    return '$dateTime, $timeOfDay, $slider, $genderPeople';
  }
}