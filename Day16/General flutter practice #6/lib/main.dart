import 'package:flutter/material.dart';
import 'package:flutter_practice_6/models/custom_model.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => CustomModel(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime _dateTime;
  TimeOfDay _timeOfDay = TimeOfDay.now();
  double _slider = 0;
  String _genderPeople;

  final listGender = <String>['Male', 'Female', 'Prefer Not to Answer']
      .map<DropdownMenuItem<String>>((String value) {
    return DropdownMenuItem<String>(
      value: value,
      child: Text(value),
    );
  }).toList();

  @override
  Widget build(BuildContext context) {
    CustomModel provider = Provider.of<CustomModel>(context, listen: false);
    void _selectTime() async {
      final TimeOfDay newTime = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (newTime != null) {
        setState(() {
          _timeOfDay = newTime;
        });
      }
    }

    void _selectDate() async {
      final DateTime dateTime = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime.utc(1900),
          lastDate: DateTime(2030));
      if (dateTime != null) {
        _dateTime = dateTime;
      }
    }

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                _selectTime();
              },
              child: Text('SELECT TIME'),
            ),
            ElevatedButton(
              onPressed: () {
                _selectDate();
              },
              child: Text('SELECT DATE'),
            ),
            Slider(
              value: _slider,
              min: 0,
              max: 1,
              label: _slider.round().toString(),
              onChanged: (double value) {
                setState(() {
                  _slider = value;
                });
              },
            ),
            ColoredBox(
              color: Color(0xffdbf3fa),
              child: DropdownButton<String>(
                dropdownColor: Colors.blue,
                value: _genderPeople,
                onChanged: (String newValue) {
                  _genderPeople = newValue;
                },
                items: listGender,
              ),
            ),
            Container(
              color: Color(0xff45b6fe),
              height: 100.0,
              width: double.infinity,
              child: InkWell(
                onTap: () {
                  provider.dateTime = _dateTime;
                  provider.timeOfDay = _timeOfDay;
                  provider.slider = _slider;
                  provider.genderPeople = _genderPeople;
                  print(provider.toString());
                },
                child: Center(
                  child: Text(
                    'Save data and print that',
                    style: TextStyle(fontSize: 25.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
