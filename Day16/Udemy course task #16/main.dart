import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.blue,
        accentColor: Colors.lightBlue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  Animation<double> _size;
  Animation<double> _shadow;
  Animation<Color> _color;

  AnimationController _controllerAnimation;
  AnimationController _colorAnimationController;

  bool isAnimation = true;

  @override
  void initState() {
    super.initState();
    _controllerAnimation = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 500),
        reverseDuration: Duration(milliseconds: 500));
    _colorAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
      reverseDuration: Duration(milliseconds: 500),
    );

    _size = Tween<double>(begin: 100, end: 200).animate(_controllerAnimation);
    _shadow = Tween<double>(begin: 0, end: 50).animate(_controllerAnimation);
    _color = ColorTween(begin: Color(0xFFFFFFFF), end: Color(0xFF011F4B))
        .animate(_colorAnimationController);
    super.initState();
  }

  @override
  void dispose() {
    _controllerAnimation.dispose();
    super.dispose();
  }

  void _tapAnimation() {
    if (isAnimation) {
      isAnimation = !isAnimation;
      _controllerAnimation.forward();
      _colorAnimationController.forward();
    } else {
      isAnimation = !isAnimation;
      _controllerAnimation.reverse();
      _colorAnimationController.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(250.0),
        child: AnimatedBuilder(
          animation: _controllerAnimation,
          builder: (BuildContext context, Widget _) {
            return Container(
              width: double.infinity,
              height: _size.value,
              decoration: BoxDecoration(
                color: _color.value,
                boxShadow: [
                  BoxShadow(blurRadius: _shadow.value, color: Colors.black)
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                  'Custom AppBar',
                  style: TextStyle(fontSize: 30.0),
                )),
              ),
            );
          },
        ),
      ),
      body: Align(
        alignment: Alignment.bottomCenter,
        child: InkWell(
          onTap: _tapAnimation,
          child: Container(
            alignment: Alignment.center,
            height: 60.0,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}

/**/
