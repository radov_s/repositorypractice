import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('widgets week'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.face),
        onPressed: () {
          print('something is doing');
        },
      ),
      body: Column(
        children: [
          Container(
            height: 250,
            width: 250,
            child: Stack(
              children: <Widget>[
                Container(
                  width: 250,
                  height: 250,
                  color: Colors.red,
                ),
                Container(
                  padding: const EdgeInsets.all(5.0),
                  alignment: Alignment.bottomCenter,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: <Color>[
                        Colors.black.withAlpha(0),
                        Colors.black12,
                        Colors.black45
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 20,
            thickness: 20,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.purple,
          ),
          Row(
            children: [
              Container(
                height: 25,
                width: 25,
                color: Colors.blue,
                child: Text('1'),
              ),
              Spacer(flex: 1,), // Defaults to a flex of one.
              Container(
                height: 25,
                width: 25,
                color: Colors.blueAccent,
                child: Text('2'),
              ),
              // Gives twice the space between Middle and End than Begin and Middle.
              Spacer(),
              Container(
                height: 25,
                width: 25,
                color: Colors.indigo,
                child: Text('3'),
              )
            ],
          )
        ],
      ),
    );
  }
}
