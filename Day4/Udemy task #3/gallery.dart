import 'package:flutter/material.dart';

import './photo.dart';

class Gallery extends StatelessWidget {
  final List<Photo> gallery = [
    Photo(
        author: 'Alex',
        photoURL:
            'https://blog.depositphotos.com/wp-content/uploads/2017/07/Soothing-nature-backgrounds-2.jpg.webp'),
    Photo(
        author: 'Yurii',
        photoURL:
            'https://imgproxy.natucate.com/PAd5WVIh-tjEKQM4Z6tm6W1J4Yc2JIYWrKEroD1c7mM/rs:fill/g:ce/w:3840/h:2160/aHR0cHM6Ly93d3cubmF0dWNhdGUuY29tL21lZGlhL3BhZ2VzL3JlaXNlYXJ0ZW4vNmMwODZlYmEtNzk3Yi00ZDVjLTk2YTItODhhNGM4OWUyODdlLzM3NjYwMTQ2NjMtMTU2NzQzMzYxMi8xMl9kYW5pZWxfY2FuX2JjLTIwNy5qcGc'),
    Photo(
        author: 'Dmitriy',
        photoURL:
            'https://s3-us-west-2.amazonaws.com/uw-s3-cdn/wp-content/uploads/sites/6/2017/11/04133712/waterfall.jpg'),
    Photo(
        author: 'Max',
        photoURL:
            'https://s2.best-wallpaper.net/wallpaper/1920x1200/1310/Autumn-nature-forest-leaves-trees-light-rays_1920x1200.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: gallery.length,
          itemBuilder: (ctx, index) {
            return Card(
              child: Column(
                children: [
                  Text(
                    gallery[index].author,
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.indigoAccent,
                    ),
                  ),
                  Image.network(gallery[index].photoURL),
                ],
              ),
            );
          }),
    );
  }
}
