

import 'package:flutter/cupertino.dart';

class Photo {
  String author;
  String photoURL;

  Photo({this.author, this.photoURL});
}