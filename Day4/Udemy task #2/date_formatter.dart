import 'package:intl/intl.dart';

class DateFormatF {
  static String func(DateTime dateTime) {
    return DateFormat().format(dateTime);
  }
}