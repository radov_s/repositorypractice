
import 'package:flutter/material.dart';
import 'home.dart';
import 'authenticate.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        './': (ctx) => MyApp(),
        Home.routeName: (ctx) => Home(),
      },
      home: Authenticate(),
    );
  }
}