import 'package:flutter/material.dart';
import './layout/main_layout.dart';
import './screens/first_page.dart';
import './screens/fourth_page.dart';
import './screens/second_page.dart';
import './screens/third_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static const routeName = '/.';

  static var currentRouteName = '/.';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: MyHomePage(),
      routes: {
        MyApp.routeName: (ctx) => MyApp(),
        FirstPage.routeName: (ctx) => FirstPage(),
        SecondPage.routeName: (ctx) => SecondPage(),
        ThirdPage.routeName: (ctx) => ThirdPage(),
        FourthPage.routeName: (ctx) => FourthPage(),
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      drawer: MainLayout(),
    );
  }
}
