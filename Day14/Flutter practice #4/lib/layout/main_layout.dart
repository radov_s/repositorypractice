import 'package:flutter/material.dart';
import '../main.dart';
import '../screens/first_page.dart';
import '../screens/second_page.dart';
import '../screens/third_page.dart';
import '../screens/fourth_page.dart';

class MainLayout extends StatelessWidget {
  void _transferToPage(BuildContext context, String name) {
    if (name == MyApp.currentRouteName) {
      Navigator.of(context).pop();
    } else {
      MyApp.currentRouteName = name;
      Navigator.of(context).pushReplacementNamed(name);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildButton(IconData icon, String name) {
      return Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: 600.0,
            child: InkWell(
              child: Icon(icon),
              onTap: () {
                _transferToPage(context, name);
              },
            ),
          ),
          Divider(
            height: 2,
            thickness: 2,
            color: Colors.black,
          ),
        ],
      );
    }

    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBar(
              title: Text('Navigation'),
              automaticallyImplyLeading: false,
            ),
            _buildButton(Icons.home, MyApp.routeName),
            _buildButton(Icons.account_box, FirstPage.routeName),
            _buildButton(Icons.image, SecondPage.routeName),
            _buildButton(Icons.title, ThirdPage.routeName),
            _buildButton(Icons.settings, FourthPage.routeName),
          ],
        ),
      ),
    );
  }
}
