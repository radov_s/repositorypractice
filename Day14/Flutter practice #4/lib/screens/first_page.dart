import 'package:flutter/material.dart';
import '../layout/main_layout.dart';

class FirstPage extends StatelessWidget {
  static const routeName = '/first-page';

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account'),
      ),
      drawer: MainLayout(),
    );
  }
}
