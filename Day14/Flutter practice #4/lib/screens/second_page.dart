import 'package:flutter/material.dart';
import '../layout/main_layout.dart';

class SecondPage extends StatelessWidget {
  static const routeName = '/second-page';

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Images'),
      ),
      drawer: MainLayout(),
    );
  }
}
