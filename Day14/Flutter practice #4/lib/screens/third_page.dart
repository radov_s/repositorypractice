import 'package:flutter/material.dart';
import '../layout/main_layout.dart';

class ThirdPage extends StatelessWidget {
  static const routeName = '/third-page';

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notes'),
      ),
      drawer: MainLayout(),
    );
  }
}
