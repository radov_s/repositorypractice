import 'package:flutter/material.dart';
import '../layout/main_layout.dart';

class FourthPage extends StatelessWidget {
  static const routeName = '/fourth-page';

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      drawer: MainLayout(),
    );
  }
}
