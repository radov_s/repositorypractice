import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    Widget _buildDivider() {
      return Divider(
        thickness: 3,
        color: Colors.black,
        height: 2,
        indent: size.width * 0.05,
        endIndent: size.width * 0.05,
      );
    }

    return Column(
      children: [
        SizedBox(
          height: size.height * 0.05,
        ),
        _buildDivider(),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: size.width * 0.05, horizontal: size.width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Custom AppBar', textScaleFactor: 1,),
              Icon(Icons.web_asset),
            ],
          ),
        ),
        SizedBox(
          height: 50,
        ),
        _buildDivider(),
      ],
    );
  }
}
