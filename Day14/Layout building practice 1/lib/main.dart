import 'package:flutter/material.dart';
import 'package:layout_building_package_1/main_layout.dart';
import './custom_appbar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainLayout(),
    );
  }
}

