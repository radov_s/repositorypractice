import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Container(
              height: mediaQuery.size.height * 0.75,
              width: mediaQuery.size.width * 0.75,
              color: Colors.red,
            ),
            Text(
              mediaQuery.orientation == Orientation.portrait
                  ? 'portrait'
                  : 'landscape',
              style: TextStyle(fontSize: 25),
            )
          ],
        ),
      ),
    );
  }
}
