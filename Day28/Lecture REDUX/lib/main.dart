import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

void main() {
  runApp(MyApp());
}

@immutable
class AppState {
   Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      EpicMiddleware(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(MyApp(
    store: store,
  ));
}

class AppState {

  AppState();

  factory AppState.initial() {
    return AppState();
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState();
  }

  static final getAppEpic = combineEpics<AppState>([]);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final store = Store(reducer, initialState: AppState(0));

  @override
  Widget build(BuildContext context) {
    return Material(
      child: StoreProvider<dynamic>(
          store: store,
          child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 100.0,
                  ),
                  Text('U push many times'),
                  Material(
                    child: StoreConnector(
                      converter: (store) => store.state.counter,
                      builder: (context, counter) => Text(
                        '$counter',
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StoreConnector(
                          converter: (store) {
                            return () => store.dispatch(Action.Increment);
                          },
                          builder: (context, callback) => FloatingActionButton(
                            onPressed: callback,
                            child: Icon(Icons.add),
                          ),
                        ),
                        StoreConnector(
                          converter: (store) {
                            return () => store.dispatch(Action.Decrement);
                          },
                          builder: (context, callback) => FloatingActionButton(
                            onPressed: callback,
                            child: Icon(Icons.remove),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      ),
    );
  }
}
