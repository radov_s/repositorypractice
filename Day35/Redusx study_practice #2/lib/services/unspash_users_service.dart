import 'package:redux_conceptions/base_network_service.dart';
import 'package:redux_conceptions/services/network_service.dart';

class UnsplashUserService {
  UnsplashUserService._privateConstructor();

  static final UnsplashUserService instance =
      UnsplashUserService._privateConstructor();
  // static const String _url = 'https://api.unsplash.com/photos/?client_id=aN27naK6D0QLKPxDkTHn3oduTLCNLivQHKqvO1X2408';
  static const String _url = 'https://api.openbrewerydb.org/breweries';

  Future<BaseNetworkService> getUsers() async {
    var responseData =
        await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}

class UnsplashImageService {
  UnsplashImageService._privateConstructor();

  static final UnsplashImageService instance =
      UnsplashImageService._privateConstructor();
  static const String _url =
      'https://api.unsplash.com/photos/?client_id=aN27naK6D0QLKPxDkTHn3oduTLCNLivQHKqvO1X2408';
  // static const String _url = 'https://api.openbrewerydb.org/breweries';

  Future<BaseNetworkService> getUsers() async {
    var responseData =
        await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
