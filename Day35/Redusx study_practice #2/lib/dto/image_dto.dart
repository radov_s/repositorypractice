class Image {
  final String id;
  final String description;
  final String imageUrl;

  Image({this.id, this.description, this.imageUrl});

  factory Image.fromJson(Map<String, dynamic> json) {
    return Image(
      id: json[0],
      description:
          json['alt_description'] == null ? ' ' : json['alt_description'],
      imageUrl: json['urls']['regular'] == null ? ' ' : json['urls']['regular'],
    );
  }
}
