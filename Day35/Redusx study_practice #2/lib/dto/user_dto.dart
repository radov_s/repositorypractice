class UserDtO {
  final String id;
  final String name;
  final String city;
  final String state;
  final String country;
  final String breweryType;

  UserDtO(
      {this.id,
      this.name,
      this.city,
      this.state,
      this.country,
      this.breweryType});

  factory UserDtO.fromJson(Map<String, dynamic> json) {
    return UserDtO(
      id: json[0],
      name: json['name'],
      city: json['city'],
      state: json['state'],
      country: json['country'],
      breweryType: json['brewery_type'],
    );
  }
}
