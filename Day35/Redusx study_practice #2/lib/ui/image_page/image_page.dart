import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/ui/image_page/image_page_viewmodel.dart';
import 'package:redux_conceptions/ui/shared/custom_bottom_bar.dart';

class ImagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ImagePageViewModel>(
      converter: ImagePageViewModel.fromStore,
      onInitialBuild: (ImagePageViewModel viewModel) => viewModel.getImage(),
      builder: (BuildContext context, ImagePageViewModel viewModel) {
        return Scaffold(
          body: Builder(
            builder: (context) => ListView.builder(
              itemCount: viewModel.images.length,
              itemBuilder: (BuildContext ctx, int index) => Dismissible(
                key: UniqueKey(),
                onDismissed: (direction) {
                  viewModel.images.removeAt(index);
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Image dismissed'),
                      duration: Duration(milliseconds: 900),
                    ),
                  );
                },
                child: Container(
                  height: 500.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    color: Colors.blueAccent.withOpacity(0.6),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                        viewModel.images[index].imageUrl,
                      ),
                    ),
                  ),
                  padding: const EdgeInsets.all(10.0),
                  margin: const EdgeInsets.all(10.0),
                  child: Text(
                    viewModel.images[index].description,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                ),
              ),
            ),
          ),
          // bottomNavigationBar: CustomBottomBar(),
        );
      },
    );
  }
}
