import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/dto/user_dto.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/images_state/images_selector.dart';
import 'package:redux_conceptions/store/users_state/users_selector.dart';

class ImagePageViewModel {
  final List<Image> images;
  final Image image;
  final void Function() getImage;

  ImagePageViewModel({
    @required this.images,
    @required this.image,
    @required this.getImage,
  });

  static ImagePageViewModel fromStore(Store<AppState> store) {
    return ImagePageViewModel(
      images: ImagesSelector.getCurrentImages(store),
      image: ImagesSelector.getSelectedImage(store),
      getImage: ImagesSelector.getImagesFunction(store),
    );
  }
}
