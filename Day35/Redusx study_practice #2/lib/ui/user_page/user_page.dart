import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/ui/shared/custom_bottom_bar.dart';
import 'package:redux_conceptions/ui/user_page/photos_page_viewmodel.dart';

class UserPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UsersPageViewModel>(
      converter: UsersPageViewModel.fromStore,
      onInitialBuild: (UsersPageViewModel viewModel) => viewModel.getUsers(),
      builder: (BuildContext context, UsersPageViewModel viewModel) {
        return Scaffold(
          body: Builder(
            builder: (context) => ListView.builder(
              itemCount: viewModel.users.length,
              itemBuilder: (BuildContext ctx, int index) => Dismissible(
                key: UniqueKey(),
                onDismissed: (direction) {
                  viewModel.users.removeAt(index);
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('User dismissed'),
                      duration: Duration(milliseconds: 900),
                    ),
                  );
                },
                child: Container(
                  height: 100.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    color: Colors.blueAccent.withOpacity(0.6),
                  ),
                  padding: const EdgeInsets.all(10.0),
                  margin: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        viewModel.users[index].name,
                        style: TextStyle(fontSize: 24, color: Colors.indigo),
                      ),
                      Text(
                        viewModel.users[index].country +
                            ', ' +
                            viewModel.users[index].state +
                            ', ' +
                            viewModel.users[index].city,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.blueAccent,
                        ),
                      ),
                      Text(
                        viewModel.users[index].breweryType,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          // bottomNavigationBar: CustomBottomBar(),
        );
      },
    );
  }
}
