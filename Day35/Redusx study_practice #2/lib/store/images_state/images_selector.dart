import 'package:redux/redux.dart';
import 'package:redux_conceptions/dto/image_dto.dart';
import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/images_state/images_action.dart';

class ImagesSelector {
  static Image getSelectedImage(Store<AppState> store) {
    return store.state.imageState.selectedImage;
  }

  static List<Image> getCurrentImages(Store<AppState> store) {
    return store.state.imageState.images;
  }

  static void Function() getImagesFunction(Store<AppState> store) {
    return () => store.dispatch(GetImagesAction());
  }
}
