

import 'package:redux_conceptions/dto/user_dto.dart';

class GetUsersAction {
  GetUsersAction();
}
class SetInitUsersAction  {
  final List<UserDtO> users;
  SetInitUsersAction({this.users});
}
class SaveUsersAction{
  List<UserDtO> users;
  SaveUsersAction({this.users});
}

class EmptyAction  {
  EmptyAction();
}