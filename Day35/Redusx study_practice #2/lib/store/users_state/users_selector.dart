import 'package:redux/redux.dart';
import 'package:redux_conceptions/dto/user_dto.dart';

import 'package:redux_conceptions/store/app/app_state.dart';
import 'package:redux_conceptions/store/users_state/users_action.dart';

class UsersSelector {
  static UserDtO getSelectedUser(Store<AppState> store) {
    return store.state.usersState.selectedUser;
  }

  static List<UserDtO> getCurrentUsers(Store<AppState> store) {
    return store.state.usersState.users;
  }

  static void Function() getUsersFunction(Store<AppState> store) {
    return () => store.dispatch((GetUsersAction()));
  }
}
