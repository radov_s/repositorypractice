
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/counter_page_state/action.dart';

import '../app_state.dart';

class CounterPageSelectors {
  static double getCounterValue(Store<AppState> store) {
    return store.state.counterPageState.counter;
  }

  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static void Function() multiplyBy2(Store<AppState> store) {
    return () => store.dispatch(MultiplyBy2());
  }

  static void Function() resetCounter(Store<AppState> store) {
    return () => store.dispatch(ResetCounter());
  }

  static void Function(Color color) changeColor(Store<AppState> store) {
    return (Color color) => store.dispatch(ChangeColor(color: color));
  }

  static Color getColor(Store<AppState> store) {
    return store.state.counterPageState.colorTheme;
  }

  static bool getBoolean(Store<AppState> store) {
    return store.state.counterPageState.appBarColor;
  }

}
