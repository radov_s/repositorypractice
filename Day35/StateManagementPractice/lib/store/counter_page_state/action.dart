import 'package:flutter/material.dart';

class IncrementAction {
  IncrementAction();
}

class DecrementAction {
  DecrementAction();
}

class MultiplyBy2 {
  MultiplyBy2();
}

class ResetCounter {
  ResetCounter();
}

class ChangeColor {
  Color color;

  ChangeColor({
    this.color,
  });
}

class GetBoolean {
  GetBoolean();
}
