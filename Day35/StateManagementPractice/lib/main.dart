import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/app_state.dart';
import 'package:redux_example/viewmodel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
      builder: (BuildContext context, CounterPageViewModel viewModel) {
        Widget _buildButton(Color color) {
          return Container(
            height: 50,
            width: double.infinity,
            color: color,
            child: InkWell(onTap: () {
              viewModel.changeColor(color);
            }),
          );
        }

        return Scaffold(
          backgroundColor: viewModel.getColorTheme,
          drawer: Drawer(
            child: Column(
              children: [
                SizedBox(
                  height: 80,
                ),
                _buildButton(Colors.blue),
                _buildButton(Colors.red),
                _buildButton(Colors.pinkAccent),
              ],
            ),
          ),
          appBar: AppBar(backgroundColor: Colors.blueAccent),
        );
      },
    );
  }
}
