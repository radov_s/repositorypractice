import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return PageView(children: [
      Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Semantics(
            onLongPress: () {
              isSelected = !isSelected;
              print(isSelected);
              showDialog(
                context: context,
                builder: (BuildContext context) => CircleWidget(),
              );
            },
            selected: isSelected,
            onMoveCursorForwardByCharacter: (bool _forward) => print(_forward),
            label: 'Company logo',
            child: Wrap(
              direction: Axis.vertical,
              children: [
                for (int i = 0; i < 3; ++i)
                  ClipOval(
                    child: CircleAvatar(
                      maxRadius: 70,
                      child: InkWell(
                        onTap: () {
                          isSelected = !isSelected;
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => CircleWidget(),
                          );
                        },
                      ),
                      backgroundImage: NetworkImage(
                        'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
                      ),
                    ),
                  ),
                ClipPath(
                  clipper: ClipPathClass(),
                  child: Container(
                    width: 150,
                    height: 150,
                    color: Color(0xdd570823),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      ThirdPage(),
    ]);
  }
}

class CircleWidget extends StatelessWidget {
  const CircleWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.black26,
        child: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
        ));
  }
}

class ClipPathClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height - 30);

    var firstControlPoint = Offset(size.width / 5, size.height);
    var firstPoint = Offset(size.width / 2, size.height);
    path.quadraticBezierTo(firstControlPoint.dx - 150,
        firstControlPoint.dy - 50, firstPoint.dx, firstPoint.dy);

    var secondControlPoint = Offset(size.width - (size.width / 5), size.height);
    var secondPoint = Offset(size.width, size.height - 30);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondPoint.dx, secondPoint.dy);

    path.lineTo(size.width, 50);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class ThirdPage extends StatelessWidget {
  const ThirdPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: <Widget>[
        SizedBox(
          width: 200.0,
          height: 100.0,
          child: ElevatedButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                applicationIcon: Icon(Icons.animation),
                applicationVersion: '0.0.1',
                applicationLegalese: 'Oops',
              );
            },
            child: null,
          ),
        ),
        SizedBox(
          width: 100.0,
          height: 200.0,
          child: AbsorbPointer(
            absorbing: true,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Color(0xff570823),
              ),
              onPressed: () {},
              child: null,
            ),
          ),
        ),
      ],
    );
  }
}
