abstract class DogBreed {
  void tellBreedName();
}

void myFunction(DogBreed dog){
  dog.tellBreedName();
}

class Bulldog implements DogBreed {
  String breedNaming = 'Bulldog';
  @override
  void tellBreedName() {
    print("Oof, $breedNaming");
  }
}

class Poodle implements DogBreed {
  String breedNaming = 'Poodle';
  @override
  void tellBreedName() {
    print("Aof, $breedNaming");
  }
}

class Chihuahua implements DogBreed {
  String breedNaming = 'Chihuahua';

  void tellBreedName() {
    print("Aff, $breedNaming");
  }
}

void main() {
  Bulldog bulldog = Bulldog();
  Poodle poodle = Poodle();
  Chihuahua chihuahua = Chihuahua();
  myFunction(bulldog);
}