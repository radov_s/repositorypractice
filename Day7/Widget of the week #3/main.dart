import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ListView(
            shrinkWrap: true,
            children: [
              Center(
                child: SizedBox(
                  width: double.infinity,
                  height: 50.0,
                  child: Center(
                    child: Text(
                      'Most incredible sounds',
                      style: TextStyle(
                        fontSize: 25,
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                color: Colors.blue,
                child: ListTile(
                  leading: Icon(Icons.face),
                  title: Text('Eooph'),
                  subtitle: Text('#31'),
                  trailing: Icon(Icons.list),
                ),
              ),
              Card(
                color: Colors.indigo,
                child: ListTile(
                  leading: Icon(Icons.face),
                  title: Text('Poof'),
                  subtitle: Text('#32'),
                  trailing: Icon(Icons.list),
                ),
              ),
              Card(
                color: Colors.blueAccent,
                child: ListTile(
                  leading: Icon(Icons.face),
                  title: Text('Bibbib'),
                  subtitle: Text('#33'),
                  trailing: Icon(Icons.list),
                ),
              ),
              Card(
                color: Colors.indigoAccent,
                child: ListTile(
                  leading: Icon(Icons.face),
                  title: Text('Booopf'),
                  subtitle: Text('#34'),
                  trailing: Icon(Icons.list),
                ),
              ),
            ],
          ),
          Card(
            child: const SelectableText.rich(
              TextSpan(
                text:
                    'A witch is a person who practices witchcraft or magic. Traditionally, the word was used to accuse someone of bewitching someone, or casting a spell on them to gain control over them by magic.', // default text style
              ),
            ),
          ),
        ],
      ),
    );
  }
}
