import 'package:flutter/material.dart';
import '../model/user.dart';

class Users extends StatelessWidget {
  final List<User> users = [
    User(
        name: 'Alex',
        urlAvatar:
            'https://www.incimages.com/uploaded_files/image/1920x1080/getty_517194189_373099.jpg',
        age: 22),
    User(
        name: 'Suslin',
        urlAvatar:
            'https://previews.123rf.com/images/ammentorp/ammentorp1802/ammentorp180200286/95518580-friends-chilling-outside-taking-group-selfie-and-smiling-laughing-young-people-standing-together-out.jpg',
        age: 32),
    User(
        name: 'Seebeck',
        urlAvatar:
            'https://thumbs.dreamstime.com/b/close-up-photo-satisfied-person-closed-eyes-arms-behind-head-wear-pullover-isolated-blue-color-background-202991171.jpg',
        age: 26),
    User(
        name: 'William',
        urlAvatar:
            'https://www.washingtonpost.com/rf/image_1484w/2010-2019/WashingtonPost/2017/03/28/Local-Politics/Images/Supreme_Court_Gorsuch_Moments_22084-70c71-0668.jpg?t=20170517',
        age: 53),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: users.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: Color(0xffb74093),
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(
                users[index].urlAvatar,
              ),
            ),
            title: Text(
              users[index].name,
            ),
            subtitle: Text(
              users[index].age.toString() + ' y.o.',
            ),
          ),
        );
      },
    );
  }
}
