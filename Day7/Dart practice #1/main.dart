import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        title: Text('Flutter Table Example'),
      ),
      body: Center(
        child: Table(
          border: TableBorder.all(
            color: Colors.black,
            style: BorderStyle.solid,
            width: 1,
          ),
          children: [
            for (int i = 0; i < 11; ++i)
              TableRow(
                children: [
                  for (int j = 0; j < 11; ++j)
                    Column(
                      children: [
                        if (j == 0 || i == 0)
                          cellConstruct(
                              i == 0 ? '${(j)}' : '${(i)}', Colors.green),
                        if (i == j && i != 0)
                          cellConstruct('${(i * j)}', Colors.lightGreen),
                        if (i <= j && i != 0 && j != 0 && i != j)
                          cellConstruct('${(i * j)}', Colors.white),
                        if (i >= j && i != 0 && j != 0 && i != j)
                          cellConstruct('${(i * j)}', Colors.yellowAccent),
                      ],
                    ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  Widget cellConstruct(String text, Color color) {
    return Container(
      height: 30,
      width: double.infinity,
      color: color,
      child: Center(
        child: Text(
          text,
        ),
      ),
    );
  }
}
