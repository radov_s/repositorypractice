import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            Tooltip(
              message: 'Transition to home',
              child: Icon(
                Icons.home,
                size: 50,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Placeholder(
                color: Colors.blueAccent,
                fallbackHeight: 100.0,
                strokeWidth: 5.0,
              ),
            ),
            Container(
              width: double.infinity,
              height: 100.0,
              color: Colors.grey,
              child: Image.network(
                  'https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg',
                  fit: BoxFit.fitWidth),
            ),
            ConstrainedBox(
              constraints: BoxConstraints(
                maxWidth: 150,
              ),
              child: Container(
                color: Colors.blue,
                width: double.infinity,
                height: 50.0,
              ),
            ),
            LimitedBox(
              maxHeight: 200,
              child: ListView.builder(
                itemCount: 6,
                itemBuilder: (context, i) {
                  return ListTile(
                    trailing: Text('hello'),
                    title: Text("$i"),
                    tileColor: i % 2 == 0  ? Colors.green : Colors.greenAccent,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
