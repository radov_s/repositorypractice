import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/app_state.dart';
import 'package:redux_example/viewmodel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
      builder: (BuildContext context, CounterPageViewModel viewModel) {
        return Scaffold(
          drawer: Drawer(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 50.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Switch(
                    value: viewModel.getBoolean,
                    onChanged: (_) {
                      viewModel.changeColor();
                    },
                  ),
                  Container(
                    width: 50.0,
                    height: 50.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.green,
                    ),
                    child: InkWell(
                        child: Icon(Icons.exposure_zero),
                        onTap: viewModel.resetCounter),
                  )
                ],
              ),
            ),
          ),
          appBar: AppBar(
            backgroundColor: viewModel.getBoolean == false ? Colors.orange : Color(0xffa9a9a9),
          ),
          body: Center(
            child: Column(
              children: [
                Text(
                  viewModel.counter.toString(),
                  style: Theme.of(context).textTheme.headline4,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FloatingActionButton(
                      child: Icon(
                        Icons.remove,
                      ),
                      onPressed: viewModel.decrementCounter,
                    ),
                    FloatingActionButton(
                      child: Icon(
                        Icons.add,
                      ),
                      onPressed: viewModel.incrementCounter,
                    ),
                    FloatingActionButton(
                      child: Icon(
                        Icons.two_k,
                      ),
                      onPressed: viewModel.multiplyBy2,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
