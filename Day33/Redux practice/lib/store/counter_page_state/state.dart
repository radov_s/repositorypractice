import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:redux_example/store/reducer.dart';

import 'action.dart';

class CounterPageState {
  final double counter;
  bool appBarColor = false;

  CounterPageState({
    this.appBarColor,
    this.counter,
  });

  factory CounterPageState.initial() {
    return CounterPageState(
      appBarColor: false,
      counter: 0,
    );
  }

  CounterPageState copyWith({
    double counter,
    bool appBarColor
  }) {
    return CounterPageState(
      appBarColor: appBarColor ?? this.appBarColor,
      counter: counter ?? this.counter,
    );
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
        MultiplyBy2: (dynamic action) => _multiplyBy2(),
        ResetCounter: (dynamic action) => _resetCounter(),
        ChangeColor: (dynamic action) => _changeColor(),
      }),
    ).updateState(action, this);
  }

  CounterPageState _incrementCounter() {
    return copyWith(counter: counter + 1);
  }

  CounterPageState _decrementCounter() {
    return copyWith(counter: counter - 1);
  }

  CounterPageState _multiplyBy2() {
    return copyWith(counter: counter * 2);
  }
  CounterPageState _resetCounter() {
    return copyWith(counter: 0);
  }

  CounterPageState _changeColor() {
    return copyWith(appBarColor: !appBarColor);
  }
}
