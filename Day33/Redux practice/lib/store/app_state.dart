import 'package:flutter/foundation.dart';

import 'counter_page_state/state.dart';

class AppState {
  final CounterPageState counterPageState;

  AppState({
    @required this.counterPageState,
  });

  factory AppState.initial() {
    return AppState(
      counterPageState: CounterPageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterPageState: state.counterPageState.reducer(action),
    );
  }
}
