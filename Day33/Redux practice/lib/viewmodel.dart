import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/app_state.dart';
import 'package:redux_example/store/counter_page_state/selector.dart';

class CounterPageViewModel {
  double counter;
  final void Function() incrementCounter;
  final void Function() decrementCounter;
  final void Function() multiplyBy2;
  final void Function() resetCounter;
  final void Function() changeColor;
  final bool getBoolean;


  CounterPageViewModel({
    @required this.counter,
    @required this.incrementCounter,
    @required this.decrementCounter,
    @required this.multiplyBy2,
    @required this.resetCounter,
    @required this.changeColor,
    @required this.getBoolean,
  });

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      counter: CounterPageSelectors.getCounterValue(store),
      incrementCounter: CounterPageSelectors.getIncrementCounterFunction(store),
      decrementCounter: CounterPageSelectors.getDecrementCounterFunction(store),
      multiplyBy2: CounterPageSelectors.multiplyBy2(store),
      resetCounter: CounterPageSelectors.resetCounter(store),
      changeColor: CounterPageSelectors.changeColor(store),
      getBoolean: CounterPageSelectors.getBoolean(store)
    );
  }
}
