void displayForLoop(List<int> listArg) {
  for (int i = 0; i < listArg.length; ++i) {
    print(listArg[i]);
  }
  print('\n');
}

void displayForInLoop(List<int> listArg) {
  for (int el in listArg) {
    print(el);
  }
  print('\n');
}

void displayForEachLoop(List<int> listArg) {
  listArg.forEach((el) => print(el));
  print('\n');
}

void displayWhileLoop(List<int> listArg) {
  int counter = 0;
  while(counter < listArg.length) {
    print(listArg[counter]);
    counter++;
  }
  print('\n');
}


void displayDoWhileLoop(List<int> listArg) {
  int counter = 0;
  do {
    print(listArg[counter]);
    counter++;
  } while (counter < listArg.length);
  print('\n');
}

void displayMethodDotMap(List<int> listArg) {
  listArg.map((el) => print(el)
  ).toList();
  print('\n');
}

void main() {
  List<int> list = [1,2,3,4,5];
  displayForLoop(list);
  displayForInLoop(list);
  displayForEachLoop(list);
  displayWhileLoop(list);
  displayDoWhileLoop(list);
  displayMethodDotMap(list);
}
