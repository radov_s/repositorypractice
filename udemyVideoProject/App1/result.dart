
import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    String resultText;
    if(resultScore < 4) {
      resultText = 'U a awesome';
    } else if (resultScore < 20) {
      resultText = 'Not so good';
    } else if (resultScore < 40) {
      resultText = 'Kill yourself';
    } else if (resultScore < 1000) {
      resultText = "Ooops";
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
              resultPhrase
          ),
          FlatButton(
              child: Text('Restart quiz!'),
              onPressed: resetHandler,
          )
        ],
      )
    );
  }
}