import 'package:flutter/material.dart';
import 'package:test_1/quiz.dart';
import 'package:test_1/result.dart';

void main() {
  runApp(MyClass());
}

class MyClass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyClassState();
  }
}

class _MyClassState extends State<MyClass> {
  static const List<Map<String, Object>> _questions = const [
    {
      'questionText' : 'what ur fav color',
      'answer' : [
        {'text': 'Black', 'score': 1},
        {'text': 'Red', 'score': 3},
        {'text': 'Green', 'score': 7},
        {'text': 'White', 'score': 53},
      ],
    },
    {
      'questionText' : 'what ur fav animal',
      'answer': [
        {'text': 'Snake', 'score': 1},
        {'text': 'Rabbit', 'score': 5},
        {'text': 'Elephant', 'score': 14},
        {'text': 'Lion', 'score': 25},
      ],
    },
    {
      'questionText' : 'beloved family member',
      'answer': [
        {'text': 'Mom', 'score': 14},
        {'text': 'Pap', 'score': 25},
        {'text': 'Sis', 'score': 1},
        {'text': 'Grandpa', 'score': 999},
      ],
    }
  ];

  int _questionIndex = 0;
  int _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;

    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('my prog'),
          backgroundColor: Colors.deepPurple,
          ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}















