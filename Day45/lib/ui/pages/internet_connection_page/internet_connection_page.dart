import 'dart:async';

import 'package:flutter/material.dart';
import 'package:streams_practice/streams/check_internet_connection.dart';

class InternetConnectionPage extends StatefulWidget {
  @override
  _InternetConnectionPageState createState() => _InternetConnectionPageState();
}

class _InternetConnectionPageState extends State<InternetConnectionPage> {
  final InternetConnectionStream _connectionStream = InternetConnectionStream();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Streams background'),
      ),
      body: StreamBuilder<bool>(
        stream: _connectionStream.connectivityStream,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('No data yet');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Text('done');
          } else if (snapshot.hasError) {
            return Text('Error' + snapshot.error);
          } else if (snapshot.data != null) {
            return Container(
              color: Colors.blueAccent,
              width: 200,
              height: 200,
              child: Center(
                child: Text(
                  snapshot.data.toString() == 'false' ? 'Not connected' : 'Connected',
                  style: TextStyle(
                    fontSize: 23
                  ),
                ),
              ),
            );
          }
          return Text('');
        },
      ),
    );
  }
}
