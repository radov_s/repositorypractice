import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class CounterPage extends StatefulWidget {
  const CounterPage({Key key}) : super(key: key);

  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  List list;

  Future createIsolate() async {
    ReceivePort receivePort = ReceivePort();
    Isolate.spawn(isolateFunction, receivePort.sendPort);

    SendPort childSendPort = await receivePort.first;

    final connection = await InternetAddress.lookup('google.com');

    ReceivePort responsePort = ReceivePort();


    childSendPort.send(['https://www.1secmail.com/api/v1/?action=genRandomMailbox&count=10', responsePort.sendPort]);

    var response = await responsePort.first;
    print(response);

    setState(() {
      list = response;
    });
  }

  static void isolateFunction(SendPort mainSendPort) async {
    ReceivePort childReceivePort = ReceivePort();
    mainSendPort.send(childReceivePort.sendPort);

    await for (var massage in childReceivePort) {
      String url = massage[0];
      SendPort replyPort = massage[1];

      var response = await http.get(url);
      replyPort.send(jsonDecode(response.body));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 80.0,
          ),
          if (list == null) Center(child: CircularProgressIndicator()),
          if (list != null)
            Expanded(
              child: Center(
                child: ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return Text(list[index]);
                  },
                ),
              ),
            ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            list = null;
          });
          createIsolate();
        },
      ),
    );
  }
}
