import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'package:rxdart/rxdart.dart';


class InternetConnectionStream {
  InternetConnectionStream() {
    Timer.periodic(Duration(seconds: 1), (timer) async {
      isConnected = await internetConnectivity();
      _controller.sink.add(isConnected);
    });
  }

  bool isConnected = false;

  StreamController _controller = StreamController<bool>();

  Stream<bool> get connectivityStream => _controller.stream;

  void addToStream() => _controller.add(internetConnectivity());

  Future<bool> internetConnectivity() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (event) {
      print(event);
      return false;
    }
    return false;
  }
}