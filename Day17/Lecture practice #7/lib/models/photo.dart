class Photo {
  final String url;
  final String author;

  Photo({this.url, this.author});
}
