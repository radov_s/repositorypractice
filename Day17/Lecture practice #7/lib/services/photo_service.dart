import 'package:lecture_practice_7/models/photo.dart';

class PhotoService {
  static Photo get(var list, int i) {
    return Photo(
      url: list[i]['urls']['full'],
      author: list[i]['user']['name'],
    );
  }
}
