import 'dart:convert';

import 'package:flutter/material.dart';
import './services/photo_service.dart';
import './models/photo.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Photo> photos = [];
  int _counter = 0;

  void _addPerson() async {
    final data = await http.get(
        'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0');

    List<dynamic> list = jsonDecode(data.body);

    if (_counter < list.length) {
      photos.add(PhotoService.get(list, _counter));
      setState(() {
        _counter++;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int i) {
          return ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(photos[i].url),
            ),
            title: Text(photos[i].author),
          );
        },
        itemCount: photos.length,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addPerson,
        child: Icon(Icons.add),
      ),
    );
  }
}
