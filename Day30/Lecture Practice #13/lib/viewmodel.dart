import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/app_state.dart';
import 'package:redux_example/store/counter_page_state/selector.dart';

class CounterPageViewModel {
  double counter;
  final void Function() incrementCounter;
  final void Function() decrementCounter;
  final void Function() multiplyBy2;
  final void Function() resetCounter;

  CounterPageViewModel({
    @required this.counter,
    @required this.incrementCounter,
    @required this.decrementCounter,
    @required this.multiplyBy2,
    @required this.resetCounter,
  });

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      counter: CounterPageSelectors.getCounterValue(store),
      incrementCounter: CounterPageSelectors.getIncrementCounterFunction(store),
      decrementCounter: CounterPageSelectors.getDecrementCounterFunction(store),
      multiplyBy2: CounterPageSelectors.getMultiplyBy2(store),
      resetCounter: CounterPageSelectors.resetCounter(store),
    );
  }
}
