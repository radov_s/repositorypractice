import 'dart:collection';
import 'package:redux_example/store/reducer.dart';

import 'action.dart';

class CounterPageState {
  final double counter;

  CounterPageState({
    this.counter,
  });

  factory CounterPageState.initial() {
    return CounterPageState(
      counter: 0,
    );
  }

  CounterPageState copyWith({
    double counter,
  }) {
    return CounterPageState(
      counter: counter ?? this.counter,
    );
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
        MultiplyBy2: (dynamic action) => _multiplyBy2(),
        ResetCounter: (dynamic action) => _resetCounter(),
      }),
    ).updateState(action, this);
  }

  CounterPageState _incrementCounter() {
    return CounterPageState(counter: counter + 1);
  }

  CounterPageState _decrementCounter() {
    return copyWith(counter: counter - 1);
  }

  CounterPageState _multiplyBy2() {
    return copyWith(counter: counter * 2);
  }
  CounterPageState _resetCounter() {
    return CounterPageState(counter: 0);
  }
}
