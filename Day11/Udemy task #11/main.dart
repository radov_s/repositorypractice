import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MyModel>(
      create: (context) => MyModel(),
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text('Udemy task 11'),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Consumer<MyModel>(
                  builder: (context, myModel, child) {
                    return Text(
                      myModel.someValue,
                      style: TextStyle(fontSize: 40.0),
                    );
                  },
                ),
                Consumer<MyModel>(
                  builder: (context, myModel, child) {
                    return DropdownButton<String>(
                      icon: Icon(Icons.format_list_numbered),
                      iconSize: 50,
                      items: List<String>.generate(
                              101, (int index) => index.toString())
                          .map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (String value) {
                        setState(
                          () {
                            myModel.doSomething(value);
                            print(value);
                          },
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyModel with ChangeNotifier {
  String someValue = 'exactly';

  void doSomething(String string) {
    someValue = string;
    notifyListeners();
  }
}
