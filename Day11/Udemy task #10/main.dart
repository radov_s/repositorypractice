import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  Widget buildButton(String textButton) {
    return Consumer<MyModel>(
      builder: (context, myModel, child) {
        return RaisedButton(
          child: Text(textButton),
          onPressed: () {
            myModel.doSomething(textButton);
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MyModel>(
      create: (context) => MyModel(),
      child: MaterialApp(
        home: Scaffold(
          body: Column(
            children: [
              Consumer<MyModel>(
                builder: (context, myModel, child) {
                  return AppBar(title: Text(myModel.someValue));
                },
              ),
              buildButton('text#1'),
              buildButton('text#2'),
              buildButton('text#3'),
              buildButton('AppBar'),
            ],
          ),
        ),
      ),
    );
  }
}

class MyModel with ChangeNotifier {
  String someValue = 'AppBar';

  void doSomething(String value) {
    someValue = value;
    notifyListeners();
  }
}
