import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:layout_building_app_store_1/providers/items.dart';
import 'package:layout_building_app_store_1/widgets/products_grid.dart';
import 'package:provider/provider.dart';

import 'screen/app_page.dart';
import 'screen/arcade_page.dart';
import 'screen/games_page.dart';
import 'screen/search_page.dart';
import 'today_page.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.white.withOpacity(0.8),
    statusBarIconBrightness: Brightness.dark,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  Widget build(BuildContext context) {

    return MultiProvider(
        providers: [
        ChangeNotifierProvider(
        create: (ctx) => Items(),
        ),
      ],
      child: MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.black, accentColor: Colors.black),
      home: HomePage(),
      )
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final date = DateTime.now();

  int _index = 0;
  List<Widget> listWidget = [
    SearchPage(),
    ArcadePage(),
    AppPage(),
    GamesPage(),
    ProductsGrid(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listWidget[_index],
      bottomNavigationBar: CupertinoTabBar(
        onTap: (newValue) {
          setState(() {
            _index = newValue;
          });
        },
        iconSize: 30.0,
        currentIndex: _index,
        activeColor: Colors.blue,
        inactiveColor: Colors.grey,
        backgroundColor: Colors.white.withOpacity(0.3),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.gamepad),
            label: 'Arcade',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.apps),
            label: 'Apps',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.airplanemode_on),
            label: 'Games',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tab),
            label: 'Today',
          ),
        ],
      ),
    );
  }
}
