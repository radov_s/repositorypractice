import 'package:flutter/material.dart';
import 'package:layout_building_app_store_1/model/item.dart';
import 'package:layout_building_app_store_1/providers/items.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import './product_item.dart';

class ProductsGrid extends StatelessWidget {
  final data = DateTime.now();

  @override
  Widget build(BuildContext context) {
    final products = Provider.of<Items>(context, listen: false).items;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Items(),
        ),
      ],
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 40.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      backgroundImage: NetworkImage(
                          'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80'),
                    ),
                    Column(
                      children: [
                        Text(
                          DateFormat.MMMMEEEEd().format(data).toUpperCase(),
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w600
                          ),
                        ),
                        Text(
                          'Today',
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.w900),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 10,
                child: GridView.builder(
                  physics: new NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(20.0),
                  itemCount: products.length,
                  itemBuilder: (ctx, i) {
                    return ProductItem(products[i]);
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    childAspectRatio: 2 / 2,
                    crossAxisSpacing: 50,
                    mainAxisSpacing: 30,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
