class Item {
  final String id;
  final String urlImage;
  final String description;
  final String addInformation;

  Item({this.id, this.urlImage, this.description, this.addInformation});
}
