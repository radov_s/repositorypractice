import 'package:flutter/material.dart';
import 'package:layout_building_app_store_1/model/item.dart';


class Items with ChangeNotifier {
  List<Item> _items = [
    Item(
        id: 'item1',
        urlImage:
        'http://groovymovieguy.com/wp-content/uploads/2020/10/the-light-house-poster-2-776x1024.jpg',
        description: 'The Lighthouse is a 2019 film directed and produced by Robert Eggers, who co-wrote the screenplay with his brother Max Eggers. A gothic psychological horror[6] film, it was international co-production of the United States and Canada, with the film being shot in black-and-white with a nearly square 1.19:1 aspect ratio. It stars Willem Dafoe and Robert Pattinson as two lighthouse keepers who begin to descend into madness when a storm strands them on the remote island where they are stationed.',
        addInformation: ' '),
    Item(
        id: 'item2',
        urlImage:
        'https://i.pinimg.com/originals/96/a0/0d/96a00d42b0ff8f80b7cdf2926a211e47.jpg',
        description: 'Friends find themselves caught up in a government conspiracy after unleashing an ancient power in the desert.',
        addInformation: ' '),
    Item(
        id: 'item3',
        urlImage:
        'https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/169241483/original/af413b406d3cb2eedc149baf2a8b87156d86d56f/make-your-movie-or-short-film-poster.jpg',
        description: '',
        addInformation: ' '),
    Item(
      id: 'item4',
      urlImage: 'https://www.indiewire.com/wp-content/uploads/2019/12/us-1.jpg?w=758',
      description: 'Jordan Peele’s “Us” had one of the best posters of 2018 thanks to its inkblot teaser, and the film’s marketing strengths continued into this year with a striking official one-sheet featuring Lupita Nyong’o. The “Us” poster communicates the film’s plot more intriguingly than any trailer could.',
      addInformation: '45.54'
    )


  ];
  List<Item> get items {
    return [..._items];
  }

  Item findById(String id) {
    return _items.firstWhere((product) => product.id == id);
  }
}